-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 10, 2016 at 06:42 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cop`
--

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `sender_id` int(10) UNSIGNED NOT NULL,
  `receiver_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `content`, `sender_id`, `receiver_id`, `created_at`, `updated_at`) VALUES
(1, '12 cung Hoàng Đạo hoàn hảo với 12 cung tương xứng với bốn mùa và 12 tháng. Các cung được phân chia làm bốn nhóm yếu tố (Lửa, Nước, Khí, Đất), mỗi nhóm yếu tố gồm 3 cung đại diện cho các cung có tính cách tương đồng với nhau.', 1, 2, '2016-11-09 16:31:47', '2016-11-09 16:31:47'),
(2, 'mỗi nhóm yếu tố gồm 3 cung đại diện cho các cung có tính cách tương đồng với nhau. ', 1, 2, '2016-11-09 16:32:33', '2016-11-09 16:32:33'),
(3, 'Các cung được phân chia làm bốn nhóm yếu tố (Lửa, Nước, Khí, Đất)', 1, 2, '2016-11-09 23:49:32', '2016-11-09 23:49:32'),
(4, ' mỗi nhóm yếu tố gồm 3 cung đại diện cho các cung có tính cách tương đồng với nhau.', 2, 1, '2016-11-09 23:51:34', '2016-11-09 23:51:34'),
(5, 'Tỳ Hưu là loài linh vật được người dân chọn để thờ cúng cầu sự mong phát tài, phát lộc, thăng quan tiến chức', 1, 2, '2016-11-09 23:55:51', '2016-11-09 23:55:51'),
(6, 'Tăng vận may 12 con giáp với Phi tinh tháng 09/2016 (từ 08/09/2016 đến 06/10/2016 Dương lịch)', 2, 1, '2016-11-09 23:56:24', '2016-11-09 23:56:24'),
(7, 'Hỏa hoạn và các biện pháp phòng ngừa trong Phong thủy', 1, 2, '2016-11-09 23:56:50', '2016-11-09 23:56:50'),
(8, 'Như các bạn cũng đã thấy, hàng năm trên thế giới nói chung và ở Việt Nam nói riêng thì hỏa hoạn luôn rình rập bất cứ nơi đâu trên toàn thế giới.', 2, 1, '2016-11-09 23:57:09', '2016-11-09 23:57:09'),
(9, 'Tính cách biểu hiện qua 12 cung hoàng đạo', 1, 3, '2016-11-10 00:30:25', '2016-11-10 00:30:25');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '2016-10-10 06:55:10', '2016-10-10 06:55:10'),
(2, 'Moderator', '2016-10-10 06:55:10', '2016-10-10 06:55:10'),
(3, 'User', '2016-10-10 06:55:10', '2016-10-10 06:55:10');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `sex` enum('male','female') COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role_id` int(10) NOT NULL DEFAULT '3',
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `religion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `name`, `email`, `description`, `sex`, `birthday`, `role_id`, `avatar`, `phone`, `address`, `religion`, `active`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'thanhfreewings', 'Thanh Nguyen', 'nguyenvanthanh9595@gmail.com', '', 'male', '14/12/1995', 3, '/uploads/avatars/1478772840_user.jpg', '01677713817', 'Ha Noi, Viet Nam', NULL, 'N', '$2y$10$5TuptzwC7xoJwPj/8SGBMOkNYc393FjCkHSNBrzFIwGe6rvRM3Ipi', 'VPhjr3gB0QIyorDYsn8zlmbpHtX1KNOmWU9nHKvrAWoVq2Nf7za1dVY2kHtb', '2016-11-05 02:28:55', '2016-11-10 15:28:42'),
(2, NULL, 'Hòa', 'hoa@example.com', NULL, 'male', NULL, 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$0zF9DFuBH8bXal765wthy.eF4a4/0HOuaCQPvpPDriTMrzvAS0dba', '6I7mejsso1wZ91a9YXVXjYmpFbvBhnQ7WlzEmCZfBT0qpAoB8fUOjccasTgf', '2016-11-05 03:54:41', '2016-11-05 03:56:13'),
(3, NULL, 'Tuấn', 'tieutu881993@gmail.com', NULL, 'male', NULL, 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$hiXwQJDDQXruL..MmtMsSOa1cjo63KAIM7loYSFEib0GBctaFuAhe', 'Sz5i3BCuJCPqYbsekV426xvdLT4GXPMRY6KJn0joWImqQkrSc3N4r1kBuwXB', '2016-11-07 01:24:27', '2016-11-07 01:24:32'),
(4, NULL, 'Phong', 'phong@example.com', NULL, 'male', NULL, 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$xGY4vWnBw/8nQQ/z0QGvjeYg5.JNzrfnii87Ztwx6IXwgVf33rIVW', 'hCXLiNqRmVrpOjsc5ilGE6MnKkirziaS0mWZuZKg6zuSXtjdl6RTJQnOvcAo', '2016-11-07 01:25:08', '2016-11-07 01:25:11'),
(5, NULL, 'Hải Nguyễn', 'hainguyen@example.com', NULL, 'male', NULL, 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$saCnkVUKFycCwR5yBykDKOMXt5vmWva5MtqStp9m5xOYk04lbzzeu', 'R2uKoRFgLHyCRlFijGLS2lSTF7ywIybaJtVh6Dlx6KzKKXVx5j9Q8WmKah9O', '2016-11-07 01:25:54', '2016-11-07 01:25:57'),
(6, NULL, 'Trọng', 'trong@example.com', NULL, 'male', NULL, 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$NTvWW54LTUlYShE.vjOn7uL.RXj/ipUJ155EMXfx/4MYRh0rHXQI.', 'hU5oHrFiMN0nqNFyRP8ZiTW28GYehU7CucB1zRnNE5GJvx5G8NRnSBHBe401', '2016-11-07 01:26:40', '2016-11-07 01:26:44'),
(7, NULL, 'Lượng', 'luong@example.com', NULL, 'male', NULL, 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$JrIbIwtXi48jFdRt905.ceNPNcEsQ8KfzTDq6eh5Nqa/LqfCijt/G', 'B7T8u0hZoUOe0tNbYm7CmTXM93Sqe33LElvWnMmCopZD2xibGdfsoNGmSFfB', '2016-11-07 01:27:23', '2016-11-10 11:23:35'),
(8, NULL, 'Phương', 'phuong@example.com', NULL, 'female', NULL, 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$tShsFPLbfbRlNh6KP1INu.iJccJ5vTRBXcVcXeLOBRbism.7Urm8a', 'mDrSxyf29qBmD9tbrvaiZwlVGdpzeMpD0PQlo3iXXI6NnSyqiSIVNLv7WY5q', '2016-11-07 01:28:24', '2016-11-07 01:28:28'),
(9, NULL, 'Trần Hiếu', 'tranhieu@example.com', NULL, 'male', NULL, 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$NX/WXB0xdr/mE8THX35XJunYWhaGz//uf7fKzOyqHy.g/uXyggTAO', '1ES97bXMA62uWyBGMRsnKMhPR8WoS1K5yhB69fGaxakw1Y865AE5aohsbFFK', '2016-11-07 01:29:17', '2016-11-10 10:42:08'),
(10, NULL, 'Minh', 'minh@example.com', NULL, 'male', NULL, 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$cVlp4MfcRss1pvBWIFA1ButlOaiUqdHoNx1PoWfeNUSfAHdGzWPrW', 'aQ1IeTIf1kXKFXnByOQGH9sjjjtbDLsKb17E5zWElYiezxDDI2jmhu9k8Sw6', '2016-11-07 01:36:08', '2016-11-07 01:36:25'),
(11, NULL, 'Nguyễn Bảo', 'nguyenbao@example.com', NULL, 'male', NULL, 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$RA1I7lEe4EuN2u.chOUA4On4pK54QthEjc2IetcvLI0ueXcPfpaTO', 'adlJiR1XfMc85aw2LuUupVIsMVtAvEi4xKaPNhXpKzslOOyNKWUtE1RyAZUg', '2016-11-07 01:37:08', '2016-11-07 01:37:11'),
(12, NULL, 'Trang Nguyễn', 'trangnguyen@example.com', NULL, 'female', NULL, 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$yhURHm9EEJGP3A6ehCkA5e2JGLIU4awPa/ThCtl8sBqe5OQgAzYua', 'eEOBRoe4BHpwFuFK12ZsEOXnNmgbmGbSfZkPnvtp56cCfMhn0abvGxF1n1mp', '2016-11-07 01:37:58', '2016-11-07 01:41:16'),
(13, NULL, 'Quỳnh', 'quynh@example.com', NULL, 'female', NULL, 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$.OWA2jyPWKnr0yab5x0jLOA7GgfA/G5S6TobqgNOOWekqMaqXq1Zy', 'yp7cpkDOx5v1f9OhRNyPoqGwyeDtdw4hsGCO86D76GyXMVaJ5Lqo9ZRZyI90', '2016-11-07 01:42:25', '2016-11-07 01:46:13'),
(14, NULL, 'Thủy', 'thuy@example.com', NULL, 'female', NULL, 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$tz.o82oYn6EpyNSvqKnoaeZEw3lMeTFJQZvR1eQzGqCR.fSDYom1y', 'rG8NadqYvCaIoxQG5ccfEwpaWZMDC8iiIbRARpJO3ZLVmtKZyvOAa7oMhoQW', '2016-11-07 01:48:29', '2016-11-07 01:51:53'),
(15, NULL, 'Thu', 'thu@example.com', NULL, 'female', NULL, 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$c9Tq7vlJSDgRd63NnHEQQuICKFUdQ98p7j4JfHpL6UyPbPyygcvu.', '8vEAtiR0snIWuR12YurR7Oh0cWviuzZdcp0fBcKahh25a5Lv2A86v2FMC6jy', '2016-11-07 01:54:45', '2016-11-07 01:58:55'),
(16, NULL, 'Hương', 'huong@example.com', NULL, 'female', NULL, 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$6e0IIuK9xdAJPtBRFylJh.ajKafXxHuGASKgLn4aspiPszxDcVp.a', 'FrOw2Yr3IKJ1MYfmOiLcfewRSS6o6uFd9dIjFRY5WUEGU7WitzlpyiZaBDCf', '2016-11-07 01:59:35', '2016-11-07 02:12:47'),
(17, NULL, 'Nhung', 'nhung@example.com', NULL, 'female', NULL, 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$jYxi0AdE/xsbJ.aoC0XM4emWifWbeczUvr4nfelppp1Ul3jT8tkXG', 'iLNXssXzAabWvn1qF2LUMkPUadUiGGxIyPePLs2NobXorV1szY5RgqXNo2Uj', '2016-11-07 02:13:16', '2016-11-07 02:16:40'),
(18, NULL, 'Ngọc', 'ngoc@example.com', NULL, 'female', NULL, 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$c2/a/RcyzDBFPoebjd2Pe.uBQFKpSAciAG6j0lWn62HslR7Dpuf1u', 'cxTyyn3uDP7NecJ7VwHjxtCPhanEDOQT8DOKEk0K3x2O5RlElqz5DAIPFpMu', '2016-11-07 02:17:21', '2016-11-07 02:23:53'),
(19, '65655', 'Leo', 'leo@example.com', NULL, 'female', '08/11/1995', 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$aWQPFqjZlSdD3/JIjovDoOkp6tl6lHc6KIbZdPmlLlH6iOx3y7zue', 'kl2jxDhfmjuxdI3CAprRp6mqjfBjrOczndzNSQ3yOEkTyV0RSWJvpIUkVkNv', '2016-11-10 15:55:42', '2016-11-10 17:36:38');

-- --------------------------------------------------------

--
-- Table structure for table `user_relationship`
--

CREATE TABLE `user_relationship` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_one_id` int(10) UNSIGNED NOT NULL,
  `user_two_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `action_user_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_relationship`
--

INSERT INTO `user_relationship` (`id`, `user_one_id`, `user_two_id`, `status`, `action_user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 1, 1, '2016-11-08 06:09:48', '2016-11-08 16:44:31'),
(2, 1, 3, 1, 1, '2016-11-08 07:20:36', '2016-11-08 17:09:46'),
(3, 1, 4, 1, 1, '2016-11-08 17:10:58', '2016-11-08 17:11:27'),
(4, 1, 5, 1, 1, '2016-11-08 17:11:21', '2016-11-08 17:23:55'),
(6, 1, 6, 1, 1, '2016-11-08 17:24:40', '2016-11-08 17:24:50'),
(7, 1, 8, 1, 1, '2016-11-08 17:24:45', '2016-11-08 17:25:15'),
(8, 1, 9, 0, 1, '2016-11-08 17:28:32', '2016-11-08 17:28:32'),
(9, 1, 11, 1, 1, '2016-11-09 11:46:30', '2016-11-09 11:46:35'),
(10, 1, 12, 1, 1, '2016-11-10 17:01:41', '2016-11-10 17:02:09'),
(11, 7, 1, 2, 1, '2016-11-10 18:00:27', '2016-11-10 18:00:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sender_id` (`sender_id`),
  ADD KEY `receiver_id` (`receiver_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_relationship`
--
ALTER TABLE `user_relationship`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_one_id` (`user_one_id`),
  ADD KEY `user_two_id` (`user_two_id`),
  ADD KEY `action_user_id` (`action_user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `user_relationship`
--
ALTER TABLE `user_relationship`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `messages_ibfk_2` FOREIGN KEY (`receiver_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `user_relationship`
--
ALTER TABLE `user_relationship`
  ADD CONSTRAINT `user_relationship_ibfk_1` FOREIGN KEY (`user_one_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_relationship_ibfk_2` FOREIGN KEY (`user_two_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_relationship_ibfk_3` FOREIGN KEY (`action_user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

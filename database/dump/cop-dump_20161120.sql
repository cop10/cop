-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 20, 2016 at 05:40 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cop`
--

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `title`, `avatar`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'COP', NULL, 1, '2016-11-11 15:43:35', '2016-11-11 15:43:35'),
(2, 'Phát triển phần mềm', NULL, 1, '2016-11-15 21:02:17', '2016-11-15 21:02:17'),
(3, 'Mô hình hóa', NULL, 1, '2016-11-17 17:06:56', '2016-11-17 17:06:56'),
(4, 'Nhóm phần mềm', NULL, 1, '2016-11-18 14:24:58', '2016-11-18 14:24:58');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `sender_id` int(10) UNSIGNED NOT NULL,
  `receiver_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `content`, `sender_id`, `receiver_id`, `created_at`, `updated_at`) VALUES
(1, '12 cung Hoàng Đạo hoàn hảo với 12 cung tương xứng với bốn mùa và 12 tháng. Các cung được phân chia làm bốn nhóm yếu tố (Lửa, Nước, Khí, Đất), mỗi nhóm yếu tố gồm 3 cung đại diện cho các cung có tính cách tương đồng với nhau.', 1, 2, '2016-11-09 16:31:47', '2016-11-09 16:31:47'),
(2, 'mỗi nhóm yếu tố gồm 3 cung đại diện cho các cung có tính cách tương đồng với nhau. ', 1, 2, '2016-11-09 16:32:33', '2016-11-09 16:32:33'),
(3, 'Các cung được phân chia làm bốn nhóm yếu tố (Lửa, Nước, Khí, Đất)', 1, 2, '2016-11-09 23:49:32', '2016-11-09 23:49:32'),
(4, ' mỗi nhóm yếu tố gồm 3 cung đại diện cho các cung có tính cách tương đồng với nhau.', 2, 1, '2016-11-09 23:51:34', '2016-11-09 23:51:34'),
(5, 'Tỳ Hưu là loài linh vật được người dân chọn để thờ cúng cầu sự mong phát tài, phát lộc, thăng quan tiến chức', 1, 2, '2016-11-09 23:55:51', '2016-11-09 23:55:51'),
(6, 'Tăng vận may 12 con giáp với Phi tinh tháng 09/2016 (từ 08/09/2016 đến 06/10/2016 Dương lịch)', 2, 1, '2016-11-09 23:56:24', '2016-11-09 23:56:24'),
(7, 'Hỏa hoạn và các biện pháp phòng ngừa trong Phong thủy', 1, 2, '2016-11-09 23:56:50', '2016-11-09 23:56:50'),
(8, 'Như các bạn cũng đã thấy, hàng năm trên thế giới nói chung và ở Việt Nam nói riêng thì hỏa hoạn luôn rình rập bất cứ nơi đâu trên toàn thế giới.', 2, 1, '2016-11-09 23:57:09', '2016-11-09 23:57:09'),
(9, 'Tính cách biểu hiện qua 12 cung hoàng đạo', 1, 3, '2016-11-10 00:30:25', '2016-11-10 00:30:25'),
(10, 'example', 1, 10, '2016-11-11 15:09:25', '2016-11-11 15:09:25'),
(11, 'example two', 10, 1, '2016-11-11 15:09:41', '2016-11-11 15:09:41'),
(12, 'mỗi nhóm yếu tố gồm 3 cung đại diện cho các cung có tính cách tương đồng với nhau.', 1, 2, '2016-11-11 16:39:12', '2016-11-11 16:39:12'),
(13, 'bhfhd', 1, 6, '2016-11-15 13:58:18', '2016-11-15 13:58:18'),
(14, 'Tính cách không ai ngờ của cô nàng Bảo Bình', 4, 1, '2016-11-18 14:30:42', '2016-11-18 14:30:42'),
(15, 'Khám phá bí mật tài vận của Song Ngư', 1, 4, '2016-11-18 14:30:54', '2016-11-18 14:30:54'),
(16, 'đại diện cho các cung có tính cách tương đồng với nhau. ', 1, 2, '2016-11-18 16:34:00', '2016-11-18 16:34:00'),
(17, 'Tính cách hai trong một của Nhân Mã', 1, 2, '2016-11-18 16:34:16', '2016-11-18 16:34:16'),
(18, 'Khám phá bí mật tài vận của Song Ngư', 2, 1, '2016-11-18 16:35:10', '2016-11-18 16:35:10'),
(19, 'Tính cách không ai ngờ của cô nàng Bảo Bình', 1, 2, '2016-11-18 16:35:21', '2016-11-18 16:35:21'),
(20, 'If the element doesn''t exist, the .each() won''t have anything to iterate over, so you don''t really need to check to see if it exists.\r\n\r\n', 2, 1, '2016-11-18 16:36:57', '2016-11-18 16:36:57'),
(21, 'Click the button to locate where in the string a specifed value occurs.', 1, 2, '2016-11-18 16:37:10', '2016-11-18 16:37:10');

-- --------------------------------------------------------

--
-- Table structure for table `messages_group`
--

CREATE TABLE `messages_group` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `sender_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `messages_group`
--

INSERT INTO `messages_group` (`id`, `content`, `sender_id`, `group_id`, `created_at`, `updated_at`) VALUES
(1, '12 cung Hoàng Đạo hoàn hảo với 12 cung tương xứng với bốn mùa và 12 tháng ', 1, 1, '2016-11-12 16:42:44', '2016-11-12 16:42:44'),
(2, 'ok', 2, 1, '2016-11-12 16:48:28', '2016-11-12 16:48:28'),
(3, 'mỗi nhóm yếu tố gồm 3 cung đại diện cho các cung có tính cách tương đồng với nhau. ', 1, 1, '2016-11-14 10:26:25', '2016-11-14 10:26:25'),
(4, 'Route groups allow you to share route attributes, such as middleware or namespaces.Route groups allow you to share route attributes, such as middleware or namespaces.Route groups allow you to share route attributes, such as middleware or namespaces.Route groups allow you to share route attributes, such as middleware or namespaces.Route groups allow you to share route attributes, such as middleware or namespaces', 1, 2, '2016-11-20 13:48:14', '2016-11-20 13:48:14');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `attachment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `content`, `image`, `attachment`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Cung hoàng đạo là gì? Cùng tìm hiểu về cung hoàng đạo', '<p>Trong&nbsp;<a href="https://vi.wikipedia.org/wiki/Chi%C3%AAm_tinh_h%E1%BB%8Dc">chi&ecirc;m tinh học</a>&nbsp;v&agrave;&nbsp;<a href="https://vi.wikipedia.org/wiki/Thi%C3%AAn_v%C4%83n_h%E1%BB%8Dc">thi&ecirc;n văn học</a>&nbsp;thời cổ,&nbsp;<strong>c&aacute;c cung Ho&agrave;ng Đạo</strong>&nbsp;l&agrave; một v&ograve;ng tr&ograve;n 360o&nbsp;v&agrave; được ph&acirc;n chia l&agrave;m 12 nh&aacute;nh, mỗi nh&aacute;nh tương ứng với một cung, g&oacute;c 30o.&nbsp;<strong>Cung Ho&agrave;ng Đạo</strong>&nbsp;tạo ra bởi c&aacute;c nh&agrave;&nbsp;<a href="https://vi.wikipedia.org/wiki/Chi%C3%AAm_tinh_h%E1%BB%8Dc">chi&ecirc;m tinh học</a>&nbsp;<a href="https://vi.wikipedia.org/wiki/Babylon">Babylon</a>&nbsp;cổ đại từ những năm&nbsp;<a href="https://vi.wikipedia.org/wiki/1645">1645</a>&nbsp;<a href="https://vi.wikipedia.org/wiki/C%C3%B4ng_Nguy%C3%AAn">trước C&ocirc;ng nguy&ecirc;n</a>. V&ograve;ng tr&ograve;n 12 cung Ho&agrave;ng Đạo ho&agrave;n hảo với 12 cung tương xứng với bốn m&ugrave;a v&agrave; 12 th&aacute;ng. C&aacute;c cung được ph&acirc;n chia l&agrave;m bốn nh&oacute;m yếu tố (Lửa, Nước, Kh&iacute;, Đất), mỗi nh&oacute;m yếu tố gồm 3 cung đại diện cho c&aacute;c cung c&oacute; t&iacute;nh c&aacute;ch tương đồng với nhau.</p>\r\n\r\n<p>Theo c&aacute;c nh&agrave;&nbsp;<a href="https://vi.wikipedia.org/wiki/Thi%C3%AAn_v%C4%83n_h%E1%BB%8Dc">thi&ecirc;n văn học</a>&nbsp;thời cổ đại, trong khoảng thời gian chừng 30 - 31&nbsp;<a href="https://vi.wikipedia.org/wiki/Ng%C3%A0y">ng&agrave;y</a>,&nbsp;<a href="https://vi.wikipedia.org/wiki/M%E1%BA%B7t_Tr%E1%BB%9Di">Mặt Trời</a>&nbsp;sẽ đi qua một trong mười hai&nbsp;<a href="https://vi.wikipedia.org/wiki/Ch%C3%B2m_sao">ch&ograve;m sao</a>&nbsp;đặc biệt. Ai sinh ra trong thời gian&nbsp;<a href="https://vi.wikipedia.org/wiki/M%E1%BA%B7t_Tr%E1%BB%9Di">Mặt Trời</a>&nbsp;đi qua ch&ograve;m sao n&agrave;o th&igrave; họ sẽ được ch&ograve;m sao đ&oacute; chiếu mệnh v&agrave; t&iacute;nh c&aacute;ch của họ cũng bị ch&ograve;m sao ảnh hưởng nhiều. 12 ch&ograve;m sao tạo th&agrave;nh 12 cung trong v&ograve;ng tr&ograve;n&nbsp;<strong>Ho&agrave;ng đạo</strong>, c&oacute; nghĩa &quot;Đường đi của mặt trời&quot;. Theo phương T&acirc;y, v&ograve;ng tr&ograve;n n&agrave;y t&ecirc;n l&agrave;&nbsp;<strong>Horoscope</strong>.&nbsp;<a href="https://vi.wikipedia.org/wiki/Ti%E1%BA%BFng_Hy_L%E1%BA%A1p">Tiếng Hy Lạp</a>&nbsp;l&agrave;&nbsp;<em>Zodiakus Kyklos</em>&nbsp;(&zeta;&omega;&delta;&iota;&alpha;&kappa;ό&sigmaf; &kappa;ύ&kappa;&lambda;&omicron;&sigmaf;) - &quot;V&ograve;ng tr&ograve;n của c&aacute;c động vật.&quot;&nbsp;<a href="https://vi.wikipedia.org/wiki/Cung_Ho%C3%A0ng_%C4%90%E1%BA%A1o#cite_note-1">[1]</a></p>\r\n\r\n<p>Nhiều nh&agrave; khoa học hiện đại xem chi&ecirc;m tinh học l&agrave; tr&ograve; m&ecirc; t&iacute;n dị đoan.<a href="https://vi.wikipedia.org/wiki/Cung_Ho%C3%A0ng_%C4%90%E1%BA%A1o#cite_note-2">[2]</a>&nbsp;Tuy nhi&ecirc;n, chi&ecirc;m tinh học vẫn giữ vị tr&iacute; trọng yếu trong nghi&ecirc;n cứu về &quot;số phận đời người&quot; của phương T&acirc;y, vẫn tồn tại ở ngay cả những nước m&agrave; tại đ&oacute; chi&ecirc;m tinh học bị cấm.<a href="https://vi.wikipedia.org/wiki/Cung_Ho%C3%A0ng_%C4%90%E1%BA%A1o#cite_note-3">[3]</a></p>\r\n\r\n<p><strong>Thời kỳ đầu</strong><br />\r\nSự ph&acirc;n chia v&ograve;ng tr&ograve;n ho&agrave;ng đạo ra l&agrave;m 12 c&aacute;nh, mỗi c&aacute;nh một k&iacute; tự được h&igrave;nh th&agrave;nh bởi c&aacute;c nh&agrave; thi&ecirc;n văn&nbsp;<a href="https://vi.wikipedia.org/wiki/Babylon">Babylon</a>&nbsp;cổ đại v&agrave;o&nbsp;<a href="https://vi.wikipedia.org/wiki/Thi%C3%AAn_ni%C3%AAn_k%E1%BB%B7_1_TCN">thi&ecirc;n ni&ecirc;n kỷ 1 TCN</a>. Mỗi c&aacute;nh c&oacute; độ quay 30otương ứng với một th&aacute;ng trong lịch Babylon cổ. C&aacute;c nh&agrave; thi&ecirc;n văn Babylon cũng đặt cho mỗi cung ho&agrave;ng đạo một k&yacute; tự. Trong đ&oacute;, c&aacute;nh đầu ti&ecirc;n c&oacute; t&ecirc;n l&agrave;&nbsp;Bạch Dương&nbsp;(Aries), k&yacute; tự l&agrave; một con cừu n&uacute;i.<a href="https://vi.wikipedia.org/wiki/Cung_Ho%C3%A0ng_%C4%90%E1%BA%A1o#cite_note-4">[4]</a>&nbsp;Dần dần sau n&agrave;y, Ho&agrave;ng Đạo đ&atilde; c&oacute; ảnh hưởng lớn hơn v&agrave;o thời&nbsp;<a href="https://vi.wikipedia.org/wiki/Hy_L%E1%BA%A1p">Hy Lạp</a>&nbsp;v&agrave;&nbsp;<a href="https://vi.wikipedia.org/wiki/%C4%90%E1%BA%BF_qu%E1%BB%91c_La_M%C3%A3">La M&atilde;</a>&nbsp;cổ đại. Việc sử dụng Ho&agrave;ng Đạo để ti&ecirc;n đo&aacute;n về t&iacute;nh c&aacute;ch, sự nghiệp,... v&agrave;o thời k&igrave; n&agrave;y trở n&ecirc;n kh&aacute; phổ biến. N&oacute; c&oacute; ảnh hưởng tới tận v&agrave;o thời Trung Cổ sau n&agrave;y.</p>\r\n\r\n<p><strong>Thời Hy Lạp cổ đại</strong><br />\r\nC&aacute;ch ph&acirc;n chia của Babylon du nhập v&agrave;o chi&ecirc;m tinh học Hy Lạp v&agrave;o thế kỷ 4 TCN.<a href="https://vi.wikipedia.org/wiki/Cung_Ho%C3%A0ng_%C4%90%E1%BA%A1o#cite_note-5">[5]</a><a href="https://vi.wikipedia.org/wiki/Cung_Ho%C3%A0ng_%C4%90%E1%BA%A1o#cite_note-6">[6]</a>&nbsp;Chiếm tinh số mệnh xuất hiện lần đầu tại&nbsp;<a href="https://vi.wikipedia.org/wiki/Ai_C%E1%BA%ADp_thu%E1%BB%99c_Hy_L%E1%BA%A1p">Ai Cập thuộc Hy Lạp</a>. Cung Ho&agrave;ng Đạo Dendera (khoảng năm 50 TCN) l&agrave; m&ocirc; tả sớm nhất về 12 cung Ho&agrave;ng Đạo.</p>\r\n\r\n<p>Đ&oacute;ng vai tr&ograve; quan trọng trong chi&ecirc;m tinh học số mệnh của phương T&acirc;y l&agrave; nh&agrave; thi&ecirc;n văn học ki&ecirc;m chi&ecirc;m tinh học&nbsp;<a href="https://vi.wikipedia.org/wiki/Claudius_Ptolemaeus">Claudius Ptolemaeus</a>&nbsp;với t&aacute;c phẩm&nbsp;<em>Tetrabiblos</em>&nbsp;được xem l&agrave; nền tảng của chi&ecirc;m tinh học phương T&acirc;y.<a href="https://vi.wikipedia.org/wiki/Cung_Ho%C3%A0ng_%C4%90%E1%BA%A1o#cite_note-7">[7]</a></p>\r\n\r\n<p><strong>Mười hai cung Ho&agrave;ng Đạo</strong><br />\r\nĐiểm khởi đầu theo l&yacute; thuyết của cung Bạch Dương l&agrave;&nbsp;<a href="https://vi.wikipedia.org/wiki/Xu%C3%A2n_ph%C3%A2n">xu&acirc;n ph&acirc;n</a>. C&aacute;c cung kh&aacute;c cứ thế nối tiếp. Ng&agrave;y giờ ch&iacute;nh x&aacute;c theo&nbsp;<a href="https://vi.wikipedia.org/wiki/L%E1%BB%8Bch_Gregory">lịch Gregory</a>&nbsp;thường kh&aacute;c biệt ch&uacute;t &iacute;t từ năm n&agrave;y sang năm kh&aacute;c, bởi lẽ lịch Gregory thay đổi tương ứng với&nbsp;<a href="https://vi.wikipedia.org/wiki/N%C4%83m_ch%C3%AD_tuy%E1%BA%BFn">năm ch&iacute; tuyến</a>,<a href="https://vi.wikipedia.org/wiki/Cung_Ho%C3%A0ng_%C4%90%E1%BA%A1o#cite_note-8">[Ghi ch&uacute; 1]</a>&nbsp;trong khi độ d&agrave;i năm ch&iacute; tuyến c&oacute; bản chất thay đổi đều đều. Trong qu&aacute; khứ gần đ&acirc;y v&agrave; tương lai kh&ocirc;ng xa th&igrave; c&aacute;c sai kh&aacute;c n&agrave;y chỉ v&agrave;o khoảng dưới hai ng&agrave;y. Từ năm 1797 đến năm 2043, ng&agrave;y xu&acirc;n ph&acirc;n (theo giờ UT -&nbsp;<em>Universal Time</em>) lu&ocirc;n rơi v&agrave;o ng&agrave;y 20 hoặc 21 th&aacute;ng 3. Ng&agrave;y xu&acirc;n ph&acirc;n từng rơi v&agrave;o ng&agrave;y 19 th&aacute;ng 3, gần đ&acirc;y nhất v&agrave;o năm 1796 v&agrave; lần tới l&agrave; năm 2044.</p>\r\n\r\n<p>Bốn nh&oacute;m Ho&agrave;ng Đạo v&agrave; ng&ocirc;i sao chiếu mệnh của từng cung</p>\r\n\r\n<p><strong>Bốn nh&oacute;m nguy&ecirc;n tố Ho&agrave;ng Đạo</strong><br />\r\nV&ograve;ng tr&ograve;n Ho&agrave;ng đạo chia th&agrave;nh 12 cung, ph&acirc;n ra bốn nguy&ecirc;n tố đ&atilde; tạo ra của thế giới theo quan niệm cổ phương T&acirc;y:&nbsp;<a href="https://vi.wikipedia.org/wiki/%C4%90%E1%BA%A5t">đất</a>,&nbsp;<a href="https://vi.wikipedia.org/wiki/L%E1%BB%ADa">lửa</a>,&nbsp;<a href="https://vi.wikipedia.org/wiki/N%C6%B0%E1%BB%9Bc">nước</a>,&nbsp;<a href="https://vi.wikipedia.org/wiki/Ch%E1%BA%A5t_kh%C3%AD">kh&iacute;</a>. Cứ ba cung l&agrave; được xếp v&agrave;o một nh&oacute;m nguy&ecirc;n tố, những cung c&ugrave;ng chung nh&oacute;m lu&ocirc;n tương hợp nhau nhất. Tuy nhi&ecirc;n, kh&ocirc;ng phải hai cung kh&aacute;c nh&oacute;m l&uacute;c n&agrave;o cũng kị nhau. Điển h&igrave;nh l&agrave; nh&oacute;m Đất c&oacute; thể kết hợp h&agrave;i h&ograve;a c&ugrave;ng nh&oacute;m Nước v&agrave; nh&oacute;m Lửa tương đối th&iacute;ch hợp với nh&oacute;m Kh&iacute;.</p>\r\n\r\n<table align="center" border="1" cellpadding="1" cellspacing="0" style="width:500px">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<p><strong>C&aacute;c nguy&ecirc;n tố</strong></p>\r\n			</td>\r\n			<td><strong>Đầu m&ugrave;a</strong></td>\r\n			<td><strong>Giữa m&ugrave;a</strong></td>\r\n			<td><strong>Cuối m&ugrave;a</strong></td>\r\n		</tr>\r\n		<tr>\r\n			<td>Nguy&ecirc;n tố&nbsp;<strong>Lửa</strong></td>\r\n			<td>Bạch Dương</td>\r\n			<td>Sư Tử</td>\r\n			<td>Nh&acirc;n M&atilde;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Nguy&ecirc;n tố&nbsp;<strong>Đất</strong></td>\r\n			<td>Kim Ngưu</td>\r\n			<td>Xử Nữ</td>\r\n			<td>Ma Kết</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Nguy&ecirc;n tố&nbsp;<strong>Kh&iacute;</strong></td>\r\n			<td>Song Tử</td>\r\n			<td>Thi&ecirc;n B&igrave;nh</td>\r\n			<td>Bảo B&igrave;nh</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Nguy&ecirc;n tố&nbsp;<strong>Nước</strong></td>\r\n			<td>Cự Giải</td>\r\n			<td>Bọ Cạp</td>\r\n			<td>Song Ngư</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Sao chiếu mệnh c&aacute;c cung ho&agrave;ng đạo</strong><br />\r\n12 cung tương ứng với 12&nbsp;<a href="https://vi.wikipedia.org/wiki/Sao">ng&ocirc;i sao</a>&nbsp;v&agrave;&nbsp;<a href="https://vi.wikipedia.org/wiki/H%C3%A0nh_tinh">h&agrave;nh tinh</a>, bao gồm cả&nbsp;<a href="https://vi.wikipedia.org/wiki/Sao_Di%C3%AAm_V%C6%B0%C6%A1ng">Di&ecirc;m Vương Tinh</a>&nbsp;đ&atilde; bị loại khỏi danh s&aacute;ch c&aacute;c h&agrave;nh tinh của&nbsp;<a href="https://vi.wikipedia.org/wiki/H%E1%BB%87_M%E1%BA%B7t_Tr%E1%BB%9Di">hệ Mặt Trời</a>:</p>\r\n\r\n<ol>\r\n	<li>Cung Bạch Dương được&nbsp;<a href="https://vi.wikipedia.org/wiki/Sao_H%E1%BB%8Fa">Hỏa Tinh</a>&nbsp;bảo hộ, tượng trưng cho&nbsp;<a href="https://vi.wikipedia.org/wiki/Th%E1%BA%A7n">thần</a>&nbsp;<a href="https://vi.wikipedia.org/wiki/Chi%E1%BA%BFn_tranh">chiến tranh</a>&nbsp;Mars trong&nbsp;<a href="https://vi.wikipedia.org/wiki/Th%E1%BA%A7n_tho%E1%BA%A1i_La_M%C3%A3">thần thoại La M&atilde;</a>&nbsp;(<a href="https://vi.wikipedia.org/wiki/Ares">Ares</a>&nbsp;của&nbsp;<a href="https://vi.wikipedia.org/wiki/Th%E1%BA%A7n_tho%E1%BA%A1i_Hy_L%E1%BA%A1p">thần thoại Hy Lạp</a>).</li>\r\n	<li>Cung Kim Ngưu được&nbsp;<a href="https://vi.wikipedia.org/wiki/Sao_Kim">Kim Tinh</a>&nbsp;bảo hộ, tượng trưng cho nữ thần&nbsp;<a href="https://vi.wikipedia.org/wiki/Venus">Venus</a>&nbsp;(<a href="https://vi.wikipedia.org/wiki/Aphrodite">Aphrodite</a>&nbsp;của thần thoại Hy Lạp.)</li>\r\n	<li>Cung Song Tử được&nbsp;<a href="https://vi.wikipedia.org/wiki/Th%E1%BB%A7y_Tinh">Thủy Tinh</a>&nbsp;bảo hộ, tượng trưng cho thần đưa tin Mercury (<a href="https://vi.wikipedia.org/wiki/Hermes">Hermes</a>).</li>\r\n	<li>Cung Cự Giải được&nbsp;<a href="https://vi.wikipedia.org/wiki/M%E1%BA%B7t_Tr%C4%83ng">Mặt Trăng</a>&nbsp;bảo hộ, tượng trưng cho nữ thần&nbsp;<a href="https://vi.wikipedia.org/wiki/H%C3%B4n_nh%C3%A2n">h&ocirc;n nh&acirc;n</a>&nbsp;<a href="https://vi.wikipedia.org/wiki/Gia_%C4%91%C3%ACnh">gia đ&igrave;nh</a>&nbsp;Junon (<a href="https://vi.wikipedia.org/wiki/Hera">Hera</a>&nbsp;trong thần thoại Hy Lạp)</li>\r\n	<li>Cung Sư Tử được&nbsp;<a href="https://vi.wikipedia.org/wiki/M%E1%BA%B7t_Tr%E1%BB%9Di">Mặt Trời</a>&nbsp;bảo hộ, tượng trưng cho thần mặt trời&nbsp;<a href="https://vi.wikipedia.org/wiki/Helios">Helios</a>.</li>\r\n	<li>Cung Xử Nữ được&nbsp;<a href="https://vi.wikipedia.org/wiki/Th%E1%BB%A7y_Tinh">Thủy Tinh</a>&nbsp;bảo hộ, tượng trưng cho tr&iacute; tuệ, l&ograve;ng chung thủy, cầu to&agrave;n nguy&ecirc;n tắc. (<a href="https://vi.wikipedia.org/wiki/Demeter">Demeter</a>)</li>\r\n	<li>Cung Thi&ecirc;n B&igrave;nh được&nbsp;<a href="https://vi.wikipedia.org/wiki/Sao_Kim">Kim Tinh</a>&nbsp;bảo hộ, cung n&agrave;y biểu tượng cho sắc đẹp, sự quyến rũ, c&ocirc;ng bằng. Tượng trưng cho nữ thần&nbsp;<a href="https://vi.wikipedia.org/wiki/Venus_(th%E1%BA%A7n_tho%E1%BA%A1i)">Venus</a>.</li>\r\n	<li>Cung Hổ C&aacute;p được&nbsp;<a href="https://vi.wikipedia.org/wiki/Sao_Di%C3%AAm_V%C6%B0%C6%A1ng">Di&ecirc;m Vương Tinh</a>&nbsp;v&agrave;&nbsp;<a href="https://vi.wikipedia.org/wiki/Sao_H%E1%BB%8Fa">Hỏa Tinh</a>&nbsp;bảo hộ, tượng trưng cho Pluto (<a href="https://vi.wikipedia.org/wiki/Hades">Hades</a>), thần cai quan &acirc;m phủ.</li>\r\n	<li>Cung Nh&acirc;n M&atilde; được&nbsp;<a href="https://vi.wikipedia.org/wiki/Sao_M%E1%BB%99c">Mộc Tinh</a>&nbsp;bảo hộ, tượng trưng cho thần&nbsp;<a href="https://vi.wikipedia.org/wiki/Tia_s%C3%A9t">sấm s&eacute;t</a>&nbsp;Jupiter (<a href="https://vi.wikipedia.org/wiki/Zeus">Zeus</a>).</li>\r\n	<li>Cung Ma Kết được&nbsp;<a href="https://vi.wikipedia.org/wiki/Sao_Th%E1%BB%95">Thổ Tinh</a>&nbsp;bảo hộ, tượng trưng cho thần của sụ hủy diệt Saturn (<a href="https://vi.wikipedia.org/wiki/Cronus">Cronos</a>).</li>\r\n	<li>Cung Bảo B&igrave;nh được&nbsp;<a href="https://vi.wikipedia.org/wiki/Sao_Thi%C3%AAn_V%C6%B0%C6%A1ng">Thi&ecirc;n Vương Tinh</a>&nbsp;bảo hộ, tượng trưng cho thần bầu trời&nbsp;<a href="https://vi.wikipedia.org/wiki/Sao_Thi%C3%AAn_V%C6%B0%C6%A1ng">Uranus</a>.</li>\r\n	<li>Cung Song Ngư được&nbsp;<a href="https://vi.wikipedia.org/wiki/Sao_H%E1%BA%A3i_V%C6%B0%C6%A1ng">Hải Vương Tinh</a>&nbsp;bảo hộ, tượng trưng cho thần&nbsp;<a href="https://vi.wikipedia.org/wiki/Bi%E1%BB%83n">biển</a>&nbsp;Neptune (<a href="https://vi.wikipedia.org/wiki/Poseidon">Poseidon</a>).</li>\r\n</ol>\r\n\r\n<p>nguồn:&nbsp;<a href="https://vi.wikipedia.org/wiki/Cung_Ho%C3%A0ng_%C4%90%E1%BA%A1o">https://vi.wikipedia.org</a></p>\r\n', '/uploads/img/1479200866_cung-hoang-dao.png', NULL, 1, '2016-11-15 16:07:46', '2016-11-15 17:04:49'),
(2, 'Khám phá tính cách “chuẩn khỏi chỉnh” của 12 cung hoàng đạo', '<p>Kh&aacute;m ph&aacute; t&iacute;nh c&aacute;ch đặc trưng chuẩn kh&ocirc;ng cần chỉnh của&nbsp;<a href="http://12cunghoangdao.net/">12 cung ho&agrave;ng đạo</a>. Cung Bạch Dương vốn l&agrave; người thẳng thắn, ch&acirc;n thật, chẳng bao giờ biết &ldquo;ủ mưu&rdquo;.Ch&iacute;nh điều đ&oacute; khiến Bạch Dương được rất nhiều người tin tưởng. H&atilde;y c&ugrave;ng 12&nbsp;<a href="http://12cunghoangdao.net/">cung ho&agrave;ng đạo</a>&nbsp;kh&aacute;m ph&aacute; t&iacute;nh c&aacute;ch đặc trưng của&nbsp;<a href="http://12cunghoangdao.net/12-chom-sao">12 ch&ograve;m sao</a>&nbsp;nh&eacute;.</p>\r\n\r\n<p><strong>Bạch Dương (21/3 &ndash; 19/4): Ch&iacute;nh trực, nghĩa kh&iacute;, ấm &aacute;p</strong></p>\r\n\r\n<p>Bạch Dương vốn l&agrave; người thẳng thắn, ch&acirc;n thật, chẳng bao giờ biết &ldquo;ủ mưu&rdquo;. Họ thường rất nghi&ecirc;m t&uacute;c trong chuyện t&igrave;nh cảm v&agrave; coi trọng thể diện. Sợ h&atilde;i sự c&ocirc; đơn, Bạch Dương lu&ocirc;n gắn b&oacute; với gia đ&igrave;nh, th&iacute;ch th&uacute; d&ugrave;ng sự h&agrave;i hước của m&igrave;nh khiến mọi người vui vẻ.</p>\r\n\r\n<p>C&oacute; đ&ocirc;i khi, ẩn sau nụ cười của Bạch Dương kh&ocirc;ng thực sự l&agrave; niềm vui. Họ d&ugrave;ng khu&ocirc;n mặt cười để che giấu tất cả. Họ kh&ocirc;ng muốn bất cứ ai phải lo lắng, cũng kh&ocirc;ng cần bất kỳ sự đồng cảm hay thương x&oacute;t n&agrave;o. Những l&uacute;c đ&oacute;, bạn n&ecirc;n d&agrave;nh tặng họ một c&aacute;i &ocirc;m sẻ chia, một bờ vai để dựa dẫm v&agrave; đ&ocirc;i lời an ủi thật l&ograve;ng.</p>\r\n\r\n<p>Sự ấm &aacute;p bề ngo&agrave;i của Bạch Dương nhiều khi khiến người kh&aacute;c tưởng rằng họ rất dễ bị lừa gạt. Tr&ecirc;n thực tế, cung Ho&agrave;ng đạo n&agrave;y &ldquo;nguy hiểm&rdquo; hơn rất nhiều. Tuy nhi&ecirc;n, họ vẫn đặt tr&aacute;i tim l&ecirc;n trước những t&iacute;nh to&aacute;n, nghi ngờ của m&igrave;nh. Sự m&acirc;u thuẫn đ&oacute; nhiều l&uacute;c khiến Bạch Dương bị tổn thương. Họ hay nghĩ ngợi linh tinh, cũng hay chạy trốn để che giấu vết thương khi t&ocirc;n nghi&ecirc;m bị x&uacute;c phạm, thậm ch&iacute; l&agrave; đẩy bản th&acirc;n đến b&ecirc;n bờ tuyệt vọng.</p>\r\n\r\n<p><strong>Kim Ngưu (20/4 &ndash; 20/5): Thật th&agrave;, lương thiện, dễ mềm l&ograve;ng, hướng nội</strong></p>\r\n\r\n<p>Kim Ngưu thuộc tu&yacute;p người lu&ocirc;n b&agrave;y tỏ mọi cảm x&uacute;c của m&igrave;nh ra ngo&agrave;i kh&ocirc;ng giấu diếm. Tuy nhi&ecirc;n c&aacute;ch thể hiện cảm x&uacute;c của họ thường kh&ocirc;ng kh&eacute;o l&eacute;o, kh&ocirc;ng mềm mại, khiến mọi người kh&oacute; chịu hoặc hiểu lầm. Mỗi khi c&atilde;i nhau, lời n&oacute;i của Kim Ngưu c&oacute; thể l&agrave;m tổn thương đối phương đến cực điểm. Khi vui vẻ, người ta sẽ thấy Kim Ngưu n&oacute;i n&oacute;i cười cười nhưng một khi đ&atilde; tức giận rồi th&igrave; một c&acirc;u họ cũng chẳng th&egrave;m mở miệng.</p>\r\n\r\n<p>Điểm yếu lớn nhất của Kim Ngưu l&agrave; dễ mềm l&ograve;ng. Chỉ cần đối phương chịu c&uacute;i đầu n&oacute;i xin lỗi v&agrave; xin tha thứ th&igrave; cho d&ugrave; lỗi lầm g&igrave; họ cũng c&oacute; thể bỏ qua. Sợ nhất l&agrave; phải thấy người kh&aacute;c rơi nước mắt, Kim Ngưu khi đ&oacute; kh&ocirc;ng những x&oacute;t thương cảm th&ocirc;ng m&agrave; thậm ch&iacute; c&ograve;n đồng &yacute; ngay tắp lự mọi y&ecirc;u cầu của người kia, k&egrave;m theo lời dỗ d&agrave;nh an ủi.</p>\r\n\r\n<p>Thường ng&agrave;y, t&iacute;nh kh&iacute; Kim Ngưu được đ&aacute;nh gi&aacute; kh&aacute; tốt bởi họ c&oacute; khả năng kiểm so&aacute;t t&acirc;m trạng của m&igrave;nh. Họ cũng kh&aacute; &yacute; thức việc bảo vệ thể diện v&agrave; tự t&ocirc;n của người kh&aacute;c nữa. Kim Ngưu thuộc tu&yacute;p người hướng nội. Họ kh&ocirc;ng th&iacute;ch những nơi xa lạ, cũng chẳng hứng th&uacute; với việc tụ tập đ&ocirc;ng vui m&agrave; chỉ mong được một m&igrave;nh lặng y&ecirc;n suy nghĩ về cuộc đời.</p>\r\n\r\n<p><strong>Song Tử (21/5 &ndash; 21/6): &ldquo;Tự kỷ&rdquo;, h&agrave;i hước, y&ecirc;u thương cuồng nhiệt</strong></p>\r\n\r\n<p>Người sinh ra dưới cung Song Tử thường trưởng th&agrave;nh rất sớm. Họ c&oacute; sở th&iacute;ch kh&aacute; &ldquo;dị&rdquo; đ&oacute; l&agrave; được ở một m&igrave;nh. Chỉ khi một m&igrave;nh, Song Tử mới cảm thấy thoải m&aacute;i, vui vẻ, mặc sức &ldquo;tự kỷ&rdquo;, suy tư. Ch&iacute;nh t&iacute;nh c&aacute;ch lạ l&ugrave;ng n&agrave;y khiến cho nhiều người cảm thấy Song Tử thật c&ocirc; đơn, c&ocirc; độc.</p>\r\n\r\n<p>Song Tử l&agrave; người c&oacute; nhiều mong muốn trong t&igrave;nh y&ecirc;u nhưng lại biết c&aacute;ch che giấu những mong mỏi đ&oacute; của m&igrave;nh. Ch&iacute;nh v&igrave; lẽ đ&oacute; m&agrave; họ thường bị hiểu lầm l&agrave; giả d&ocirc;́i, chỉ người trong cuộc mới biết họ lu&ocirc;n l&agrave; người trao đi ch&acirc;n t&igrave;nh nhưng lại bị ch&agrave; đạp phũ ph&agrave;ng. Người ta chỉ tr&iacute;ch Song Tử l&agrave; kẻ đa t&igrave;nh, lăng nhăng nhưng kỳ thực đối với t&igrave;nh y&ecirc;u họ cố chấp v&ocirc; c&ugrave;ng. D&ugrave; cuộc sống biến đổi thế n&agrave;o, Song Tử vẫn giữ th&aacute;i độ cuồng nhiệt, say đắm với &aacute;i t&igrave;nh. Họ y&ecirc;u rất s&acirc;u nhưng &iacute;t khi n&oacute;i ra. T&igrave;nh y&ecirc;u của họ thường được thể hiện ra ngo&agrave;i sau khi đ&atilde; &ldquo;phủ&rdquo; th&ecirc;m &ldquo;lớp&rdquo; vỏ h&agrave;i hước, nh&iacute; nhố, vui tươi. Trong t&igrave;nh cảm họ linh hoạt, nhanh nhẹn, biết ph&aacute;t huy sức mạnh để gi&agrave;nh thế chủ động về ph&iacute;a m&igrave;nh.</p>\r\n\r\n<p>Sự hiểu biết v&agrave; năng khiếu h&agrave;i hước của Song Tử khiến kh&ocirc;ng &iacute;t người phải ngưỡng mộ, ghen tị. Nếu được l&agrave; một nửa của Song Tử, bạn sẽ như ch&uacute; chim dưới khung trời ng&aacute;t xanh hạnh ph&uacute;c, cả cuộc đời này kh&ocirc;ng biết đến nỗi buồn hay ch&aacute;n nản.</p>\r\n\r\n<p><strong>Cự Giải (22/6 &ndash; 22/7): Nhạy cảm, trọng thể diện, gi&agrave;u đức hi sinh</strong></p>\r\n\r\n<p>V&igrave; tự ti n&ecirc;n trong chuyện t&igrave;nh cảm Cự Giải rất dễ bị tổn thương. Nhưng họ cũng ch&iacute;nh l&agrave; mẫu người c&oacute; thể v&igrave; một chuyện si&ecirc;u b&eacute; nhỏ giản đơn m&agrave; cảm thấy hạnh ph&uacute;c, thỏa m&atilde;n. Cự Giải rất kh&oacute; mở l&ograve;ng, đặt trọn niềm tin v&agrave; t&igrave;nh y&ecirc;u ở một người. Họ dễ x&uacute;c động, mau nước mắt v&agrave; một khi đ&atilde; rơi v&agrave;o trạng th&aacute;i bi thương sầu thảm th&igrave; rất kh&oacute; tho&aacute;t ra. Họ sẵn s&agrave;ng hy sinh bản th&acirc;n v&igrave; người kh&aacute;c, dốc sức dốc l&ograve;ng v&igrave; một mối quan hệ.</p>\r\n\r\n<p>Đ&acirc;y c&ograve;n l&agrave; những người kh&ocirc;ng th&iacute;ch qua lại với người c&oacute; t&iacute;nh c&aacute;ch v&agrave; nh&acirc;n phẩm kh&ocirc;ng tốt bởi tr&aacute;i tim hiền l&agrave;nh lương thiện của họ kh&ocirc;ng th&iacute;ch nghi nổi với những người lừa gạt hay xảo tr&aacute;. Trọng thể diện v&ocirc; c&ugrave;ng, thế n&ecirc;n trong một số trường hợp Cự Giải sẵn s&agrave;ng hi sinh thể x&aacute;c để bảo vệ phẩm chất của m&igrave;nh.</p>\r\n\r\n<p>Cự Giải kh&ocirc;ng hay n&oacute;i lời từ chối, cũng chẳng hề biết dụng mưu. Cảm gi&aacute;c khi từ chối ai đ&oacute; khiến họ &aacute;y n&aacute;y v&agrave; thấy tội lỗi v&ocirc; c&ugrave;ng n&ecirc;n Cua thường chỉ d&aacute;m n&oacute;i &ldquo;kh&ocirc;ng&rdquo; với những người th&acirc;n thiết nhất.</p>\r\n\r\n<p><strong>Sư Tử (23/7 &ndash; 22/8): Ki&ecirc;u h&atilde;nh, dứt kho&aacute;t, &ldquo;cư xử đẹp&rdquo; với bạn b&egrave;</strong></p>\r\n\r\n<p>Sư Tử rất ngại việc phải lựa chọn hoặc đưa ra quyết định. Họ th&iacute;ch nghe &yacute; kiến v&agrave; những ph&acirc;n t&iacute;ch đ&aacute;nh gi&aacute; của bạn b&egrave; về một vấn đề sau đ&oacute; mới quyết theo c&aacute;ch của m&igrave;nh. Điểm kh&aacute;c biệt giữa Sư Tử v&agrave; Thi&ecirc;n B&igrave;nh đ&oacute; l&agrave; một khi đ&atilde; quyết sẽ chẳng c&oacute; ai hay điều g&igrave; c&oacute; thể l&agrave;m Sư Tử hồi t&acirc;m chuyển &yacute;.</p>\r\n\r\n<p>Nụ cười của người thuộc cung Sư Tử giản đơn, ch&acirc;n th&agrave;nh, thuần khiết nhất tr&ecirc;n đời. Họ lu&ocirc;n cư xử với mọi người bằng th&aacute;i độ t&iacute;ch cực trong khi những đau thương Sư Tử giữ lại cho m&igrave;nh. Rất &iacute;t người c&oacute; may mắn được tr&ocirc;ng thấy nước mắt của người thuộc cung Sư Tử bởi niềm ki&ecirc;u h&atilde;nh kh&ocirc;ng cho ph&eacute;p họ rơi lệ trước mặt ai kh&aacute;c. Họ ki&ecirc;u h&atilde;nh v&agrave; trọng thể diện v&ocirc; c&ugrave;ng.</p>\r\n\r\n<p>Sư Tử thường c&oacute; nhiều bạn b&egrave; nhờ t&iacute;nh c&aacute;ch vui vẻ ph&oacute;ng kho&aacute;ng c&ugrave;ng t&agrave;i ăn n&oacute;i đầy t&iacute;nh thuyết phục của m&igrave;nh. Bạn sẽ rất hạnh ph&uacute;c nếu được Sư Tử coi l&agrave; bạn. Họ lu&ocirc;n đối xử với bạn hữu như với anh chị em, sẵn s&agrave;ng cho đi m&agrave; kh&ocirc;ng cần đ&aacute;p lại. Th&agrave; để người phụ m&igrave;nh, Sư Tử nhất quyết kh&ocirc;ng phụ người.</p>\r\n\r\n<p><strong>Xử Nữ (23/8 &ndash; 23/9): Rất sợ c&ocirc; đơn</strong></p>\r\n\r\n<p>Vẻ ngo&agrave;i lạnh l&ugrave;ng, trầm mặc, gh&ecirc; gớm nhưng kỳ thực Xử Nữ l&agrave; người c&oacute; nội t&acirc;m đặc biệt s&acirc;u sắc, chung thủy, căm gh&eacute;t sự phản bội. Với kẻ th&ugrave;, cung Ho&agrave;ng đạo n&agrave;y kh&ocirc;ng bao giờ nương tay nhưng khi ở cạnh những người y&ecirc;u thương, họ lại hay ho&agrave;i nghi, bất an, thiếu tự tin. Xử Nữ cũng hay c&oacute; th&oacute;i quen soi m&oacute;i, x&eacute;t n&eacute;t chỉ tr&iacute;ch kh&aacute; cay nghiệt. Tuy vậy, họ l&agrave; người &ldquo;khẩu x&agrave; t&acirc;m phật&rdquo;, kh&ocirc;ng đối tốt với bạn sẽ chẳng c&oacute; chuyện họ ph&ecirc; ph&aacute;n gay gắt đến thế. Xử Nữ kh&ocirc;ng th&iacute;ch gần gũi người lạ, nhưng đối với người th&acirc;n lại th&iacute;ch &ldquo;d&iacute;nh như sam&rdquo;, quan t&acirc;m chăm s&oacute;c tận t&igrave;nh.</p>\r\n\r\n<p>Một khi đ&atilde; d&agrave;nh t&igrave;nh cảm cho ai, Xử Nữ sẽ tận tụy, cống hiến, ở cạnh người đ&oacute; đến c&ugrave;ng. Vậy n&ecirc;n t&igrave;nh y&ecirc;u của Xử Nữ phần lớn l&agrave; t&igrave;nh cảm từ một ph&iacute;a, thầm lặng, buồn b&atilde;, đau thương. Cảm x&uacute;c khi y&ecirc;u của Xử Nữ kh&ocirc;ng hề cứng nhắc hay ngh&egrave;o n&agrave;n, nhưng c&aacute;i họ thiếu đ&oacute; l&agrave; ngọn lửa nhiệt t&igrave;nh để lan tỏa những x&uacute;c cảm đ&oacute; ra ngo&agrave;i.</p>\r\n\r\n<p>Người sinh ra dưới cung Xử Nữ hấp dẫn người kh&aacute;c bằng sự uy&ecirc;n b&aacute;c của m&igrave;nh. Bản th&acirc;n họ lu&ocirc;n chơi vơi giữa mạng lưới cảm x&uacute;c cực kỳ phức tạp do ch&iacute;nh bản th&acirc;n tạo n&ecirc;n.</p>\r\n\r\n<p><strong>Thi&ecirc;n B&igrave;nh (24/9 &ndash; 23/10): Thanh lịch, c&ocirc;ng bằng, cần c&oacute; người đồng h&agrave;nh</strong></p>\r\n\r\n<p>Thi&ecirc;n B&igrave;nh trời sinh đ&atilde; sở hữu vẻ ngo&agrave;i nho nh&atilde; c&ugrave;ng t&agrave;i năng ưu việt, bản th&acirc;n họ cũng th&iacute;ch được mọi người ngưỡng mộ, t&aacute;n dương. Thi&ecirc;n B&igrave;nh hầu như chẳng bao giờ tranh gi&agrave;nh, cướp giật của ai thứ g&igrave;, cũng chẳng b&agrave;y tỏ sự tức giận, cay c&uacute; khi m&oacute;n đồ thuộc quyền sở hữu bị người kh&aacute;c tước đoạt. Với họ, những h&agrave;nh động cũng như cảm x&uacute;c đ&oacute; thật yếu đuối, đ&aacute;ng xấu hổ v&agrave; một khi thể hiện ra ngo&agrave;i sẽ ngay lập tức ph&aacute; hủy h&igrave;nh tượng ổn định, thanh tao m&agrave; họ x&acirc;y dựng cho m&igrave;nh.</p>\r\n\r\n<p>Thi&ecirc;n B&igrave;nh l&agrave;m g&igrave; cũng muốn c&oacute; bạn đồng h&agrave;nh. Họ gh&eacute;t sự c&ocirc; độc v&agrave; cả cuộc đời lu&ocirc;n mong mỏi t&igrave;m cho m&igrave;nh một bến đỗ b&igrave;nh y&ecirc;n. T&igrave;m được rồi, Thi&ecirc;n B&igrave;nh kh&ocirc;ng ngần ngại việc gắn b&oacute; b&ecirc;n người ấy, đạp bằng mọi thử th&aacute;ch d&ugrave; l&agrave; khoảng c&aacute;ch địa l&yacute; hay tuổi t&aacute;c, thời gian.</p>\r\n\r\n<p>Những người sinh ra dưới cung Ho&agrave;ng đạo gi&agrave;u l&ograve;ng trắc ẩn n&agrave;y &iacute;t khi &ldquo;cậy&rdquo; c&oacute; khả năng ăn n&oacute;i kh&eacute;o l&eacute;o để bắt nạt người kh&aacute;c. Khi giận dữ họ vẫn chọn c&aacute;ch im lặng hoặc n&oacute;i chuyện hết sức nhẹ nh&agrave;ng v&igrave; kh&ocirc;ng muốn khiến đối phương đau l&ograve;ng.</p>\r\n\r\n<p><strong>Thần N&ocirc;ng (24/10 &ndash; 22/11): &ldquo;Khẩu x&agrave; t&acirc;m Phật&rdquo;, cảm x&uacute;c cực đoan</strong></p>\r\n\r\n<p>Thiếu cảm gi&aacute;c an to&agrave;n, sợ tổn thương, vậy n&ecirc;n khi ngủ Thần N&ocirc;ng hầu như chẳng l&uacute;c n&agrave;o rời xa th&uacute; b&ocirc;ng hay chiếc gối &ocirc;m mềm mại. Đừng để vẻ bề ngo&agrave;i gh&ecirc; gớm, ki&ecirc;n cường của họ đ&aacute;nh lừa, đ&oacute; thực chất chỉ l&agrave; lớp vỏ ngụy trang cho một nội t&acirc;m yếu đuối, nhạy cảm. Thế giới ri&ecirc;ng của một Thần N&ocirc;ng c&oacute; đủ mọi loại cung bậc cảm x&uacute;c từ hạnh ph&uacute;c tột c&ugrave;ng để bi thương cực đỉnh.</p>\r\n\r\n<p>Thần N&ocirc;ng l&agrave; kiểu người &ldquo;khẩu x&agrave; t&acirc;m Phật&rdquo;, thường bu&ocirc;ng lời lẽ cay độc d&ugrave; trong l&ograve;ng y&ecirc;u mến, tr&acirc;n trọng v&ocirc; c&ugrave;ng. Họ l&agrave; người hiểu chuyện, kiểm so&aacute;t cảm x&uacute;c cũng tốt nhưng ai bảo đ&atilde; tr&oacute;t kho&aacute;c l&ecirc;n m&igrave;nh lớp mặt nạ hằn học với cuộc đời? Trong t&igrave;nh y&ecirc;u, Thần N&ocirc;ng l&agrave; những người kh&oacute; rung động, kh&oacute; tin tưởng, kh&oacute; mở l&ograve;ng nhưng một khi đ&atilde; y&ecirc;u ai th&igrave; chẳng dễ g&igrave; bỏ cuộc bu&ocirc;ng tay. Vẻ ngo&agrave;i tỉnh t&aacute;o, lạnh l&ugrave;ng khi ấy sẽ từ từ được b&oacute;c t&aacute;ch, phơi b&agrave;y một tr&aacute;i tim cuồng nhiệt, ấm &aacute;p, ch&acirc;n th&agrave;nh, chung thủy v&ocirc; c&ugrave;ng.</p>\r\n\r\n<p>Nếu bị phản bội, Thần N&ocirc;ng ngay lập tức sẽ biến th&agrave;nh một quả bom đ&iacute;ch thực, bởi họ l&agrave; người y&ecirc;u gh&eacute;t đều cực điểm, cực đoan. Sau n&agrave;y khi mọi chuyện đ&atilde; qua đi, bất kể bạn dốc sức cứu v&atilde;n thế n&agrave;o cũng đừng mong c&oacute; được niềm tin từ Thần N&ocirc;ng một lần nữa&hellip;</p>\r\n\r\n<p><strong>Nh&acirc;n M&atilde; (23/11 &ndash; 21/12): Lu&ocirc;n cảm thấy c&ocirc; đơn giữa v&ograve;ng tr&ograve;n b&egrave; bạn</strong></p>\r\n\r\n<p>Bẩm sinh đ&atilde; c&oacute; nh&acirc;n duy&ecirc;n tốt, M&atilde; M&atilde; lại c&agrave;ng được mọi người y&ecirc;u qu&yacute; nhờ sự &ldquo;mồm m&eacute;p&rdquo;, l&aacute;u c&aacute; của m&igrave;nh. Nh&acirc;n M&atilde; h&agrave;o ph&oacute;ng, y&ecirc;u tự do, th&iacute;ch giao tiếp n&ecirc;n cảm gi&aacute;c m&agrave; họ gh&eacute;t bỏ nhất l&agrave; khi phải một m&igrave;nh trơ trọi c&ocirc; đơn ở một nơi trống trải. Nhiều bạn b&egrave; l&agrave; thế nhưng mỗi khi gặp chuyện, Nh&acirc;n M&atilde; thường chỉ biết một m&igrave;nh gặm nhấm nỗi buồn. Đau đến nhỏ m&aacute;u, họ vẫn sẽ gắng gượng mỉm cười.</p>\r\n\r\n<p>Khi bắt đầu một đoạn t&igrave;nh cảm, Nh&acirc;n M&atilde; đ&agrave;o hoa thường chỉ trao đi một phần nhỏ con tim m&igrave;nh. Trải qua một khoảng thời gian d&agrave;i với mu&ocirc;n v&agrave;n thử th&aacute;ch, họ mới chịu tin tưởng, gắn b&oacute;, cam kết với một người n&agrave;o đ&oacute;. Đừng lo lắng nếu Nh&acirc;n M&atilde; của bạn cứ mải m&ecirc; khắp chốn rong chơi. Đ&uacute;ng l&agrave; họ cực kỳ ham m&ecirc; mạo hiểm, trải nghiệm v&agrave; t&ocirc;n thờ tự do nhưng Nh&acirc;n M&atilde; cũng rất y&ecirc;u qu&yacute; m&aacute;i ấm gia đ&igrave;nh của m&igrave;nh. Họ lu&ocirc;n cần một nơi b&igrave;nh y&ecirc;n để trở về sau mỗi chuyến th&aacute;m hiểm..</p>\r\n\r\n<p><strong>Ma Kết (22/12 &ndash; 19/1): Nội t&acirc;m dữ dội ẩn sau vẻ ngo&agrave;i lạnh l&ugrave;ng</strong></p>\r\n\r\n<p>Ma Kết th&iacute;ch được &ldquo;một m&igrave;nh một thế giới&rdquo; để mặc sức ngẫm nghĩ về cuộc sống. Những biến động trong thế giới nội t&acirc;m của một Ma Kết thường đến bất chợt v&agrave; mỗi khi xuất hiện đều kh&aacute; dữ dội. Họ c&oacute; thể đang vui tươi đột nhi&ecirc;n trở n&ecirc;n trầm mặc v&igrave; một &yacute; nghĩ tho&aacute;ng qua, để rồi lại c&oacute; thể bất chợt cười vui rạng rỡ.</p>\r\n\r\n<p>Ma Kết c&oacute; tấm l&ograve;ng lương thiện, biết cảm th&ocirc;ng. Họ lu&ocirc;n đem lại cảm gi&aacute;c an to&agrave;n, đ&aacute;ng tin cậy cho người đối diện. Ma Kết cũng th&iacute;ch những thứ khiến tr&aacute;i tim băng gi&aacute; c&ocirc; đơn của họ trở n&ecirc;n ấm &aacute;p, được chở che. Do trưởng th&agrave;nh sớm n&ecirc;n c&aacute;c Ma Kết lu&ocirc;n biết r&otilde; m&igrave;nh muốn g&igrave;. Họ cũng sẽ kh&ocirc;ng ngừng cố gắng để đạt được điều đ&oacute;, đặc biệt l&agrave; khi đ&atilde; lập gia đ&igrave;nh.</p>\r\n\r\n<p>C&aacute;c Ma Kết &ldquo;ghi điểm&rdquo; trong mắt người kh&aacute;c nhờ khả năng điều h&agrave;nh v&agrave; quản l&yacute; xuất sắc của m&igrave;nh. Nh&igrave;n lạnh l&ugrave;ng vậy th&ocirc;i, họ l&agrave; những người v&ocirc; c&ugrave;ng nhạy cảm. Đừng dại dột mang t&igrave;nh cảm của Ma Kết ra đ&ugrave;a để rồi nhận lại sự căm hận đến lạnh nhạt của một người y&ecirc;u gh&eacute;t r&otilde; r&agrave;ng như họ.</p>\r\n\r\n<p><strong>Bảo B&igrave;nh (20/1 &ndash; 18/2): Ki&ecirc;n định, hết m&igrave;nh trong t&igrave;nh y&ecirc;u nhưng cũng l&yacute; tr&iacute;, tỉnh t&aacute;o kh&ocirc;ng k&eacute;m</strong></p>\r\n\r\n<p>C&oacute; Bảo B&igrave;nh ở cạnh đồng nghĩa với việc &ldquo;l&agrave;m bạn&rdquo; c&ugrave;ng những cảm gi&aacute;c mới mẻ, vui tươi. Bảo B&igrave;nh c&oacute; t&iacute;nh c&aacute;ch kh&aacute; t&ugrave;y hứng, bốc đồng nhưng đứng trước t&igrave;nh bạn, t&igrave;nh y&ecirc;u, họ c&oacute; thể trở th&agrave;nh một người ki&ecirc;n nhẫn, cố chấp đến kh&oacute; hiểu. Một khi đ&atilde; trao t&igrave;nh cảm cho ai, đừng h&ograve;ng Bảo B&igrave;nh bỏ cuộc, chạy trốn, chia li d&ugrave; đối phương chẳng c&ograve;n y&ecirc;u m&igrave;nh. Họ cũng chẳng ngại ngần việc để lộ t&acirc;m trạng yếu đuối si t&igrave;nh trước mặt người kh&aacute;c.</p>\r\n\r\n<p>Si t&igrave;nh l&agrave; vậy nhưng Bảo B&igrave;nh lu&ocirc;n biết đ&acirc;u l&agrave; giới hạn của m&igrave;nh. Khi cảm thấy đ&atilde; dốc to&agrave;n lực nhưng t&igrave;nh h&igrave;nh vẫn kh&ocirc;ng thể cứu v&atilde;n, họ sẽ để đối phương được như &yacute; muốn. Sau đ&oacute; Bảo B&igrave;nh nguyện một m&igrave;nh hứng chịu mọi buồn b&atilde; c&ocirc; đơn. Ngay khi bắt đầu y&ecirc;u, người thuộc cung Bảo B&igrave;nh đ&atilde; x&aacute;c định &aacute;i t&igrave;nh ch&iacute;nh l&agrave; một &ldquo;canh bạc&rdquo;. Một l&agrave; c&oacute; được tất cả, hai l&agrave; &ldquo;trắng tay&rdquo; nhưng họ vẫn dũng cảm trao tr&aacute;i tim đi chẳng giữ lại điều g&igrave;.</p>\r\n\r\n<p><strong>Song Ngư (19/2 &ndash; 20/3): Nhạy cảm, th&aacute;nh thiện, sợ h&atilde;i sự c&ocirc; đơn</strong></p>\r\n\r\n<p>Song Ngư l&agrave; cung Ho&agrave;ng đạo cực kỳ nhạy cảm. Họ c&oacute; khả năng nhận thức mọi thay đổi d&ugrave; nhỏ b&eacute; tinh tế nhất nhờ gi&aacute;c quan thứ S&aacute;u cực nhạy. Song Ngư gh&eacute;t sự giả dối nhưng l&ograve;ng tốt v&ocirc; hạn kh&ocirc;ng cho ph&eacute;p họ từ chối bất cứ ai d&ugrave; l&iacute; tr&iacute; c&oacute; nghi ngờ.</p>\r\n\r\n<p>Chinh phục th&agrave;nh c&ocirc;ng tr&aacute;i tim Song Ngư, bạn sẽ c&oacute; được một người y&ecirc;u tận tụy, sẵn s&agrave;ng thay đổi bản th&acirc;n v&igrave; t&igrave;nh y&ecirc;u của m&igrave;nh. Tuy nhi&ecirc;n h&atilde;y chủ động từ bỏ nếu ngay từ đầu bạn đ&atilde; kh&ocirc;ng c&oacute; &yacute; định gắn b&oacute; với họ l&acirc;u d&agrave;i, Song Ngư với t&acirc;m hồn trong s&aacute;ng, ng&acirc;y thơ kh&ocirc;ng đ&aacute;ng bị đối xử như v&acirc;̣y.</p>\r\n\r\n<p>Người sinh ra dưới ch&ograve;m sao Song Ngư cực kỳ sợ c&ocirc; đơn. Họ lu&ocirc;n mong được c&oacute; được một chỗ dựa vững chắc. Đ&ocirc;i l&uacute;c họ cũng kh&aacute; cố chấp trong việc theo đuổi mơ ước của ri&ecirc;ng m&igrave;nh. Sự&hellip; ấu trĩ đ&oacute; thật ra cũng rất đ&aacute;ng y&ecirc;u, người c&oacute; ước mơ mới l&agrave; người c&oacute; cuộc sống tốt đẹp.</p>\r\n', '/uploads/img/1479206697_cung-hoang-dao.png', NULL, 1, '2016-11-15 17:44:57', '2016-11-15 17:44:57'),
(3, 'Sự lãng mạn siêu đáng yêu của nàng Bạch Dương', '<p>Sự l&atilde;ng mạn si&ecirc;u đ&aacute;ng y&ecirc;u của n&agrave;ng Bạch Dương. Bạch Dương l&agrave; c&ocirc; g&aacute;i c&oacute; c&aacute; t&iacute;nh mạnh mẽ, quyết đo&aacute;n nhưng cũng si&ecirc;u đ&aacute;ng y&ecirc;u v&agrave; l&atilde;ng mạn đấy.</p>\r\n\r\n<p>Sự c&aacute; t&iacute;nh, thẳng thắn của con g&aacute;i Bạch Dương vốn nổi tiếng gần xa rồi. C&aacute;c n&agrave;ng l&agrave;m bạn th&igrave; nhiệt t&igrave;nh, h&agrave;o ph&oacute;ng, chịu chơi; l&agrave;m người y&ecirc;u th&igrave; cuồng nhiệt, hết m&igrave;nh. Mọi người bị thu h&uacute;t bởi tinh thần ti&ecirc;n phong v&agrave; nhiệt huyết trong cuộc sống. Chỉ cần nh&igrave;n thấy họ th&ocirc;i l&agrave; đ&atilde; thấy sức sống bừng bừng rồi.</p>\r\n\r\n<p>C&ocirc; n&agrave;ng rất thẳng thắn trong vấn đề t&igrave;nh cảm, kh&ocirc;ng chấp mối quan hệ nhập nhằng, mập mờ hay sự kh&ocirc;ng th&agrave;nh thật. Với nữ Bạch Dương, t&igrave;nh y&ecirc;u kh&ocirc;ng phải l&agrave; tất cả, n&agrave;ng c&oacute; thể sống tốt m&agrave; chẳng cần sự hiện diện của đ&agrave;n &ocirc;ng trong cuộc sống. N&agrave;ng c&oacute; năng lực, tự do v&agrave; đầy th&uacute; vui. Lao m&igrave;nh v&agrave;o c&ocirc;ng việc, thả tr&ocirc;i theo những cuộc chơi hay đơn giản l&agrave; tận hưởng cuộc sống đa m&agrave;u sắc.</p>\r\n\r\n<p>Nhưng s&acirc;u b&ecirc;n trong sự quyết liệt ấy, n&agrave;ng khao kh&aacute;t t&igrave;nh cảm v&agrave; mong c&oacute; người b&ecirc;n cạnh sẻ chia vui buồn trong cuộc sống. Hơn thế nữa, bản chất của c&ocirc; n&agrave;ng Bạch Dương rất l&atilde;ng mạn, kh&ocirc;ng y&ecirc;u th&igrave; th&ocirc;i nhưng c&oacute; t&igrave;nh cảm l&agrave; hết m&igrave;nh, kh&ocirc;ng t&iacute;nh to&aacute;n hơn thiệt.</p>\r\n\r\n<p>Sự dịu d&agrave;ng của Bạch Dương chỉ d&agrave;nh cho người họ y&ecirc;u. Những cử chỉ quan t&acirc;m, vẻ mặt đ&aacute;ng y&ecirc;u, sở th&iacute;ch trẻ con. Tất cả l&agrave; của ch&agrave;ng. Ngược lại, Bạch Dương cũng muốn đối phương l&agrave;m cho m&igrave;nh những bất ngờ nho nhỏ đầy l&atilde;ng mạn. Cả hai sẽ duy tr&igrave; t&igrave;nh cảm một c&aacute;ch tự nhi&ecirc;n, nhiều cảm x&uacute;c như thế.</p>\r\n\r\n<p>Cũng v&igrave; thế m&agrave; khi y&ecirc;u, Bạch Dương nữ thi&ecirc;n về t&igrave;nh cảm hơn l&yacute; tr&iacute;, đ&ocirc;i l&uacute;c ghen tu&ocirc;ng v&ocirc; cớ. Chỉ cần đối phương hơi lơ l&agrave; hay c&oacute; biểu hiện lạ một ch&uacute;t th&ocirc;i l&agrave; n&agrave;ng sẵn s&agrave;ng &ldquo;tra khảo&rdquo;, bởi Bạch Dương muốn bản th&acirc;n l&agrave; số 1, l&agrave; duy nhất của ch&agrave;ng. Khi nhận thấy t&igrave;nh cảm rạn nứt, d&ugrave; rất đau khổ th&igrave; dứt kho&aacute;t cắt đứt l&agrave; lựa chọn của Bạch Dương.</p>\r\n', '/uploads/img/1479206894_bach-duong.png', NULL, 1, '2016-11-15 17:48:14', '2016-11-15 17:48:14'),
(4, 'Mẹ Kim Ngưu – người bạn đồng hành đáng tin cậy', '<p>Mẹ Kim Ngưu &ndash; người bạn đồng h&agrave;nh đ&aacute;ng tin cậy. Mỗi người mẹ l&agrave; một b&iacute; mật. Kh&aacute;m ph&aacute; sự tuyệt vời của b&agrave; mẹ Kim Ngưu nh&eacute;!</p>\r\n\r\n<p>Những đứa trẻ thường hay hỏi han, thắc mắc đủ điều. Nếu c&oacute; một b&agrave; mẹ Kim Ngưu th&igrave; ch&uacute;ng chẳng phải lo bị mắng v&igrave; những c&acirc;u hỏi hết sức ngớ ngẩn hay những suy tư ấu trĩ , bởi mẹ sẽ ki&ecirc;n nhẫn giảng giải bằng hết. Sự trầm tĩnh của mẹ khiến những đứa trẻ cảm thấy tin tưởng v&agrave; lu&ocirc;n c&oacute; chỗ dựa trong cuộc sống. Mỗi khi gặp kh&oacute; khăn, người đầu ti&ecirc;n ch&uacute;ng t&igrave;m tới xin lời khuy&ecirc;n ch&iacute;nh l&agrave; b&agrave; mẹ Kim Ngưu. Mẹ theo s&aacute;t từng bước tiến tr&ecirc;n đường đời, l&agrave; người bạn đồng h&agrave;nh đ&aacute;ng tin cậy của con.</p>\r\n\r\n<p>Người mẹ cung Kim Ngưu l&agrave; người ổn định v&agrave; kh&aacute; ki&ecirc;n quyết, c&oacute; thể bảo đảm sự an to&agrave;n tuyệt đối cho những b&eacute; cưng n&ecirc;n lu&ocirc;n khuyến kh&iacute;ch ch&uacute;ng ra ngo&agrave;i v&agrave; tham gia c&aacute;c hoạt động ngo&agrave;i trời. Mẹ muốn c&aacute;c con c&oacute; thể mạo hiểm v&agrave; thưởng thức những điều k&igrave; th&uacute; b&ecirc;n ngo&agrave;i, những điều n&agrave;y gi&uacute;p mẹ Kim Ngưu c&oacute; thể gần gũi v&agrave; gi&uacute;p con hiểu được mục đ&iacute;ch thật sự cần hướng đến. V&agrave; hơn nữa, con sẽ học được b&agrave;i học gi&aacute; trị về l&ograve;ng trung th&agrave;nh, sự tận tụy v&agrave; t&iacute;nh ki&ecirc;n nhẫn từ mẹ.</p>\r\n\r\n<p>Cung Kim Ngưu vốn hơi bảo thủ v&agrave; th&iacute;ch &aacute;p đặt suy nghĩ l&ecirc;n người kh&aacute;c n&ecirc;n khi trở th&agrave;nh mẹ, họ cũng đối với con như vậy. Điều n&agrave;y &iacute;t nhiều g&acirc;y ra sự xung đột với trẻ. Nhưng v&igrave; y&ecirc;u m&agrave; mẹ Kim Ngưu sẵn s&agrave;ng thay đổi, dần dần điều chỉnh th&aacute;i độ, quan điểm v&agrave; suy nghĩ của m&igrave;nh th&ocirc;ng tho&aacute;ng, cởi mở hơn, cho con được tự do ph&aacute;t triển trong khu&ocirc;n khổ. Sự bao dung của mẹ sẽ khiến con cảm động v&agrave; thấu hiểu hơn.</p>\r\n', '/uploads/img/1479207196_kim-nguu.png', NULL, 1, '2016-11-15 17:53:16', '2016-11-15 17:53:16'),
(5, 'Song Tử và thế giới đa chiều', '<p>Song Tử v&agrave; thế giới đa chiều, Những người sinh ra dưới ch&ograve;m sao Song Tử c&oacute; những b&iacute; mật ri&ecirc;ng v&agrave; những lưu &yacute; rất đặc biệt!</p>\r\n\r\n<p>Bất cứ ai thuộc ch&ograve;m Song Tử đều c&oacute; t&iacute;nh &ldquo;hai mặt&rdquo;, sự đa chiều trong t&iacute;nh c&aacute;ch, trong t&igrave;nh y&ecirc;u v&agrave; trong mọi sự lựa chọn. Đ&acirc;y l&agrave; ch&ograve;m sao tự do v&agrave; hay thay đổi. Họ th&acirc;n thiện, t&iacute;ch cực v&agrave; th&uacute; vị, dẫu c&oacute; tham gia bữa tiệc 100 người th&igrave; chỉ cần 1 giờ l&agrave; họ đ&atilde; l&agrave;m quen với hết tất cả v&agrave; th&acirc;n với một số người.</p>\r\n\r\n<p>Sự linh hoạt l&agrave; thế mạnh v&agrave; cũng l&agrave; điểm yếu của Song Tử. Ch&ograve;m sao n&agrave;y đa t&agrave;i, dễ th&iacute;ch nghi nhưng nhạy cảm, hay thay đổi v&agrave; chỉ c&oacute; thể l&agrave;m tốt việc m&agrave; m&igrave;nh y&ecirc;u th&iacute;ch. Bởi vậy, Song Tử l&agrave; những người thường xuy&ecirc;n thay đổi c&ocirc;ng việc, người y&ecirc;u, thậm ch&iacute; cả nơi ở. Họ gh&eacute;t phải l&agrave;m một việc qu&aacute; l&acirc;u, y&ecirc;u một người qu&aacute; l&acirc;u, ở một chỗ qu&aacute; l&acirc;u. Sự t&ugrave; t&uacute;ng v&agrave; quen thuộc sẽ giết chết Song Tử.</p>\r\n\r\n<p>Nhưng ngược lại, Song Tử cũng l&agrave; ch&ograve;m sao trung th&agrave;nh. Họ trung th&agrave;nh với cảm x&uacute;c thật của m&igrave;nh. Chỉ cần l&agrave; điều họ y&ecirc;u th&iacute;ch, người họ y&ecirc;u thương th&igrave; Song Tử sẽ thẳng thắn, quyết liệt bảo vệ v&agrave; đi tới tận c&ugrave;ng.</p>\r\n\r\n<p>Y&ecirc;u một Song Tử l&agrave; thử th&aacute;ch tương đối lớn. Phải lu&ocirc;n quan t&acirc;m tới những điều họ n&oacute;i, những thứ họ l&agrave;m nhưng kh&ocirc;ng &aacute;p đặt v&agrave; s&aacute;t sao để họ c&oacute; kh&ocirc;ng gian thoải m&aacute;i; phải theo kịp những suy nghĩ nhanh như điện xẹt của họ v&agrave; đặc biệt l&agrave; kh&ocirc;ng &ldquo;thiếu muối&rdquo;. Sự thay đổi bất thường, kh&ocirc;ng quy tắc của Song Tử c&oacute; thể khiến đối phương ch&oacute;ng mặt, nhưng cũng ch&iacute;nh điều đ&oacute; tạo ra sự th&uacute; vị v&agrave; hấp dẫn trong t&igrave;nh y&ecirc;u của cả hai.</p>\r\n\r\n<p>Song Tử l&agrave; ch&ograve;m sao được sao Thủy bảo hộ, những người thuộc ch&ograve;m sao n&agrave;y c&oacute; lợi thế về ng&ocirc;n ngữ, văn chương, sự s&aacute;ng tạo nghệ thuật, th&iacute;ch hợp với nghề viết văn, truyền th&ocirc;ng.</p>\r\n\r\n<p>Để t&igrave;m thấy một Song Tử, h&atilde;y dạo qua c&aacute;c nh&agrave; s&aacute;ch, khu vui chơi đ&ocirc;ng đ&uacute;c hay một rạp chiếu phim tư liệu. Họ th&iacute;ch những nơi c&oacute; thể t&igrave;m kiếm th&ocirc;ng tin v&agrave; giao lưu với mọi người.</p>\r\n\r\n<p>Nếu bạn l&agrave; một c&ocirc; g&aacute;i Song Tử, đ&ocirc;i khi bạn sẽ cảm thấy m&igrave;nh hơi k&igrave; cục, th&iacute;ch y&ecirc;n tĩnh v&agrave; thả lỏng, kh&aacute;c hẳn với thường ng&agrave;y. Đ&oacute; thực ra l&agrave; t&iacute;nh c&aacute;ch ẩn, thường c&oacute; ở những c&ocirc; g&aacute;i thuộc cung Song Tử. Họ nhạy cảm, c&oacute; đ&ocirc;i ch&uacute;t mơ mộng, đ&ocirc;i ch&uacute;t liều lĩnh v&agrave; tinh thần tự do. Những gi&acirc;y ph&uacute;t ch&igrave;m đắm trong thế giới ri&ecirc;ng l&agrave; l&uacute;c Song Tử c&acirc;n bằng, nạp đầy năng lượng cho những điều mới mẻ ph&iacute;a trước.</p>\r\n', '/uploads/img/1479207306_song-tu.png', NULL, 1, '2016-11-15 17:55:06', '2016-11-15 17:55:06'),
(6, '12 sắc thái dịu dàng của cung Cự Giải', '<p>12 sắc th&aacute;i dịu d&agrave;ng của cung Cự Giải. Ch&ograve;m sao Cự Giải nổi tiếng nhu h&ograve;a, ấm &aacute;p. Nhưng mỗi con gi&aacute;p thuộc cung Cự Giải lại c&oacute; c&aacute;ch thể hiện sự dịu d&agrave;ng của m&igrave;nh theo c&aacute;ch rất ri&ecirc;ng.</p>\r\n\r\n<p>Cung Cự Giải tuổi T&yacute; mơ mộng, thậm ch&iacute; l&agrave; hơi hoang tưởng. Sự dịu d&agrave;ng của họ thể hiện qua những lời lẽ bay bổng c&ugrave;ng những h&agrave;nh động l&atilde;ng mạn.</p>\r\n\r\n<p>Cung Cự Giải tuổi Sửu hơi trầm lặng, th&iacute;ch d&ugrave;ng h&agrave;nh động để thể hiện t&igrave;nh cảm hơn l&agrave; lời n&oacute;i. Họ cục mịch nhưng thực sự rất quan t&acirc;m tới người kh&aacute;c.</p>\r\n\r\n<p>Cung Cự Giải tuổi Dần t&iacute;nh t&igrave;nh n&oacute;ng nẩy, hấp tấp hơn n&ecirc;n sự dịu d&agrave;ng kh&ocirc;ng thể hiện r&otilde;. Nhưng kh&ocirc;ng phải v&igrave; thế m&agrave; họ mất đi chất ấm &aacute;p đặc trưng, chỉ l&agrave; họ thể hiện hơi ồn &agrave;o m&agrave; th&ocirc;i.</p>\r\n\r\n<p>Cung Cự Giải tuổi M&atilde;o nh&uacute;t nh&aacute;t, hiền l&agrave;nh, chỉ b&agrave;y tỏ những điều trong l&ograve;ng với người th&acirc;n quen.</p>\r\n\r\n<p>Cung Cự Giải tuổi Th&igrave;n th&iacute;ch mơ mộng, kh&ocirc;ng d&aacute;m đối mặt với thực tế. Đ&ocirc;i khi họ dịu d&agrave;ng, nhưng cũng đ&ocirc;i khi l&agrave; cố chấp khiến đối phương kh&oacute; chịu.</p>\r\n\r\n<p>Cung Cự Giải tuổi Tỵ hơi lười nh&aacute;c, ham chơi, c&oacute; ch&uacute;t v&ocirc; t&acirc;m.</p>\r\n\r\n<p>Cung Cự Giải tuổi Ngọ rất nhạy cảm, hay nghi ngờ. Họ d&ugrave;ng nước mắt v&agrave; lời n&oacute;i nhẹ nh&agrave;ng để dằn vặt đối phương.</p>\r\n\r\n<p>Cung Cự Giải tuổi M&ugrave;i hiền l&agrave;nh, lương thiện v&agrave; dễ gần. Sự ấm &aacute;p, dịu d&agrave;ng to&aacute;t l&ecirc;n từ mọi h&agrave;nh động của họ.</p>\r\n\r\n<p>Cung Cự Giải tuổi Th&acirc;n hiền l&agrave;nh, chất ph&aacute;c, kh&ocirc;ng tranh gi&agrave;nh với người kh&aacute;c. Họ sẵn s&agrave;ng nhường nhịn bạn với vẻ h&ograve;a &aacute;i nhất c&oacute; thể.</p>\r\n\r\n<p>Cung Cự Giải tuổi Dậu th&agrave;nh thật, ng&acirc;y thơ n&ecirc;n dễ bị lừa.</p>\r\n\r\n<p>Cung Cự Giải tuổi Tuất nhạy cảm, trung th&agrave;nh, mạnh mẽ v&agrave; qu&ecirc;n m&igrave;nh v&igrave; người kh&aacute;c.</p>\r\n\r\n<p>Cung Cự Giải tuổi Hợi hiền l&agrave;nh, dễ bị lừa gạt, lợi dụng.</p>\r\n', '/uploads/img/1479207408_cu-giai.png', NULL, 1, '2016-11-15 17:56:48', '2016-11-15 17:56:48');
INSERT INTO `posts` (`id`, `title`, `content`, `image`, `attachment`, `created_by`, `created_at`, `updated_at`) VALUES
(7, 'Cung sư tử cần lưu ý những gì cho năm 2016', '<p>Cung sư tử cần lưu &yacute; những g&igrave; cho năm 2016, Trong năm B&iacute;nh Th&acirc;n, người sinh cung Sư Tử sẽ kh&ocirc;ng gặp những cơ hội n&agrave;o qu&aacute; đặc biệt v&agrave; cũng kh&ocirc;ng c&oacute; những điểm mốc l&agrave;m thay đổi cuộc đời. Tr&ecirc;n thực tế, đ&acirc;y l&agrave; năm bạn n&ecirc;n tiếp tục với c&ocirc;ng việc của m&igrave;nh v&agrave; ki&ecirc;n tr&igrave; với kế hoạch ban đầu.</p>\r\n\r\n<p>Điều cần lưu &yacute; trước ti&ecirc;n, h&agrave;nh tinh bảo trợ cho Sư Tử ch&iacute;nh l&agrave; mặt trời. Trong nửa đầu năm 2016, mặt trời sẽ c&oacute; những ảnh hưởng t&iacute;ch cực. N&oacute;i c&aacute;ch kh&aacute;c, người sinh cung Sư Tử sẽ cảm nhận được d&ograve;ng năng lượng mạnh mẽ, l&agrave;m tăng hiệu quả c&ocirc;ng việc v&agrave; đạt được những mục ti&ecirc;u của cuộc sống một c&aacute;ch r&otilde; r&agrave;ng.</p>\r\n\r\n<p>Tuy nhi&ecirc;n, đến cuối m&ugrave;a h&egrave;, ảnh hưởng n&agrave;y sẽ chấm dứt. Điều n&agrave;y kh&ocirc;ng c&oacute; nghĩa l&agrave; Sư Tử kh&ocirc;ng c&ograve;n được sự trợ gi&uacute;p n&agrave;o, hay kh&ocirc;ng vượt qua được những kh&oacute; khăn. Sư Tử vốn l&agrave; biểu tượng của sức mạnh, v&igrave; vậy h&atilde;y giữ vững sự mạnh mẽ của bản th&acirc;n. Đừng qu&ecirc;n tạo h&oacute;a đ&atilde; ban tặng cho bạn những chiếc răng nanh v&agrave; m&oacute;ng vuốt sắc nhọn. C&agrave;ng về giữa năm, bạn sẽ c&agrave;ng nhận ra bản năng Sư Tử trong m&igrave;nh.</p>\r\n\r\n<p>Về chi tiết, từ th&aacute;ng 1 đến cuối th&aacute;ng 3, Sư Tử sẽ trải qua đỉnh cao của sức mạnh. H&atilde;y thoải m&aacute;i bắt đầu một v&agrave;i kế hoạch v&agrave; đừng lăn tăn bạn kh&ocirc;ng thể ho&agrave;n th&agrave;nh, v&igrave; trong tương lai gần bạn sẽ kh&ocirc;ng c&oacute; cơ hội tương tự.</p>\r\n\r\n<p>Đầu năm 2016 bạn sẽ c&oacute; ảnh hưởng của sao Thủy, nhưng t&igrave;nh h&igrave;nh t&agrave;i ch&iacute;nh vẫn c&oacute; phần mơ hồ. Sẽ kh&ocirc;ng c&oacute; g&igrave; đặc biệt hoặc nổi trội xảy ra trong thời gian n&agrave;y x&eacute;t về đời sống t&igrave;nh cảm. H&atilde;y tin v&agrave;o trực gi&aacute;c của bản th&acirc;n, đ&acirc;y c&oacute; thể coi l&agrave; ngọn hải đăng của ch&iacute;nh bạn để đạt được mục ti&ecirc;u mong muốn.</p>\r\n\r\n<p>Từ th&aacute;ng 4 đến cuối th&aacute;ng 6, t&igrave;nh h&igrave;nh sẽ kh&ocirc;ng thay đổi một c&aacute;ch triệt để. Nguồn năng lượng t&iacute;ch cực của mặt trời vẫn tiếp tục bổ trợ cho Sư Tử. Điều bạn cần nhớ l&agrave; kh&ocirc;ng được mất niềm tin ở ch&iacute;nh m&igrave;nh.</p>\r\n\r\n<p>Đến giữa m&ugrave;a h&egrave;, bạn c&oacute; thể phải trải qua một giai đoạn trầm cảm kh&aacute; d&agrave;i. Nguy&ecirc;n nh&acirc;n c&oacute; thể đa dạng, v&agrave; ch&iacute;nh bạn mới l&agrave; người phải tự giải quyết c&aacute;c trải nghiệm cảm x&uacute;c n&agrave;y. Tuy nhi&ecirc;n, điều n&agrave;y kh&ocirc;ng g&acirc;y ảnh hưởng đ&aacute;ng kể cho cuộc sống của Sư Tử. V&igrave; vậy bạn n&ecirc;n ch&uacute; t&acirc;m v&agrave;o c&aacute;c mối quan hệ gia đ&igrave;nh trong thời điểm n&agrave;y.</p>\r\n\r\n<p>Th&aacute;ng 8, th&aacute;ng cuối c&ugrave;ng của m&ugrave;a h&egrave; sẽ đ&aacute;nh dấu sự bắt đầu của một giai đoạn mới. Mặt trời kh&ocirc;ng c&ograve;n ảnh hưởng mạnh đến Sư Tử, v&igrave; vậy bạn sẽ cảm gi&aacute;c một nguồn năng lượng lớn bị thất tho&aacute;t, khiến hiệu quả c&ocirc;ng việc bị giảm mạnh. May mắn l&agrave; v&agrave;o thời điểm n&agrave;y, t&igrave;nh h&igrave;nh kh&ocirc;ng c&oacute; g&igrave; kh&oacute; khăn. Nguy&ecirc;n nh&acirc;n l&agrave; cuối m&ugrave;a h&egrave;, Sư Tử c&oacute; thể đ&atilde; thực hiện được phần lớn c&aacute;c kế hoạch cho năm 2016.</p>\r\n\r\n<p>Thời gian n&agrave;y Sư Tử c&oacute; thể an nhi&ecirc;n hưởng thụ cuộc sống. Đến m&ugrave;a thu, nhiều người sẽ muốn &lsquo;l&aacute;nh đời&rsquo; v&agrave; trốn v&agrave;o kh&ocirc;ng gian của ri&ecirc;ng m&igrave;nh. Tuy nhi&ecirc;n điều n&agrave;y kh&ocirc;ng c&oacute; g&igrave; tồi tệ.</p>\r\n\r\n<p>Trong năm 2016 sẽ kh&ocirc;ng c&oacute; vấn đề n&agrave;o lớn xảy ra trong c&ocirc;ng việc hay gia đ&igrave;nh của Sư Tử. Một số c&atilde;i v&atilde; nho nhỏ cần bạn lưu t&acirc;m trong năm nay để đảm bảo tương lai kh&ocirc;ng ph&aacute;t sinh vấn đề. Bạn sẽ giải quyết được những chuyện n&agrave;y một c&aacute;ch nhẹ nh&agrave;ng m&agrave; kh&ocirc;ng phải cố gắng qu&aacute; nhiều. Điều cốt yếu l&agrave; đối mặt với nỗi buồn v&agrave;o m&ugrave;a đ&ocirc;ng, ngo&agrave;i ra mọi chuyện kh&aacute;c sẽ ổn thỏa.</p>\r\n\r\n<p>Đ&aacute; hộ mệnh của người sinh cung Sư tử l&agrave; m&atilde; n&atilde;o onyx, ruby, carnelian (ngọc hồng lựu), m&atilde; n&atilde;o c&oacute; v&acirc;n (sardonyx). Trong đ&oacute;, ruby c&oacute; năng lượng mặt trời mạnh nhất. Ngo&agrave;i ra, bạn c&oacute; thể d&ugrave;ng th&ecirc;m peridot &ndash; loại được mệnh danh l&agrave; &lsquo;đ&aacute; của mặt trời&rsquo;.</p>\r\n', '/uploads/img/1479207509_su-tu.png', NULL, 1, '2016-11-15 17:58:29', '2016-11-15 17:58:29'),
(8, 'Bản chất, tính cách đặc trưng của đàn ông xử nữ', '<p>Bản chất, t&iacute;nh c&aacute;ch đặc trưng của đ&agrave;n &ocirc;ng xử nữ. Đ&agrave;n &ocirc;ng Xử Nữ rất gọn g&agrave;ng ngăn nắp v&agrave; t&igrave;m kiếm sự ho&agrave;n hảo nhất trong số&nbsp;12 ch&ograve;m sao. Về bản chất anh ấy cũng l&agrave; người đ&agrave;n &ocirc;ng tinh tế chu đ&aacute;o v&agrave; sẵn l&ograve;ng phục vụ bạn nhất.</p>\r\n\r\n<p><strong>C&oacute; đầu &oacute;c v&agrave; hăng say lao động</strong></p>\r\n\r\n<p>Đ&agrave;n &ocirc;ng Xử Nữ c&oacute; bộ n&atilde;o ph&aacute;t triển cao độ v&agrave; tr&iacute; tuệ cao th&acirc;m kh&ocirc;n lường, chưa bao giờ để t&igrave;nh cảm v&agrave; dục vọng bản năng nhấn ch&igrave;m tư duy. Anh ấy l&agrave; mẫu đ&agrave;n &ocirc;ng thực tế v&agrave; cổ điển, chủ nghĩa thực dụng lu&ocirc;n lấn &aacute;t chủ nghĩa l&atilde;ng mạn. Th&ocirc;ng thường, anh ấy &iacute;t khi bị hun n&oacute;ng bởi kh&aacute;t vọng quyền lực, đa số t&igrave;nh huống anh ấy kh&ocirc;ng phải nh&agrave; mưu lược th&acirc;m hiểm với dụng t&acirc;m kh&oacute; lường, anh ấy kh&ocirc;ng c&oacute; nguyện vọng m&atilde;nh liệt được xuất hiện dưới &aacute;nh đ&egrave;n s&acirc;n khấu, kh&ocirc;ng tha thiết cuộc sống được mọi người tung h&ocirc;, ca ngợi, thậm ch&iacute; anh ấy th&iacute;ch trở th&agrave;nh người trải thảm, l&oacute;t đường cho th&agrave;nh c&ocirc;ng của kẻ kh&aacute;c, th&iacute;ch thực hiện những c&ocirc;ng việc cụ thể đằng sau hậu trường cho nh&acirc;n vật ch&iacute;nh.</p>\r\n\r\n<p>Anh ấy lu&ocirc;n lu&ocirc;n khao kh&aacute;t được l&agrave;m việc v&agrave; phục vụ người kh&aacute;c một c&aacute;ch ch&acirc;n th&agrave;nh, t&aacute;c phong giản dị, th&ocirc; mộc, kh&ocirc;ng h&agrave;o nho&aacute;ng bề ngo&agrave;i, lu&ocirc;n cẩn thận, tận tụy, thực tế thực tiễn, l&agrave; người đ&agrave;n &ocirc;ng trung th&agrave;nh, đ&aacute;ng tin cậy.</p>\r\n\r\n<p>Đ&agrave;n &ocirc;ng thuộc cung Xử Nữ l&agrave; người lu&ocirc;n cưỡng chế bản th&acirc;n phải lao động hăng say, chăm chỉ, cuộc s&ocirc;ng nghi&ecirc;m t&uacute;c, thanh tịnh như gi&aacute;o đồ đạo Thiền hoặc kẻ khổ h&agrave;nh tăng, lu&ocirc;n b&oacute; buộc bản th&acirc;n một c&aacute;ch h&agrave; khắc, cẩn thận tỉ mỉ từng chi tiết nhỏ, dường như kh&ocirc;ng bao giờ d&aacute;m vượt qua ranh giới m&igrave;nh đ&atilde; vạch ra. Anh ấy l&yacute; tr&iacute;, b&igrave;nh tĩnh, kh&eacute;o tay, linh cảm tốt nhưng anh ấy hiện thực đến nỗi khiến bạn cảm thấy v&ocirc; vị, nh&agrave;m ch&aacute;n. Anh ấy cẩn thận, m&aacute;y m&oacute;c đến mức m&aacute;y m&oacute;c, thiếu t&igrave;nh người.</p>\r\n\r\n<p>Anh ấy đối đ&atilde;i với mọi người một c&aacute;ch &ocirc;n h&ograve;a, rất khi&ecirc;m tốn, hay bẽn lẽn, thậm ch&iacute; xấu hổ. Anh ấy rất biết c&aacute;ch quan t&acirc;m đến người kh&aacute;c, khi người kh&aacute;c cần sự gi&uacute;p đỡ, anh ấy rất nhiệt t&igrave;nh phục vụ. Anh ấy c&oacute; &yacute; thức tr&aacute;ch nhiệm cao độ, sẵn s&agrave;ng hi sinh giấc ngủ v&agrave; thời gian nghỉ ngơi c&aacute; nh&acirc;n cho c&ocirc;ng việc.</p>\r\n\r\n<p><strong>Người đ&agrave;n &ocirc;ng mẫu mực v&agrave; t&ocirc;n thờ sự ho&agrave;n hảo</strong></p>\r\n\r\n<p>Đ&agrave;n &ocirc;ng sao Xử Nữ v&ocirc; c&ugrave;ng t&ocirc;n trọng ph&aacute;p luật v&agrave; c&aacute;c quy chuẩn đạo đức, t&iacute;nh c&aacute;ch trung th&agrave;nh, đ&aacute;ng tin, ổn định, chỉ cần giao việc cho anh ấy, bạn chỉ cần k&ecirc; cao gối ngủ ngon đợi tin tốt l&agrave;nh, bởi v&igrave; anh ấy l&agrave; người cả nghĩ thi&ecirc;n bẩm, thần kinh lu&ocirc;n đặt ừong trạng th&aacute;i lo lắng, căng thẳng cao độ. Anh ấy sợ những chi tiết kh&ocirc;ng ho&agrave;n hảo sẽ hủy hoại mọi c&ocirc;ng lao m&agrave; anh ấy hết l&ograve;ng bỏ ra, bởi vậy anh ấy sẽ căng thẳng thần kinh, cẩn thận theo d&otilde;i tiến tr&igrave;nh c&ocirc;ng việc từng ch&uacute;t một.</p>\r\n\r\n<p>Anh ấy l&agrave; người theo đuổi chủ nghĩa ho&agrave;n hảo một c&aacute;ch tinh tế như vậy, hơn thế nữa, mục ti&ecirc;u ho&agrave;n hảo cao đến nỗi mọi người kh&ocirc;ng thể với tới, điều đ&oacute; v&ocirc; t&igrave;nh khiến nội t&acirc;m anh ấy lu&ocirc;n thiếu tự tin v&agrave; kh&ocirc;ng ngừng chứng minh sự cần thiết của m&igrave;nh đối với người kh&aacute;c. Nội t&acirc;m của anh ấy bị nỗi sợ kh&ocirc;ng đạt tới sự ho&agrave;n hảo lũng đoạn, chi phối, kiếm t&igrave;m khuynh hướng ho&agrave;n hảo khiến anh ấy vĩnh viễn bận rộn một c&aacute;ch kh&ocirc;ng biết mệt mỏi, chỉ lao động cật lực, hăng say mới gi&uacute;p anh ấy tho&aacute;t khỏi nỗi dằn vặt bởi cảm gi&aacute;c thiếu an to&agrave;n, ch&iacute;nh v&igrave; vậy anh ấy v&ocirc; thức trở th&agrave;nh người cuồng c&ocirc;ng.</p>\r\n\r\n<p>Anh ấy y&ecirc;u c&ocirc;ng việc hơn cả y&ecirc;u bản th&acirc;n, anh ấy &ldquo;bận đến nỗi ban ng&agrave;y kh&ocirc;ng c&oacute; thời gian nằm mơ giữa ban ng&agrave;y; c&ograve;n ban đ&ecirc;m mệt đến nỗi kh&ocirc;ng c&oacute; thời gian ngắm sao trời&rdquo;. Tuy rằng anh ấy hy vọng được hồi đ&aacute;p bằng vật chất nhưng động cơ l&agrave;m việc thực chất lại l&agrave; muốn được chứng m&igrave;nh bản th&acirc;n l&agrave; người cần thiết kh&ocirc;ng thể thiếu trong tập thể. Ch&iacute;nh bởi thế, bạn thường tiếc thay cho anh ấy, lẽ ra động cơ, sự nỗ lực v&agrave; hăng say l&agrave;m việc của anh ấy phải được ph&uacute;c đ&aacute;p nhiều hơn những thức hiện thời anh ấy được hưởng.</p>\r\n\r\n<p>Đ&agrave;n &ocirc;ng Xử Nữ l&agrave; một t&aacute;c phấm ho&agrave;n sảo của nh&acirc;n gian, anh ấy qu&aacute; sức ch&uacute; &yacute; tới h&igrave;nh tượng của m&igrave;nh trong con mắt mọi người, tu&acirc;n thủ c&aacute;c gi&aacute;o l&yacute;, quy chuẩn một c&aacute;ch th&aacute;i qu&aacute;, cuộc sống kh&aacute; quy luật của anh ấy đạt đến độ ti&ecirc;u chuẩn khiến những người kh&aacute;c phần n&agrave;o cảm thấy v&ocirc; vị v&agrave; tẻ nhạt. Anh ấy ho&agrave;n hảo đến nỗi thiếu sự tinh nghịch v&agrave; l&atilde;ng tử, kh&ocirc;ng c&oacute; sự ng&ocirc;ng cuồng của những người v&ocirc; kỷ luật, v&ocirc; tổ chức.</p>\r\n\r\n<p><strong>Xử Nữ l&agrave; mẫu đ&agrave;n &ocirc;ng đ&aacute;ng tin cậy v&agrave; an to&agrave;n</strong></p>\r\n\r\n<p>Anh ấy hay lo lắng một c&aacute;ch th&aacute;i qu&aacute; khiến thần kinh lu&ocirc;n trong trạng th&aacute;i căng thẳng, hồi hộp, nhiều khi chuyện b&eacute; x&eacute; ra to. Anh ấy qu&aacute; cẩn thận, tinh tế đến độ chỉ nh&igrave;n thấy chi tiết m&agrave; kh&ocirc;ng thấy to&agrave;n cảnh, giống như &ldquo;chỉ nh&igrave;n thấy c&acirc;y rừng m&agrave; kh&ocirc;ng thấy được cả khu rừng&rdquo;.</p>\r\n\r\n<p>Th&oacute;i quen kiếm t&igrave;m sự ho&agrave;n hảo khiến anh ấy c&oacute; rất nhiều y&ecirc;u cầu ngoan cố như gọn g&agrave;ng, ngăn nắp, sạch sẽ, đ&ocirc;i mắt tinh tế, qu&eacute;t ngang qu&eacute;t dọc khắp nơi khiến anh ấy nhanh ch&oacute;ng ph&aacute;t hiện những khiếm khuyết hoặc lỗi nhỏ thiếu ho&agrave;n hảo như vết bẩn nhỏ tr&ecirc;n cổ &aacute;o bạn, bạn t&ocirc; son kh&ocirc;ng c&acirc;n đối, hai ng&agrave;y bạn vẫn chưa gội đầu, bất kỳ những chi tiết nhỏ kh&ocirc;ng bắt mắt n&agrave;o cũng kh&ocirc;ng tho&aacute;t khỏi đường qu&eacute;t quan s&aacute;t của cặp mắt anh ấy.</p>\r\n\r\n<p>Điều đ&oacute; khiến anh ấy lu&ocirc;n kh&ocirc;ng thỏa m&atilde;n với bất kỳ ai v&agrave; bất kỳ việc g&igrave;, bởi vậy anh ấy tự biến m&igrave;nh trở th&agrave;nh nh&agrave; ph&ecirc; b&igrave;nh bẩm sinh, th&iacute;ch bới m&oacute;c hơn khen ngợi người kh&aacute;c. Anh ấy cần cảm gi&aacute;c an to&agrave;n v&agrave; đảm bảo, anh ấy cẩn thận, ổn định, tỉnh t&aacute;o đến mức vẻ đẹp ho&agrave;n hảo của mỹ nữ giống như y&ecirc;u tinh cũng kh&oacute; l&ograve;ng m&ecirc; hoặc v&agrave; khiến anh ấy sa v&agrave;o lưới t&igrave;nh.</p>\r\n\r\n<p>Anh ấy l&agrave; mẫu đ&agrave;n &ocirc;ng v&ocirc; c&ugrave;ng an to&agrave;n, đ&aacute;ng tin v&agrave; đem lại sự đảm bảo cho phụ nữ, bạn kh&ocirc;ng thể kh&ocirc;ng thừa nhận anh ấy l&agrave; người đ&agrave;n &ocirc;ng tốt kh&oacute; l&ograve;ng t&igrave;m thấy trong số h&agrave;ng đống đ&agrave;n &ocirc;ng sống tr&ecirc;n c&otilde;i đời n&agrave;y, nhưng sự ho&agrave;n hảo của anh ấy cũng c&oacute; c&aacute;i gi&aacute; của n&oacute;: bạn c&oacute; thật sự chịu được sự ho&agrave;n hảo kh&ocirc;ng t&igrave; vết tới mức tẻ nhạt kh&ocirc;ng?</p>\r\n', '/uploads/img/1479207669_xu-nu.png', NULL, 1, '2016-11-15 18:01:09', '2016-11-15 18:01:09'),
(9, 'Thiên Bình và niềm đam mê bất tận với tình yêu', '<p>Thi&ecirc;n B&igrave;nh v&agrave; niềm đam m&ecirc; bất tận với t&igrave;nh y&ecirc;u, Ch&ograve;m sao Thi&ecirc;n B&Igrave;nh c&oacute; đam m&ecirc; bất tận với t&igrave;nh y&ecirc;u. Đối với B&igrave;nh nhi, t&igrave;nh y&ecirc;u vừa l&agrave; nhu cầu, vừa l&agrave; bản năng.</p>\r\n\r\n<p>Ch&ograve;m sao thứ 7 của v&ograve;ng tr&ograve;n ho&agrave;ng đạo n&agrave;y c&oacute; những dấu hiệu nổi trội về mối quan hệ tương t&aacute;c với người kh&aacute;c. Được cai trị bởi sao Kim &ndash; h&agrave;nh tinh của t&igrave;nh dục v&agrave; sự l&atilde;ng mạn, Thi&ecirc;n B&igrave;nh thực sự &ldquo;nghiện&rdquo; y&ecirc;u v&agrave; cảm thấy v&ocirc; c&ugrave;ng kh&oacute; khăn khi c&ocirc; đơn.</p>\r\n\r\n<p>Cũng bởi được sao Kim bảo hộ n&ecirc;n Thi&ecirc;n B&igrave;nh c&oacute; vẻ hấp dẫn v&agrave; duy&ecirc;n d&aacute;ng rất tự nhi&ecirc;n. Đ&acirc;y l&agrave; kh&iacute; chất đặc biệt chỉ c&oacute; ở người cung Thi&ecirc;n B&igrave;nh. Phụ nữ cung Thi&ecirc;n B&igrave;nh được cho l&agrave; xinh đẹp v&agrave; quyến rũ nhất. Đ&agrave;n &ocirc;ng Thi&ecirc;n B&igrave;nh th&igrave; rất tinh tế v&agrave; thấu hiểu. Bởi vẻ ngo&agrave;i bắt mắt n&ecirc;n người cung Thi&ecirc;n B&igrave;nh tự tin v&agrave;o khả năng được người kh&aacute;c say m&ecirc;. Đ&oacute; l&agrave; nguy&ecirc;n nh&acirc;n s&acirc;u xa của việc họ th&iacute;ch th&uacute; với t&igrave;nh y&ecirc;u.</p>\r\n\r\n<p>Thi&ecirc;n B&igrave;nh l&agrave; ch&ograve;m sao của nghệ thuật v&agrave; c&aacute;i đẹp. Người thuộc cung n&agrave;y l&agrave;m bất cứ điều g&igrave; cũng hướng tới gi&aacute; trị thẩm mĩ, cầu k&igrave; v&agrave; tinh tế trong cả những điều rất nhỏ. Bởi thế m&agrave; họ c&oacute; kh&iacute; chất qu&yacute; ph&aacute;i, vượt trội hơn so với người kh&aacute;c.</p>\r\n\r\n<p>T&igrave;nh y&ecirc;u l&agrave; một m&ocirc;n nghệ thuật đầy th&aacute;ch thức v&agrave; trải nghiệm, l&agrave; một loại thẩm mĩ ở mức độ cao n&ecirc;n dường như c&oacute; sức hấp dẫn bất tận với Thi&ecirc;n B&igrave;nh. T&igrave;nh y&ecirc;u tạo cho họ cảm hứng sống v&agrave; cảm gi&aacute;c m&igrave;nh trở n&ecirc;n quan trọng.</p>\r\n\r\n<p>Dường như ch&ograve;m sao Thi&ecirc;n B&igrave;nh l&agrave; những người sống để y&ecirc;u, nhưng họ lại kh&ocirc;ng th&iacute;ch h&ocirc;n nh&acirc;n. T&igrave;nh y&ecirc;u v&agrave; h&ocirc;n nh&acirc;n l&agrave; hai kh&aacute;i niệm kh&aacute;c xa nhau đối với B&igrave;nh nhi. Họ c&oacute; thể y&ecirc;u đương cuồng nhiệt, say đắm, c&oacute; những mối t&igrave;nh l&atilde;ng mạn, ngọt ng&agrave;o nhưng kết h&ocirc;n th&igrave; kh&ocirc;ng. Thậm ch&iacute;, c&oacute; những Thi&ecirc;n B&igrave;nh từ chối kết h&ocirc;n d&ugrave; đ&atilde; y&ecirc;u nhau rất l&acirc;u. B&igrave;nh nhi th&iacute;ch t&igrave;nh y&ecirc;u bởi n&oacute; k&iacute;ch th&iacute;ch v&agrave; mang đến cảm gi&aacute;c tuyệt vời, c&ograve;n h&ocirc;n nh&acirc;n gắn liền với tr&aacute;ch nhiệm v&agrave; sự r&agrave;ng buộc lại khiến họ e sợ.</p>\r\n\r\n<p>Thi&ecirc;n B&igrave;nh l&agrave; những người t&igrave;nh tuyệt vời nhưng thiếu ki&ecirc;n nhẫn. Họ th&iacute;ch sự đẹp đẽ v&agrave; h&agrave;o nho&aacute;ng, khi t&igrave;nh y&ecirc;u mất đi hai yếu tố ấy họ sẽ dễ ch&aacute;n nản, thất vọng v&agrave; bỏ rơi đối phương. Đ&oacute; l&agrave; mặt tr&aacute;i của ch&ograve;m sao n&agrave;y.</p>\r\n\r\n<p>Hơn thế nữa, bản t&iacute;nh thiếu quyết đo&aacute;n, do dự, hay so s&aacute;nh cũng khiến Thi&ecirc;n B&igrave;nh đắn đo về sự lựa chọn của m&igrave;nh, đặc biệt l&agrave; ph&aacute;i nữ. Họ c&oacute; xu hướng so s&aacute;nh người y&ecirc;u, t&igrave;nh y&ecirc;u của m&igrave;nh với những c&ocirc; g&aacute;i kh&aacute;c v&agrave; nếu cảm thấy m&igrave;nh kh&ocirc;ng bằng th&igrave; sẽ ngấm ngầm c&oacute; lựa chọn kh&aacute;c.</p>\r\n\r\n<p>Tuy nhi&ecirc;n, một điểm rất th&uacute; vị l&agrave; Thi&ecirc;n B&igrave;nh biết c&aacute;ch kết nối trong t&igrave;nh y&ecirc;u. Họ hấp dẫn đối phương v&agrave; g&acirc;y dựng mối quan hệ bằng sự th&ocirc;ng minh, nhạy cảm, hiểu biết của m&igrave;nh, n&ecirc;n nếu ai đang y&ecirc;u Thi&ecirc;n B&igrave;nh sẽ cảm thấy cực k&igrave; hạnh ph&uacute;c. C&ograve;n đ&atilde; kết h&ocirc;n ư, hẳn l&agrave; bạn đ&atilde; c&oacute; một người bạn đời tuyệt vời.</p>\r\n', '/uploads/img/1479207771_thien-binh.png', NULL, 1, '2016-11-15 18:02:51', '2016-11-15 18:02:51'),
(10, 'Hé lộ mâu thuẫn tình yêu của Bọ Cạp', '<p>H&eacute; lộ m&acirc;u thuẫn t&igrave;nh y&ecirc;u của Bọ Cạp. Thi&ecirc;n Yết l&agrave; ch&ograve;m sao b&iacute; ẩn nhất trong v&ograve;ng tr&ograve;n ho&agrave;ng đạo. Vẻ lạnh l&ugrave;ng, kh&oacute; gần của Yết Yết khiến người kh&aacute;c hoang mang nhưng ẩn sau đ&oacute; l&agrave; t&acirc;m hồn khao kh&aacute;t y&ecirc;u thương rất m&atilde;nh liệt.</p>\r\n\r\n<p>Người thuộc cung Thi&ecirc;n Yết c&oacute; bản lĩnh cực k&igrave; tốt, sẵn s&agrave;ng đối mặt với th&aacute;ch thức v&agrave; những bất hạnh của cuộc sống. Họ cảm thấy đ&oacute; kh&ocirc;ng phải l&agrave; vật cản m&agrave; l&agrave; một cuộc chơi d&agrave;nh cho kẻ mạnh. Thế n&ecirc;n, Thi&ecirc;n Yết lu&ocirc;n c&oacute; th&aacute;i độ cao ngạo, mạnh mẽ, thậm ch&iacute; c&oacute; phần hơi đ&aacute;ng sợ.</p>\r\n\r\n<p>Nhưng đối với t&igrave;nh y&ecirc;u, Thi&ecirc;n Yết lại mang bản chất của một kẻ si t&igrave;nh với mong muốn được thể hiện t&igrave;nh cảm. Họ tha thiết khi y&ecirc;u v&agrave; th&iacute;ch chiếm hữu t&igrave;nh cảm. Bởi vậy, điều họ cần ở đối phương l&agrave; l&ograve;ng chung thủy chứ kh&ocirc;ng phải &aacute;nh mắt ngưỡng mộ. Họ muốn nhận được sự cảm th&ocirc;ng chia sẻ v&agrave; y&ecirc;u thương th&agrave;nh thật chứ kh&ocirc;ng phải l&agrave; những ph&uacute;t bốc đồng, h&agrave;o nho&aacute;ng. V&agrave; ngược lại, họ cũng thật t&acirc;m đối xử với người m&igrave;nh y&ecirc;u như vậy.</p>\r\n\r\n<p>Người cung Thi&ecirc;n Yết thực ra mưu cầu sự ổn định trong t&igrave;nh cảm. Họ muốn được c&ugrave;ng đối phương trải qua những th&aacute;ng ng&agrave;y h&ograve;a hợp, b&ecirc;n nhau b&igrave;nh y&ecirc;n. Nhưng bản chất t&iacute;nh c&aacute;ch lại lu&ocirc;n đẩy Yết Yết v&agrave;o những mối t&igrave;nh b&ugrave;ng ch&aacute;y, rực lửa, cuồng nhiệt song chẳng k&eacute;o d&agrave;i được bao l&acirc;u. Điều n&agrave;y khiến họ rất m&acirc;u thuẫn.</p>\r\n\r\n<p>Phụ nữ chịu ảnh hưởng của sao Kim rất hợp với đ&agrave;n &ocirc;ng Thi&ecirc;n Yết. D&ugrave; l&agrave; cuộc sống tinh thần hay c&aacute;c lĩnh vực kh&aacute;c họ đều t&igrave;m thấy điểm tương đồng với nhau. Phụ nữ cung Cự Giải mang tới cho đ&agrave;n &ocirc;ng Thi&ecirc;n Yết sự dịu d&agrave;ng, đời sống t&igrave;nh cảm ngọt ng&agrave;o, ấm &aacute;p. Những c&ocirc; g&aacute;i Song Ngư khiến họ cảm nhận được niềm vui của cuộc sống. C&ograve;n thật bất ngờ, người phụ nữ dũng cảm, thực tế cung Sư Tử sẽ l&agrave;m thay đổi t&iacute;nh c&aacute;ch của đ&agrave;n &ocirc;ng Thi&ecirc;n Yết, gi&uacute;p họ tho&aacute;t khỏi những suy nghĩ phức tạp, &acirc;u lo.</p>\r\n\r\n<p>Đ&agrave;n &ocirc;ng cung Kim Ngưu ch&acirc;n thật v&agrave; nhẫn nại l&agrave; lựa chọn l&yacute; tưởng cho những c&ocirc; n&agrave;ng Thi&ecirc;n Yết. Ngo&agrave;i ra, một ch&agrave;ng trai Song Ngư cũng kh&aacute; hợp gu với n&agrave;ng. C&ograve;n nếu c&oacute; anh ch&agrave;ng Cự Giải n&agrave;o xuất hiện trong cuộc đời th&igrave; n&agrave;ng Thi&ecirc;n Yết sẽ được nắm quyền cai quản.</p>\r\n', '/uploads/img/1479207900_bo-cap.png', NULL, 1, '2016-11-15 18:05:00', '2016-11-15 18:05:00'),
(11, 'Tính cách hai trong một của Nhân Mã', '<p>T&iacute;nh c&aacute;ch hai trong một của Nh&acirc;n M&atilde;, Tương tự như Song Tử v&agrave; Song Ngư, Nh&acirc;n M&atilde; l&agrave; một ch&ograve;m sao k&eacute;p, c&oacute; khuynh hướng ph&acirc;n liệt trong bản chất.</p>\r\n\r\n<p>C&oacute; hai con người c&ugrave;ng tồn tại trong một Nh&acirc;n M&atilde;. Một người thể thao, năng động, hướng ngoại v&agrave; một người tr&iacute; tuệ, s&aacute;ng tạo,đam m&ecirc; kh&aacute;m ph&aacute; học thuật. Hầu như, một Nh&acirc;n M&atilde; đều c&oacute; hai khuynh hướng như vậy, nhưng c&aacute;i n&agrave;o bộc lộ r&otilde; r&agrave;ng hơn sẽ l&agrave; khuynh hướng chủ đạo, định hướng cuộc đời v&agrave; phong c&aacute;ch sống của ch&ograve;m sao n&agrave;y.</p>\r\n\r\n<p>Nh&acirc;n M&atilde; trong một cuộc tranh c&atilde;i rất dễ bộc lộ sự ph&acirc;n liệt trong t&iacute;nh c&aacute;ch. Họ l&agrave; người ngại va chạm, kh&ocirc;ng th&iacute;ch đ&ocirc;i co v&agrave; lu&ocirc;n lấy &ldquo;dĩ h&ograve;a vi qu&yacute;&rdquo; l&agrave;m đầu n&ecirc;n c&oacute; xung đột sẽ nhanh ch&oacute;ng nh&uacute;n nhường, kh&ocirc;ng l&agrave;m to chuyện. Nhưng mặt kh&aacute;c, nếu b&ugrave;ng nổ, M&atilde; M&atilde; c&oacute; thể l&agrave; người tranh luận v&ocirc; c&ugrave;ng hăng h&aacute;i với l&yacute; lẽ đanh th&eacute;p, h&ugrave;ng hồn, sẵn s&agrave;ng tham gia c&aacute;c cuộc tranh luận n&oacute;ng, thậm ch&iacute; đụng độ để bảo vệ m&igrave;nh.</p>\r\n\r\n<p>Nhiệt t&igrave;nh, lạc quan l&agrave; những phẩm chất thường thấy ở Nh&acirc;n M&atilde;. Nhưng song song trong con người họ l&agrave; sự u buồn, bất an v&agrave; che giấu cảm x&uacute;c. Họ l&agrave; những người dễ bị tổn thương bởi những điều nhỏ, dễ cảm thấy mất thăng bằng v&igrave; kh&oacute; khăn. Kh&ocirc;ng chia sẻ c&ugrave;ng ai v&agrave; tự m&igrave;nh lấy lại cảm x&uacute;c khiến Nh&acirc;n M&atilde; tạo cho m&igrave;nh vỏ bọc vững v&agrave;ng.</p>\r\n\r\n<p>Nh&acirc;n M&atilde; thẳng thắn nhưng lại kh&aacute; quanh co trong vấn đề thể hiện cảm x&uacute;c. Hiếm khi thấy một Nh&acirc;n M&atilde; bộc lộ trạng th&aacute;i vui, buồn, thất vọng hay đau khổ. Th&agrave;nh c&ocirc;ng kh&ocirc;ng tỏ ra tự m&atilde;n, thất bại kh&ocirc;ng tỏ ra ch&aacute;n nản.</p>\r\n\r\n<p>Trong t&igrave;nh y&ecirc;u, Nh&acirc;n M&atilde; th&iacute;ch tự do v&agrave; k&iacute;ch th&iacute;ch. Họ y&ecirc;u những người ph&oacute;ng kho&aacute;ng, muốn c&oacute; những c&acirc;u chuyện t&igrave;nh l&atilde;ng mạn, cần kh&ocirc;ng gian ri&ecirc;ng để thỏa sức vẫy v&ugrave;ng.</p>\r\n\r\n<p>Thế nhưng, bản chất của ch&ograve;m sao n&agrave;y l&agrave; ghen tu&ocirc;ng v&agrave; sở hữu. Đừng nh&igrave;n vẻ bề ngo&agrave;i hết sức tưng tửng m&agrave; bị lừa. Nh&acirc;n M&atilde; kh&ocirc;ng bao giờ tha thứ cho sự thiếu chung thủy v&agrave; quản người y&ecirc;u cũng chặt lắm đấy.</p>\r\n', '/uploads/img/1479208020_nhan-ma.png', NULL, 1, '2016-11-15 18:07:00', '2016-11-15 18:07:00'),
(12, 'Nét quyến rũ tiềm ẩn của nàng Ma Kết', '<p>N&eacute;t quyến rũ tiềm ẩn của n&agrave;ng Ma Kết. Kh&ocirc;ng phải l&agrave; c&ocirc; g&aacute;i nữ t&iacute;nh, xinh đẹp nhất&nbsp;cung ho&agrave;ng đạo&nbsp;nhưng Ma Kết l&agrave; c&ocirc; n&agrave;ng khiến bất k&igrave; ch&agrave;ng trai n&agrave;o cũng muốn chinh phục. Kh&aacute;m ph&aacute; n&eacute;t quyến rũ tiềm ẩn trong vẻ bề ngo&agrave;i ch&iacute;n chắn, điềm đạm, hơi cứng nhắc của n&agrave;ng Ma Kết nh&eacute;.</p>\r\n\r\n<p><strong>Ma Kết &ndash; Vẻ hấp dẫn đi c&ugrave;ng năm th&aacute;ng</strong><br />\r\nNhững c&ocirc; n&agrave;ng Ma Kết kh&ocirc;ng thể n&oacute;i l&agrave; &ldquo;sắc nước hương trời&rdquo; nhưng từ họ to&aacute;t ra vẻ đẹp của t&acirc;m hồn v&agrave; sự trưởng th&agrave;nh, khiến người kh&aacute;c kh&oacute; c&oacute; thể cưỡng lại. Đ&oacute; l&agrave; sự hấp dẫn ng&agrave;y một tăng tiến theo thời gian v&agrave; bền l&acirc;u c&ugrave;ng năm th&aacute;ng.</p>\r\n\r\n<p>Sự độc lập, kỷ luật v&agrave; nghi&ecirc;m t&uacute;c của Ma Kết khiến c&aacute;c ch&agrave;ng trai quanh c&ocirc; n&agrave;ng phải d&egrave; chừng, dẫu th&iacute;ch nhưng kh&ocirc;ng d&aacute;m t&aacute;n tỉnh qu&aacute; đ&agrave;. N&agrave;ng l&agrave;m chủ bản th&acirc;n trong mọi t&igrave;nh huống, nắm thế chủ động v&agrave; khiến người kh&aacute;c đi theo m&igrave;nh chứ kh&ocirc;ng bao giờ đi theo người kh&aacute;c.</p>\r\n\r\n<p>N&agrave;ng th&ocirc;ng minh, duy&ecirc;n d&aacute;ng, giỏi ngoại giao, thu h&uacute;t đối phương bằng thần th&aacute;i v&agrave; tr&iacute; tuệ chứ kh&ocirc;ng nhờ ngoại h&igrave;nh. Những c&acirc;u chuyện th&uacute; vị của Ma Kết đ&ocirc;i khi c&ograve;n hấp dẫn v&agrave; giữ ch&acirc;n c&aacute;c ch&agrave;ng trai l&acirc;u hơn l&agrave; một th&acirc;n h&igrave;nh bốc lửa.</p>\r\n\r\n<p>N&agrave;ng gia gi&aacute;o nhưng kh&ocirc;ng cổ hủ, truyền thống nhưng cũng rất hiện đại. Một c&ocirc; g&aacute;i Ma Kết c&oacute; thể đi bar th&ocirc;ng đ&ecirc;m với bạn, tiệc t&ugrave;ng suốt những ng&agrave;y nghỉ lễ nhưng lu&ocirc;n c&oacute; c&ocirc; bạn th&acirc;n đi k&egrave;m để kh&ocirc;ng bao giờ rơi v&agrave;o những t&igrave;nh huống nguy hiểm hay t&igrave;nh một đ&ecirc;m bất ngờ. N&agrave;ng chơi nhưng vẫn kiểm so&aacute;t được bản th&acirc;n, khiến c&aacute;c ch&agrave;ng vừa say m&ecirc; vừa t&ocirc;n trọng.</p>\r\n\r\n<p><strong>Ma Kết &ndash; C&oacute; ch&iacute;nh kiến, mạnh mẽ, bướng bỉnh nhưng đầy nữ t&iacute;nh</strong><br />\r\nTr&aacute;i ngược hẳn với sự nữ t&iacute;nh, dịu d&agrave;ng của Song Ngư, Ma Kết c&oacute; ch&iacute;nh kiến ri&ecirc;ng, sự mạnh mẽ từ b&ecirc;n trong, thậm ch&iacute; đ&ocirc;i l&uacute;c th&agrave;nh bướng bỉnh.</p>\r\n\r\n<p>N&agrave;ng c&oacute; mục ti&ecirc;u v&agrave; phấn đấu hết m&igrave;nh v&igrave; mục ti&ecirc;u đ&oacute;. T&igrave;nh y&ecirc;u chỉ l&agrave; một phần trong cuộc đời Ma Kết. Đặc bi&ecirc;t, n&agrave;ng c&oacute; quan điểm ri&ecirc;ng v&agrave; kh&ocirc;ng v&igrave; y&ecirc;u m&agrave; thay đổi to&agrave;n bộ bản th&acirc;n. N&agrave;ng sẵn s&agrave;ng tranh luận với người y&ecirc;u để bảo vệ quan điểm v&agrave; &yacute; kiến của m&igrave;nh. Bởi thế, y&ecirc;u một n&agrave;ng Ma Kết sẽ kh&ocirc;ng bao giờ nh&agrave;m ch&aacute;n, kh&ocirc;ng bao giờ ch&igrave;m ngập trong nước mắt v&agrave; những lời than v&atilde;n, c&ocirc; ấy thừa biết c&aacute;ch giải quyết vấn đề hiệu quả hơn.</p>\r\n\r\n<p>Sự nữ t&iacute;nh của Ma Kết cũng rất đặc biệt. C&ocirc; ấy thể hiện n&oacute; bằng sự t&ocirc;n trọng v&agrave; thấu hiểu người đ&agrave;n &ocirc;ng của m&igrave;nh. D&ugrave; độc lập, c&oacute; chỗ đứng ri&ecirc;ng, kh&ocirc;ng dựa dẫm v&agrave;o ai nhưng con g&aacute;i Ma Kết kh&ocirc;ng bao giờ coi thường hay chế nhạo người m&igrave;nh y&ecirc;u. C&ocirc; ấy tin rằng, ai cũng c&oacute; khả năng ri&ecirc;ng. Bởi thế m&agrave; ở b&ecirc;n cạnh Ma Kết nữ c&aacute;c ch&agrave;ng trai sẽ kh&ocirc;ng cần gồng m&igrave;nh l&ecirc;n để thể hiện kh&iacute; ph&aacute;ch đ&agrave;n &ocirc;ng.</p>\r\n', '/uploads/img/1479208155_ma-ket.png', NULL, 1, '2016-11-15 18:09:15', '2016-11-15 18:09:15'),
(13, 'Tính cách không ai ngờ của cô nàng Bảo Bình', '<p>T&iacute;nh c&aacute;ch kh&ocirc;ng ai ngờ của c&ocirc; n&agrave;ng Bảo B&igrave;nh. C&ocirc; n&agrave;ng Bảo B&igrave;nh c&oacute; ch&uacute;t lập dị, qu&aacute;i qu&aacute;i nhưng b&ecirc;n trong vẻ ngo&agrave;i kh&aacute;c thường ấy l&agrave; một c&aacute; t&iacute;nh hết sức th&uacute; vị v&agrave; bất ngờ.</p>\r\n\r\n<p><strong>Bảo B&igrave;nh &ndash; C&ocirc; g&aacute;i gi&agrave;u l&ograve;ng nh&acirc;n &aacute;i, thấu hiểu v&agrave; cảm th&ocirc;ng</strong><br />\r\nBảo B&igrave;nh l&agrave; những c&ocirc; n&agrave;ng gi&agrave;u l&ograve;ng nh&acirc;n &aacute;i. Họ c&oacute; khả năng thấu hiểu v&agrave; cảm th&ocirc;ng s&acirc;u sắc, v&igrave; thế họ đặc biệt y&ecirc;u th&iacute;ch c&aacute;c hoạt động từ thiện. Những c&ocirc; n&agrave;ng n&agrave;y lu&ocirc;n th&iacute;ch đi, muốn đi v&agrave; muốn được chia sẻ v&agrave; xoa dịu nỗi đau của người kh&aacute;c.</p>\r\n\r\n<p>Bảo B&igrave;nh sẵn s&agrave;ng bộc lộ bản th&acirc;n v&agrave; cũng &iacute;t khi d&agrave;nh thời gian quan t&acirc;m xem người kh&aacute;c đang nghĩ g&igrave; về m&igrave;nh. Tuy nhi&ecirc;n, ngay cả khi đ&atilde; tiết lộ tất cả về m&igrave;nh, n&agrave;ng vẫn khiến cho người kh&aacute;c cảm thấy c&ocirc; ấy l&agrave; con người rất kh&oacute; đo&aacute;n. Bởi v&igrave; sở th&iacute;ch, c&aacute; t&iacute;nh cũng như c&aacute;ch sống của Bảo B&igrave;nh lu&ocirc;n thay đổi. Cuộc đời của họ l&agrave; những h&agrave;nh tr&igrave;nh đi t&igrave;m những thứ th&uacute; vị, mới mẻ v&agrave; tuyệt vời hơn.</p>\r\n\r\n<p><strong>Bảo B&igrave;nh &ndash; Sống nội t&acirc;m, th&iacute;ch sự tự do c&aacute; nh&acirc;n</strong><br />\r\nT&acirc;m sự với một c&ocirc; g&aacute;i Bảo B&igrave;nh l&agrave; một điều thực sự rất hấp dẫn bởi n&agrave;ng l&agrave; người biết lắng nghe, chia sẻ với c&aacute;ch n&oacute;i chuyện v&ocirc; c&ugrave;ng cuốn h&uacute;t.<br />\r\nNgo&agrave;i ra, Bảo B&igrave;nh c&ograve;n l&agrave; người sống nội t&acirc;m v&agrave; &iacute;t khi để người kh&aacute;c phải lo lắng cho m&igrave;nh. N&agrave;ng th&iacute;ch sự tự do c&aacute; nh&acirc;n v&agrave; muốn được t&ocirc;n trọng trong bất kỳ một mối quan hệ n&agrave;o.</p>\r\n\r\n<p>Bảo B&igrave;nh gh&eacute;t nhất l&agrave; những người kh&ocirc;ng giữ lời hứa. Với họ, lời n&oacute;i đi k&egrave;m với chữ t&iacute;n v&agrave; niềm tin. N&agrave;ng kh&ocirc;ng th&iacute;ch nợ nần hoặc c&oacute; khi họ cũng kh&oacute; cho ai vay mượn.</p>\r\n\r\n<p>Khi Bảo B&igrave;nh tưởng tượng, n&agrave;ng c&oacute; thể nghĩ ra bất cứ trường hợp n&agrave;o, bất cứ điều g&igrave; m&agrave; người kh&aacute;c nghĩ l&agrave; bất khả thi, l&agrave; lập dị.</p>\r\n\r\n<p><strong>Bảo B&igrave;nh &ndash; C&oacute; thể thuộc về tất cả, c&oacute; thể kh&ocirc;ng thuộc về bất k&igrave; một ai</strong><br />\r\nTrong t&igrave;nh y&ecirc;u, Bảo B&igrave;nh rất đặc biệt, n&agrave;ng c&oacute; thể rất chung thủy với bạn nhưng cũng c&oacute; thể l&atilde;nh đạm với bạn bởi v&igrave; n&agrave;ng c&oacute; thể thuộc về tất cả, n&agrave;ng cũng c&oacute; thể kh&ocirc;ng thuộc về bất k&igrave; ai. Vậy n&ecirc;n để Bảo B&igrave;nh chung thủy với bạn, bạn cần cho n&agrave;ng sự tự do cần thiết.</p>\r\n\r\n<p>T&igrave;nh y&ecirc;u đối với Bảo B&igrave;nh l&agrave; kh&ocirc;ng phải l&agrave; một đam m&ecirc; ch&aacute;y bỏng m&agrave; ẩn chứa n&eacute;t tinh tế, nhẹ nh&agrave;ng v&agrave; ch&acirc;n th&agrave;nh. B&ecirc;n ngo&agrave;i, c&ocirc; n&agrave;ng l&agrave; người c&aacute; t&iacute;nh mạnh nhưng s&acirc;u trong t&acirc;m lại l&agrave; một c&ocirc; g&aacute;i v&ocirc; c&ugrave;ng thuần khiết.</p>\r\n\r\n<p>Họ l&agrave; những c&ocirc; n&agrave;ng dễ chịu, khoan dung, kh&ocirc;ng th&iacute;ch chỉ tr&iacute;ch người kh&aacute;c, kh&ocirc;ng hay ghen tu&ocirc;ng hay đ&ograve;i hỏi một c&aacute;ch v&ocirc; l&yacute;. N&agrave;ng Bảo B&igrave;nh cũng kh&ocirc;ng phải mẫu người sướt mướt, ủy mị hay n&iacute;u k&eacute;o bất cứ người n&agrave;o kh&ocirc;ng cần đến c&ocirc; ấy. Tất cả những g&igrave; n&agrave;ng cần l&agrave; một người đ&agrave;n &ocirc;ng lu&ocirc;n đem đến cho họ nhiều điều th&uacute; vị v&agrave; đặc biệt.</p>\r\n\r\n<p><strong>Bảo B&igrave;nh &ndash; C&oacute; khả năng trực gi&aacute;c v&agrave; ti&ecirc;n đo&aacute;n, chu đ&aacute;o với c&aacute;c con</strong><br />\r\nBảo B&igrave;nh c&oacute; khả năng trực gi&aacute;c v&agrave; t&agrave;i ti&ecirc;n đo&aacute;n hiếm c&oacute;. Bởi v&igrave; Bảo B&igrave;nh l&agrave; những c&ocirc; n&agrave;ng của tương lai, biết r&otilde; những g&igrave; sẽ diễn ra v&agrave;o ng&agrave;y mai.</p>\r\n\r\n<p>L&agrave; một người mẹ, n&agrave;ng rất chu đ&aacute;o v&agrave; lu&ocirc;n muốn chăm s&oacute;c tận t&igrave;nh cho c&aacute;c con của m&igrave;nh. B&agrave; mẹ Bảo B&igrave;nh cực kỳ t&acirc;m l&yacute; v&agrave; biết lắng nghe c&aacute;c con. B&ecirc;n cạnh đ&oacute;, kỷ luật l&agrave; điều kh&ocirc;ng thể thiếu khi c&ocirc; ấy dạy dỗ lũ trẻ. N&agrave;ng dạy cho con c&aacute;i t&igrave;nh y&ecirc;u thương nh&acirc;n loại, sống trung thực. N&agrave;ng sẽ kh&ocirc;ng bao giờ tr&aacute;ch phạt ch&uacute;ng v&igrave; sự ngay thẳng, cho d&ugrave; sự thật được n&oacute;i ra c&oacute; cay đắng đến mấy.</p>\r\n', '/uploads/img/1479208267_bao-binh.png', NULL, 1, '2016-11-15 18:11:07', '2016-11-15 18:11:07'),
(14, 'Khám phá bí mật tài vận của Song Ngư', '<p>Kh&aacute;m ph&aacute; b&iacute; mật t&agrave;i vận của Song Ngư. Những b&iacute; mật về t&agrave;i vận của Song Ngư &ndash; ch&ograve;m sao cuối c&ugrave;ng của&nbsp;12 cung ho&agrave;ng đạo! nếu bạn l&agrave; song ngữ th&igrave; h&atilde;y xem thử nh&eacute; !</p>\r\n\r\n<p>Người thuộc ch&ograve;m sao Song Ngư thường c&oacute; tr&iacute; tuệ phong ph&uacute; v&agrave; vận dụng tr&iacute; tuệ của m&igrave;nh v&agrave;o những mục đ&iacute;ch quan trọng. Họ biết c&aacute;ch đạt được th&agrave;nh c&ocirc;ng, t&igrave;m được hạnh ph&uacute;c trong cuộc sống.</p>\r\n\r\n<p>Tr&iacute; tuệ phong ph&uacute; dẫn đường khiến Song Ngư kh&ocirc;ng bao giờ hồ đồ. Họ c&oacute; con mắt tinh anh, x&aacute;c định ch&iacute;nh x&aacute;c cơ hội v&agrave; th&aacute;ch thức trong c&ocirc;ng việc, kh&ocirc;ng h&agrave;nh động m&ugrave; qu&aacute;ng bất chấp hậu quả. V&igrave; thế, hiếm khi thấy Ngư Ngư bị dồn v&agrave;o ch&acirc;n tường hoặc c&ugrave;ng đường kh&ocirc;ng lối tho&aacute;t.</p>\r\n\r\n<p>Trước khi đầu tư hoặc tiến h&agrave;nh một c&ocirc;ng việc mới, người cung Song Ngư đ&atilde; l&ecirc;n kế hoạch b&agrave;i bản, suy x&eacute;t kỹ c&agrave;ng trước sau rồi mới h&agrave;nh động để c&aacute;c bước đều được tiến h&agrave;nh ăn khớp, nhịp nh&agrave;ng nhằm gi&agrave;nh được thắng lợi cuối c&ugrave;ng. Kh&ocirc;ng chỉ vậy, Ngư Ngư c&ograve;n c&oacute; tầm nh&igrave;n xa, linh hoạt trong c&aacute;c t&igrave;nh huống, lu&ocirc;n hướng tới lợi &iacute;ch l&acirc;u d&agrave;i thay v&igrave; những điều nhỏ nhặt trước mắt.</p>\r\n\r\n<p>Tuy nhi&ecirc;n, nhược điểm lớn nhất của người cung Song Ngư l&agrave; nh&uacute;t nh&aacute;t, thiếu dũng cảm. Đ&ocirc;i khi t&iacute;nh to&aacute;n qu&aacute; nhiều hoặc kh&ocirc;ng d&aacute;m mạo hiểm sẽ khiến cơ hội tr&ocirc;i qua một c&aacute;ch đ&aacute;ng tiếc.</p>\r\n\r\n<p>Hơn thế nữa, Song Ngư t&iacute;nh to&aacute;n th&igrave; rất nhanh nhẹn nhưng kh&ocirc;ng biết quản l&yacute; tiền bạc hiệu quả. L&uacute;c cần ch&uacute;t mạo hiểm th&igrave; họ rụt r&egrave;, khi cần thận trọng th&igrave; họ lại cảm t&iacute;nh n&ecirc;n đầu tư kh&ocirc;ng thu được lợi nhuận như mong muốn. Nếu khắc phục được điều n&agrave;y th&igrave; t&agrave;i vận của Song Ngư sẽ rất kh&aacute; khẩm.</p>\r\n', '/uploads/img/1479208372_song-ngu.png', NULL, 1, '2016-11-15 18:12:52', '2016-11-15 18:12:52');

-- --------------------------------------------------------

--
-- Table structure for table `post_comment`
--

CREATE TABLE `post_comment` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `parent_id` int(10) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `post_comment`
--

INSERT INTO `post_comment` (`id`, `post_id`, `content`, `created_by`, `parent_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'Vòng tròn Hoàng đạo chia thành 12 cung, phân ra bốn nguyên tố đã tạo ra của thế giới theo quan niệm cổ phương Tây: đất, lửa, nước, khí.', 1, NULL, '2016-11-15 17:24:24', '2016-11-15 17:24:24'),
(2, 1, '12 cung tương ứng với 12 ngôi sao và hành tinh, bao gồm cả Diêm Vương Tinh đã bị loại khỏi danh sách các hành tinh của hệ Mặt Trời:', 1, NULL, '2016-11-15 17:37:45', '2016-11-15 17:37:45'),
(3, 13, 'Là một người mẹ, nàng rất chu đáo và luôn muốn chăm sóc tận tình cho các con của mình. Bà mẹ Bảo Bình cực kỳ tâm lý và biết lắng nghe các con', 1, NULL, '2016-11-18 14:27:39', '2016-11-18 14:27:39'),
(4, 13, 'Họ là những cô nàng dễ chịu, khoan dung, không thích chỉ trích người khác, không hay ghen tuông hay đòi hỏi một cách vô lý', 1, NULL, '2016-11-18 15:05:00', '2016-11-18 15:05:00');

-- --------------------------------------------------------

--
-- Table structure for table `post_likes`
--

CREATE TABLE `post_likes` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '2016-10-10 06:55:10', '2016-10-10 06:55:10'),
(2, 'Moderator', '2016-10-10 06:55:10', '2016-10-10 06:55:10'),
(3, 'User', '2016-10-10 06:55:10', '2016-10-10 06:55:10');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `content`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Route groups allow you to share route attributes, such as middleware or namespaces, across a large number of routes without needing to define those attributes on each individual route. Shared attributes are specified in an array format as the first parameter to the Route::group method.', 1, '2016-11-14 09:47:37', '2016-11-14 09:47:37'),
(2, 'This single route declaration creates multiple routes to handle a variety of actions on the resource. The generated controller will already have methods stubbed for each of these actions, including notes informing you of the HTTP verbs and URIs they handle.', 1, '2016-11-14 09:54:06', '2016-11-14 09:54:06'),
(3, 'This single route declaration creates multiple routes to handle a variety of actions on the resource. The generated controller will already have methods stubbed for each of these actions, including notes informing you of the HTTP verbs and URIs they handle.', 1, '2016-11-15 17:37:24', '2016-11-15 17:37:24'),
(4, 'Khám phá bí mật tài vận của Song Ngư', 15, '2016-11-20 02:31:23', '2016-11-20 02:31:23');

-- --------------------------------------------------------

--
-- Table structure for table `status_attachment`
--

CREATE TABLE `status_attachment` (
  `id` int(10) UNSIGNED NOT NULL,
  `status_id` int(10) UNSIGNED NOT NULL,
  `attachment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `status_comment`
--

CREATE TABLE `status_comment` (
  `id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `status_id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `status_comment`
--

INSERT INTO `status_comment` (`id`, `content`, `status_id`, `parent_id`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Next, you may register a resourceful route to the controller:', 2, NULL, 1, '2016-11-14 10:22:10', '2016-11-14 10:22:10'),
(2, 'This single route declaration creates multiple routes to handle a variety of actions on the resource. The generated controller will already have methods stubbed for each of these actions, including notes informing you of the HTTP verbs and URIs they handle.', 2, NULL, 1, '2016-11-14 10:23:05', '2016-11-14 10:23:05'),
(3, 'Controller''s also allow you to register middleware using a Closure. This provides a convenient way to define a middleware for a single controller without defining an entire middleware class:', 1, NULL, 1, '2016-11-14 10:23:30', '2016-11-14 10:23:30'),
(4, 'The generated controller will already have methods stubbed for each of these actions', 2, NULL, 1, '2016-11-19 19:56:57', '2016-11-19 19:56:57');

-- --------------------------------------------------------

--
-- Table structure for table `status_image`
--

CREATE TABLE `status_image` (
  `id` int(10) UNSIGNED NOT NULL,
  `status_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `status_likes`
--

CREATE TABLE `status_likes` (
  `id` int(10) UNSIGNED NOT NULL,
  `status_id` int(10) UNSIGNED NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `status_likes`
--

INSERT INTO `status_likes` (`id`, `status_id`, `created_by`, `created_at`, `updated_at`) VALUES
(3, 2, 1, '2016-11-19 18:57:37', '2016-11-19 18:57:37');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `sex` enum('male','female') COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `role_id` int(10) NOT NULL DEFAULT '3',
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `religion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `name`, `email`, `description`, `sex`, `birthday`, `role_id`, `avatar`, `phone`, `address`, `religion`, `active`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'thanhfreewings', 'Thanh Nguyen', 'nguyenvanthanh9595@gmail.com', 'Example', 'male', '1995-12-14 00:00:00', 1, '/uploads/avatars/1479094488_a4.jpg', '01677713817', 'Ha Noi, Viet Nam', 'Không', 'N', '$2y$10$5TuptzwC7xoJwPj/8SGBMOkNYc393FjCkHSNBrzFIwGe6rvRM3Ipi', 'H2MCwyg50s426gxyikjBxTyjsodjvQs11ZMsce52GEy9sjJIMRPvnWA9U1vQ', '2016-11-05 02:28:55', '2016-11-19 17:38:41'),
(2, NULL, 'Hòa', 'hoa@example.com', NULL, 'male', '1995-02-17 00:00:00', 2, NULL, NULL, NULL, NULL, 'N', '$2y$10$0zF9DFuBH8bXal765wthy.eF4a4/0HOuaCQPvpPDriTMrzvAS0dba', 'xVWKul1qN9DRipcUX5CG1zPoLUrcx789nuZzk8eYXgHJXebBC0gyo87xStw7', '2016-11-05 03:54:41', '2016-11-12 09:49:06'),
(3, NULL, 'Tuấn', 'tieutu881993@gmail.com', NULL, 'male', '1995-03-22 00:00:00', 2, NULL, NULL, NULL, NULL, 'N', '$2y$10$hiXwQJDDQXruL..MmtMsSOa1cjo63KAIM7loYSFEib0GBctaFuAhe', 'Sz5i3BCuJCPqYbsekV426xvdLT4GXPMRY6KJn0joWImqQkrSc3N4r1kBuwXB', '2016-11-07 01:24:27', '2016-11-07 01:24:32'),
(4, NULL, 'Phong', 'phong@example.com', NULL, 'male', '1996-04-25 00:00:00', 3, '/uploads/avatars/1479456098_a1.jpg', NULL, NULL, NULL, 'N', '$2y$10$xGY4vWnBw/8nQQ/z0QGvjeYg5.JNzrfnii87Ztwx6IXwgVf33rIVW', 'hCXLiNqRmVrpOjsc5ilGE6MnKkirziaS0mWZuZKg6zuSXtjdl6RTJQnOvcAo', '2016-11-07 01:25:08', '2016-11-18 08:01:38'),
(5, NULL, 'Hải Nguyễn', 'hainguyen@example.com', NULL, 'male', '1994-06-17 00:00:00', 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$saCnkVUKFycCwR5yBykDKOMXt5vmWva5MtqStp9m5xOYk04lbzzeu', 'R2uKoRFgLHyCRlFijGLS2lSTF7ywIybaJtVh6Dlx6KzKKXVx5j9Q8WmKah9O', '2016-11-07 01:25:54', '2016-11-07 01:25:57'),
(6, NULL, 'Trọng', 'trong@example.com', NULL, 'male', '1992-07-14 00:00:00', 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$NTvWW54LTUlYShE.vjOn7uL.RXj/ipUJ155EMXfx/4MYRh0rHXQI.', 'hU5oHrFiMN0nqNFyRP8ZiTW28GYehU7CucB1zRnNE5GJvx5G8NRnSBHBe401', '2016-11-07 01:26:40', '2016-11-07 01:26:44'),
(7, NULL, 'Lượng', 'luong@example.com', NULL, 'male', '1989-07-25 00:00:00', 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$JrIbIwtXi48jFdRt905.ceNPNcEsQ8KfzTDq6eh5Nqa/LqfCijt/G', 'B7T8u0hZoUOe0tNbYm7CmTXM93Sqe33LElvWnMmCopZD2xibGdfsoNGmSFfB', '2016-11-07 01:27:23', '2016-11-10 11:23:35'),
(8, NULL, 'Phương', 'phuong@example.com', NULL, 'female', '1998-09-18 00:00:00', 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$tShsFPLbfbRlNh6KP1INu.iJccJ5vTRBXcVcXeLOBRbism.7Urm8a', '95hIiun3tFYQ28m1ST8Gwq61J7u14d4r28KslQN8THfDceRvdRFhfv9TdS1c', '2016-11-07 01:28:24', '2016-11-19 15:32:06'),
(9, NULL, 'Trần Hiếu', 'tranhieu@example.com', NULL, 'male', '1999-10-23 00:00:00', 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$NX/WXB0xdr/mE8THX35XJunYWhaGz//uf7fKzOyqHy.g/uXyggTAO', '1ES97bXMA62uWyBGMRsnKMhPR8WoS1K5yhB69fGaxakw1Y865AE5aohsbFFK', '2016-11-07 01:29:17', '2016-11-10 10:42:08'),
(10, NULL, 'Minh', 'minh@example.com', NULL, 'male', '1995-11-07 00:00:00', 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$cVlp4MfcRss1pvBWIFA1ButlOaiUqdHoNx1PoWfeNUSfAHdGzWPrW', 'aQ1IeTIf1kXKFXnByOQGH9sjjjtbDLsKb17E5zWElYiezxDDI2jmhu9k8Sw6', '2016-11-07 01:36:08', '2016-11-07 01:36:25'),
(11, NULL, 'Nguyễn Bảo', 'nguyenbao@example.com', NULL, 'male', '1993-12-21 00:00:00', 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$RA1I7lEe4EuN2u.chOUA4On4pK54QthEjc2IetcvLI0ueXcPfpaTO', 'adlJiR1XfMc85aw2LuUupVIsMVtAvEi4xKaPNhXpKzslOOyNKWUtE1RyAZUg', '2016-11-07 01:37:08', '2016-11-07 01:37:11'),
(12, NULL, 'Trang Nguyễn', 'trangnguyen@example.com', NULL, 'female', '1994-01-03 00:00:00', 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$yhURHm9EEJGP3A6ehCkA5e2JGLIU4awPa/ThCtl8sBqe5OQgAzYua', 'eEOBRoe4BHpwFuFK12ZsEOXnNmgbmGbSfZkPnvtp56cCfMhn0abvGxF1n1mp', '2016-11-07 01:37:58', '2016-11-07 01:41:16'),
(13, NULL, 'Quỳnh', 'quynh@example.com', NULL, 'female', '1995-01-20 00:00:00', 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$.OWA2jyPWKnr0yab5x0jLOA7GgfA/G5S6TobqgNOOWekqMaqXq1Zy', 'yp7cpkDOx5v1f9OhRNyPoqGwyeDtdw4hsGCO86D76GyXMVaJ5Lqo9ZRZyI90', '2016-11-07 01:42:25', '2016-11-07 01:46:13'),
(14, NULL, 'Thủy', 'thuy@example.com', NULL, 'female', '1997-02-23 00:00:00', 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$tz.o82oYn6EpyNSvqKnoaeZEw3lMeTFJQZvR1eQzGqCR.fSDYom1y', 'rG8NadqYvCaIoxQG5ccfEwpaWZMDC8iiIbRARpJO3ZLVmtKZyvOAa7oMhoQW', '2016-11-07 01:48:29', '2016-11-07 01:51:53'),
(15, NULL, 'Thu', 'thu@example.com', NULL, 'female', '1999-10-17 00:00:00', 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$c9Tq7vlJSDgRd63NnHEQQuICKFUdQ98p7j4JfHpL6UyPbPyygcvu.', '8vEAtiR0snIWuR12YurR7Oh0cWviuzZdcp0fBcKahh25a5Lv2A86v2FMC6jy', '2016-11-07 01:54:45', '2016-11-07 01:58:55'),
(16, NULL, 'Hương', 'huong@example.com', NULL, 'female', '1996-11-20 00:00:00', 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$6e0IIuK9xdAJPtBRFylJh.ajKafXxHuGASKgLn4aspiPszxDcVp.a', 'FrOw2Yr3IKJ1MYfmOiLcfewRSS6o6uFd9dIjFRY5WUEGU7WitzlpyiZaBDCf', '2016-11-07 01:59:35', '2016-11-07 02:12:47'),
(17, NULL, 'Nhung', 'nhung@example.com', NULL, 'female', '1991-05-19 00:00:00', 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$jYxi0AdE/xsbJ.aoC0XM4emWifWbeczUvr4nfelppp1Ul3jT8tkXG', 'iLNXssXzAabWvn1qF2LUMkPUadUiGGxIyPePLs2NobXorV1szY5RgqXNo2Uj', '2016-11-07 02:13:16', '2016-11-07 02:16:40'),
(18, NULL, 'Ngọc', 'ngoc@example.com', NULL, 'female', '1994-12-25 00:00:00', 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$c2/a/RcyzDBFPoebjd2Pe.uBQFKpSAciAG6j0lWn62HslR7Dpuf1u', 'cxTyyn3uDP7NecJ7VwHjxtCPhanEDOQT8DOKEk0K3x2O5RlElqz5DAIPFpMu', '2016-11-07 02:17:21', '2016-11-07 02:23:53'),
(19, '65655', 'Leo', 'leo@example.com', NULL, 'female', '1999-10-18 00:00:00', 3, NULL, NULL, NULL, NULL, 'N', '$2y$10$aWQPFqjZlSdD3/JIjovDoOkp6tl6lHc6KIbZdPmlLlH6iOx3y7zue', 'kl2jxDhfmjuxdI3CAprRp6mqjfBjrOczndzNSQ3yOEkTyV0RSWJvpIUkVkNv', '2016-11-10 15:55:42', '2016-11-10 17:36:38');

-- --------------------------------------------------------

--
-- Table structure for table `users_group`
--

CREATE TABLE `users_group` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_group`
--

INSERT INTO `users_group` (`id`, `user_id`, `group_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2016-11-11 15:43:35', '2016-11-11 15:43:35'),
(2, 2, 1, '2016-11-11 15:43:35', '2016-11-11 15:43:35'),
(3, 3, 1, '2016-11-11 15:43:35', '2016-11-11 15:43:35'),
(4, 1, 2, '2016-11-15 21:02:18', '2016-11-15 21:02:18'),
(5, 3, 2, '2016-11-15 21:02:18', '2016-11-15 21:02:18'),
(6, 5, 2, '2016-11-15 21:02:18', '2016-11-15 21:02:18'),
(7, 6, 2, '2016-11-15 21:02:18', '2016-11-15 21:02:18'),
(8, 11, 2, '2016-11-15 21:02:18', '2016-11-15 21:02:18'),
(9, 1, 3, '2016-11-17 17:06:56', '2016-11-17 17:06:56'),
(10, 8, 3, '2016-11-17 17:06:56', '2016-11-17 17:06:56'),
(11, 11, 3, '2016-11-17 17:06:56', '2016-11-17 17:06:56'),
(12, 1, 4, '2016-11-18 14:24:59', '2016-11-18 14:24:59'),
(13, 4, 4, '2016-11-18 14:24:59', '2016-11-18 14:24:59'),
(14, 5, 4, '2016-11-18 14:24:59', '2016-11-18 14:24:59'),
(15, 11, 4, '2016-11-18 14:24:59', '2016-11-18 14:24:59'),
(16, 12, 4, '2016-11-18 14:24:59', '2016-11-18 14:24:59');

-- --------------------------------------------------------

--
-- Table structure for table `user_relationship`
--

CREATE TABLE `user_relationship` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_one_id` int(10) UNSIGNED NOT NULL,
  `user_two_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `action_user_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_relationship`
--

INSERT INTO `user_relationship` (`id`, `user_one_id`, `user_two_id`, `status`, `action_user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 1, 1, '2016-11-08 06:09:48', '2016-11-18 15:35:15'),
(2, 1, 3, 1, 1, '2016-11-08 07:20:36', '2016-11-08 17:09:46'),
(3, 1, 4, 1, 1, '2016-11-08 17:10:58', '2016-11-08 17:11:27'),
(4, 1, 5, 1, 1, '2016-11-08 17:11:21', '2016-11-08 17:23:55'),
(6, 1, 6, 1, 1, '2016-11-08 17:24:40', '2016-11-08 17:24:50'),
(7, 1, 8, 1, 1, '2016-11-08 17:24:45', '2016-11-08 17:25:15'),
(8, 1, 9, 0, 1, '2016-11-08 17:28:32', '2016-11-08 17:28:32'),
(9, 1, 11, 1, 1, '2016-11-09 11:46:30', '2016-11-09 11:46:35'),
(10, 1, 12, 1, 1, '2016-11-10 17:01:41', '2016-11-10 17:02:09'),
(11, 7, 1, 1, 1, '2016-11-10 18:00:27', '2016-11-14 10:42:18'),
(13, 1, 10, 1, 10, '2016-11-11 15:08:38', '2016-11-11 15:09:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sender_id` (`sender_id`),
  ADD KEY `receiver_id` (`receiver_id`);

--
-- Indexes for table `messages_group`
--
ALTER TABLE `messages_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sender_id` (`sender_id`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `post_comment`
--
ALTER TABLE `post_comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `post_likes`
--
ALTER TABLE `post_likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `status_attachment`
--
ALTER TABLE `status_attachment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indexes for table `status_comment`
--
ALTER TABLE `status_comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `status_id` (`status_id`);

--
-- Indexes for table `status_image`
--
ALTER TABLE `status_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_id` (`status_id`);

--
-- Indexes for table `status_likes`
--
ALTER TABLE `status_likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status_id` (`status_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `users_group`
--
ALTER TABLE `users_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `user_relationship`
--
ALTER TABLE `user_relationship`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_one_id` (`user_one_id`),
  ADD KEY `user_two_id` (`user_two_id`),
  ADD KEY `action_user_id` (`action_user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `messages_group`
--
ALTER TABLE `messages_group`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `post_comment`
--
ALTER TABLE `post_comment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `post_likes`
--
ALTER TABLE `post_likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `status_attachment`
--
ALTER TABLE `status_attachment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `status_comment`
--
ALTER TABLE `status_comment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `status_image`
--
ALTER TABLE `status_image`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `status_likes`
--
ALTER TABLE `status_likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `users_group`
--
ALTER TABLE `users_group`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `user_relationship`
--
ALTER TABLE `user_relationship`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `groups`
--
ALTER TABLE `groups`
  ADD CONSTRAINT `groups_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `messages_ibfk_2` FOREIGN KEY (`receiver_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `messages_group`
--
ALTER TABLE `messages_group`
  ADD CONSTRAINT `messages_group_ibfk_1` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `messages_group_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`);

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `post_comment`
--
ALTER TABLE `post_comment`
  ADD CONSTRAINT `post_comment_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
  ADD CONSTRAINT `post_comment_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `post_likes`
--
ALTER TABLE `post_likes`
  ADD CONSTRAINT `post_likes_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
  ADD CONSTRAINT `post_likes_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `status`
--
ALTER TABLE `status`
  ADD CONSTRAINT `status_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `status_attachment`
--
ALTER TABLE `status_attachment`
  ADD CONSTRAINT `status_attachment_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`);

--
-- Constraints for table `status_comment`
--
ALTER TABLE `status_comment`
  ADD CONSTRAINT `status_comment_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `status_comment_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`);

--
-- Constraints for table `status_image`
--
ALTER TABLE `status_image`
  ADD CONSTRAINT `status_image_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`);

--
-- Constraints for table `status_likes`
--
ALTER TABLE `status_likes`
  ADD CONSTRAINT `status_likes_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`),
  ADD CONSTRAINT `status_likes_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `users_group`
--
ALTER TABLE `users_group`
  ADD CONSTRAINT `users_group_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `users_group_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`);

--
-- Constraints for table `user_relationship`
--
ALTER TABLE `user_relationship`
  ADD CONSTRAINT `user_relationship_ibfk_1` FOREIGN KEY (`user_one_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_relationship_ibfk_2` FOREIGN KEY (`user_two_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_relationship_ibfk_3` FOREIGN KEY (`action_user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

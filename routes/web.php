<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/error', function(){
    return view('errors.503');
});
Route::get('/introduce', 'HomeController@getIntroduce');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index');

    Route::post('/status/like', 'StatusController@postLike');
    Route::post('/blog/post/like', 'PostController@postLike');

    Route::get('/user/friend', 'UserController@getFriend');
    Route::post('/user/search', 'UserController@postSearch');
    Route::get('/user/list', 'UserController@getList');
    Route::post('/user/role/{id}', 'UserController@postChangeRole');

    Route::post('/group/leave/{id}', 'GroupController@postLeaveGroup');

    Route::resource('/user', 'UserController');
    Route::resource('/group', 'GroupController');
    Route::resource('/status', 'StatusController');
    Route::resource('/diary', 'DiaryController');
    Route::resource('/status/comment', 'StatusCommentController');
    Route::resource('/blog/post', 'PostController');
    Route::resource('/blog/comment', 'PostCommentController');

    Route::get('/relationship/index', 'UserRelationshipController@getIndex');
    Route::post('/relationship/create', 'UserRelationshipController@postCreate');
    Route::post('/relationship/accepted', 'UserRelationshipController@postAccepted');
    Route::post('/relationship/declined', 'UserRelationshipController@postDeclined');
    Route::post('/relationship/blocked', 'UserRelationshipController@postBlocked');
    Route::post('/relationship/delete', 'UserRelationshipController@postDelete');
    Route::get('/relationship/declined/view', 'UserRelationshipController@getDeclined');
    Route::get('/relationship/blocked/view', 'UserRelationshipController@getBlocked');
    Route::post('/relationship/block', 'UserRelationshipController@postBlock');

    Route::get('/message', 'MessageController@getIndex');
    Route::get('/message/new', 'MessageController@getNew');
    Route::post('/message/new', 'MessageController@postNew');
    Route::get('/message/{id}', 'MessageController@getShow');
    Route::post('/message/create/{id}', 'MessageController@postCreate');
    Route::post('/message/group/create/{id}', 'MessageController@postCreateGroup');

});

Auth::routes();

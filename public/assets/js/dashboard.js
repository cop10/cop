$(document).ready(function(){
    $("a[href='#moveTop']").click(function() {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });
    $(window).on("scroll", function() {
        var scrollPos = $(window).scrollTop();
        if (scrollPos <= 150) {
            $(".btn-move-top").fadeOut();
        } else {
            $(".btn-move-top").fadeIn();
        }
    });
    $(".hide-menu").click(function(){
        $('.hide-menu').addClass('hidden');
        $('.show-menu').removeClass('hidden');
        $('.sidebar').addClass('hidden');
        $('.page-content').css('position','static');
    });
    $(".show-menu").click(function(){
        $('.hide-menu').removeClass('hidden');
        $('.show-menu').addClass('hidden');
        $('.sidebar').removeClass('hidden');
        $('.page-content').css('position','absolute');
    });
    $('.dropdown-toggle').click(function (event) {
        event.preventDefault();
        $(this).next('.dropdown').toggle();
        $(this).children('.glyphicon-menu-left').css({
            '-webkit-transform':'rotate(-90deg)',
            '-moz-transform':'rotate(-90deg)',
            '-o-transform':'rotate(-90deg)',
            '-ms-transform':'rotate(-90deg)',
            'transform':'rotate(-90deg)'});
        $(this).parent('li').css('background-color','#243d3d');
    });
    $(document).click(function (e) {
        var target = e.target;
        if (!$(target).is('.dropdown-toggle') && !$(target).parents().is('.dropdown-toggle')) {
            $('.dropdown').hide();
            $('.glyphicon-menu-left').css({
            '-webkit-transform':'rotate(0deg)',
            '-moz-transform':'rotate(0deg)',
            '-o-transform':'rotate(0deg)',
            '-ms-transform':'rotate(0deg)',
            'transform':'rotate(0deg)'});
            $('.sidebar > ul > li').css('background-color','#2F4F4F');
        }
    });
    $('#addTag').click(function(){
        $(".first-tag").clone().appendTo(".group-tag").removeClass('first-tag').val('');
        $('.remove-tag').removeClass('hidden');
    });
    $('.remove-tag').click(function(){
        $('.group-tag').children().remove();
        $(".first-tag").val('');
        $(this).addClass('hidden');
    });
});
<!DOCTYPE html>
<html>
    <head>
        <title>Be right back.</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <style>
            html, body {
                height: 100%;
            }
            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                background-color: #f2f2f2;
                font-family: sans-serif;
            }
            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }
            .content {
                text-align: center;
                display: inline-block;
            }
            .title {
                font-size: 30px;
                margin-bottom: 10px;
            }
            a{
                color: #B0BEC5;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">@if(\Auth::user()->language == 'VN') Bạn không được phép truy cập địa chỉ này. @else You are not allowed to access this address. @endif</div>
                <a href="/">@if(\Auth::user()->language == 'VN') Trang chủ @else Home @endif</a>
            </div>
        </div>
    </body>
</html>

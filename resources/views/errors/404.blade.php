@extends('layouts.index')

@section('content')
<div class="col-md-12 box box-min">
    <h1 class="text-muted text-center">Địa chỉ trang web không tồn tại...!</h1>
    <p class="text-muted text-center">Trở về <a href="/">Trang chủ</a></p>
</div>
@stop
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <h1>Something wrong happened, we will fix it soon!!!</h1>
            <h2>Go to our <a href="/">home page</a></h2>                
        </div>

    </div>
</div>
@stop

@section('extrascripts')

@stop

@extends('layouts.index')

@section('title') List users @endsection

@section('content')
<div class="col-md-12 box">
    <div class="row">
        <div class="col-md-12">
            <a href="javascript:;"><label>@if(\Auth::user()->language == 'VN') Người dùng @else Users @endif</label></a>
        </div>
        <div class="col-md-12">
            <table class="table table-hover">
                @foreach($users as $user)
                <tr>
                    <td><a href="/user/{{$user->id}}"><img src="{{$user->getAvatar()}}" class="img-circle" height="30" width="30"> {{$user->name}}</a></td>
                    <td><small class="text-muted">{{$user->email}}</small></td>
                    <td>
                        @if($user->role_id == 1)
                        <label class="label label-success">Admin</label>
                        @elseif($user->role_id == 2)
                        <label class="label label-info">Moderator</label>
                        @endif
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
@endsection

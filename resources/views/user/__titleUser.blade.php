<div class="col-md-12">
    <label><a href="/user/{{\Auth::user()->id}}">@if(\Auth::user()->language == 'VN') Tài khoản @else Profile @endif</a></label>
    <ul class="list-inline pull-right">
        <li><a href="/user" class="{{ \Request::is( 'user') ? 'nav-active' : '' }}">@if(\Auth::user()->language == 'VN') Cập nhật @else Update @endif</a></li>
        <li><a href="/user/{{\Auth::user()->id}}/edit" class="{{ \Request::is( 'user/*/edit') ? 'nav-active' : '' }}">@if(\Auth::user()->language == 'VN') Bảo mật @else Security @endif</a></li>
        <li><a href="/diary" class="{{ \Request::is( 'diary*') ? 'nav-active' : '' }}">@if(\Auth::user()->language == 'VN') Nhật ký @else Diary @endif</a></li>
        <li><a href="/user/friend" class="{{ \Request::is( 'user/friend') ? 'nav-active' : '' }}">@if(\Auth::user()->language == 'VN') Bạn bè ({{count(\App\Models\UserRelationship::getFriend())}}) @else Friends ({{count(\App\Models\UserRelationship::getFriend())}}) @endif</a></li>
    </ul>
    <hr style="margin-top: 5px">
</div>
<style type="text/css">
    .nav-active{
        color: #000;
    }
</style>

@extends('layouts.index')

@section('title') Friends @endsection

@section('content')
<div class="col-md-12 box box-min">
    <div class="row">
        @include('user.__titleUser')
        <div class="col-md-12">
            @if(count(\App\Models\UserRelationship::getFriend()) == 0)
            <p class="text-muted"><label>@if(\Auth::user()->language == 'VN') Bạn chưa có bạn bè, hãy kết bạn để làm quen với mọi người. @else You have no friends, make friends to get to know everyone. @endif</label></p>
            @endif
            @foreach(\App\Models\UserRelationship::getFriend() as $friend)
            <div class="col-md-6 box-my-friend" id="{{$friend->user()->name}}">
                <div class="bg-friend">
                    <a href="/user/{{$friend->user()->id}}"><img src="{{$friend->user()->getAvatar()}}" height="100" width="100"> <label>{{$friend->user()->name}}</label></a>
                    <small class="text-muted link1">{{count($friend->user()->friend())}} @if(\Auth::user()->language == 'VN') bạn bè @else @if(count($friend->user()->friend()) >= 2) friends @else friend @endif @endif<br>(@if($friend->user()->sex == 'male')<i class="fa fa-mars"></i>@else<i class="fa fa-venus"></i>@endif {{$friend->user()->getHoroscope()}})</small>
                    <a href="/message/{{$friend->user()->id}}" class="btn btn-xs link2 btn-theme"><i class="fa fa-comments"></i> @if(\Auth::user()->language == 'VN') Tin nhắn @else Message @endif</a>
                    </ul>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
<style type="text/css">
    .box-my-friend{
        padding: 5px;
        margin: 0;
        height: 110px;
    }
    .box-my-friend .bg-friend{
        background-color: #f9f9f9;
        border: 1px solid #f2f2f2;
        height: 102px;
    }
    .box-my-friend .bg-friend label{
        position: absolute;
        top: 12px;
        left: 111px;
    }
    .box-my-friend .bg-friend .link2{
        position: absolute;
        right: 10px;
        bottom: 10px;
    }
    .box-my-friend .bg-friend .link1{
        position: absolute;
        left: 111px;
        top: 35px;
    }
</style>
@endsection

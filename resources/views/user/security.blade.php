@extends('layouts.index')

@section('title') Password @endsection

@section('content')
<div class="col-md-12 box box-min">
    <div class="row">
        @include('user.__titleUser')
        <div class="col-md-12">
            <div class="col-md-8 col-md-offset-2">
                <form action="/user/{{\Auth::user()->id}}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="form-group">
                        <label>@if(\Auth::user()->language == 'VN') Mật khẩu hiện tại @else Current password @endif</label>
                        <input type="password" placeholder="Current password" name="old_password" class="form-control" required>
                        @if ($errors->has('old_password'))
                            <label class="text-danger">{{ $errors->first('old_password') }}</label>
                        @endif
                        @if(Session::has('error'))
                            <label class="text-danger">{{Session::get('error')}}</label>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>@if(\Auth::user()->language == 'VN') Mật khẩu mới @else New password @endif</label>
                        <input type="password" placeholder="New password" name="new_password" class="form-control" required>
                        @if ($errors->has('new_password'))
                            <label class="text-danger">{{ $errors->first('new_password') }}</label>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>@if(\Auth::user()->language == 'VN') Nhập lại mật khẩu mới @else Password confirmation @endif</label>
                        <input type="password" placeholder="Password confirmation" name="confirm_new_password" class="form-control" required>
                        @if ($errors->has('confirm_new_password'))
                            <label class="text-danger">{{ $errors->first('confirm_new_password') }}</label>
                        @endif
                    </div>
                    <br>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-theme">@if(\Auth::user()->language == 'VN') Thay đổi mật khẩu @else Change password @endif</button>
                    </div>
                    <br>
                    <br>
                    <br>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

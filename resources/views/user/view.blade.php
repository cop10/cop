@extends('layouts.index')

@section('title') {{$user->name}} @endsection

@section('content')
<div class="col-md-12 box">
    <div class="row">
        @if($user->id == \Auth::user()->id)
        @include('user.__titleUser')
        @endif
        <div class="col-md-3">
            <a href="{{$user->getAvatar()}}" class="example-image-link" data-lightbox="example-1"><img src="{{$user->getAvatar()}}" class="example-image m-b-5" style="width: 100%;height: auto;" alt=""></a><br>
            @if($user->id == \Auth::user()->id)
            <a href="/user" class="btn btn-xs btn-block btn-default"><i class="fa fa-edit"></i>@if(\Auth::user()->language == 'VN') Cập nhật tài khoản @else Update profile @endif</a>
            @else
            <a href="/message/{{$user->id}}" class="btn btn-xs btn-block btn-outline pull-right"><i class="fa fa-comments"></i>@if(\Auth::user()->language == 'VN') Nhắn tin @else Message @endif</a>
            @endif
        </div>
        <div class="col-md-9">
            <h3 style="margin:0;color:#337ab9">{{$user->name}}</h3><br>
            @if($user->userRelationship())
            @if($user->userRelationship()->status == 1 || $user->userRelationship()->status == 3)
            <p class="text-muted"><label class="label label-success m-r-5"><i class="fa fa-check"></i>@if(\Auth::user()->language == 'VN') Bạn bè @else Friend @endif</label>@if(\Auth::user()->language == 'VN') từ @else at @endif {{date('d-M-Y',strtotime($user->userRelationship()->created_at))}}</p>
            @endif
            @endif
            <p class="text-muted"><i class="fa fa-envelope-o"></i> Email : {{$user->email}}</p>
            @if(\Auth::user()->role_id == 1 AND $user->id != \Auth::user()->id)
            <form action="/user/role/{{$user->id}}" id="changeRole" method="POST">
                <p class="text-muted m-r-5" style="float: left">Role: </p>
                {{ csrf_field() }}
                <select name="role_id" id="selectRole">
                    <option value="1" @if($user->role_id == 1) selected @endif>Admin</option>
                    <option value="2" @if($user->role_id == 2) selected @endif>Moderator</option>
                    <option value="3" @if($user->role_id == 3) selected @endif>User</option>
                </select>
            </form>
            <script type="text/javascript">
                $(document).ready(function(){
                    $('#selectRole').change(function(){
                        $('#changeRole').submit();
                    });
                });
            </script>
            @endif
            <p class="text-muted" style="clear: both"><i class="fa fa-phone"></i> @if(\Auth::user()->language == 'VN') Điện thoại @else Phone @endif : {{$user->phone}}</p>
            <p class="text-muted"><i class="fa fa-map-marker"></i>@if(\Auth::user()->language == 'VN') Địa chỉ @else Address @endif : {{$user->address}}</p>
            <p class="text-muted">
                <i class="fa fa-user"></i>
                @if(\Auth::user()->language == 'VN') Bạn bè @else Friends @endif :
                @if(count($user->friend()) == 0 AND $user->id != \Auth::user()->id)
                    @if(\Auth::user()->language == 'VN')
                        hãy là người đầu tiên kết bạn với
                        @if($user->sex == 'male') anh ấy @else cô ấy @endif
                    @else
                        Be the first to befriend
                        @if($user->sex == 'male') him @else her @endif
                    @endif
                @else
                {{count($user->friend())}}
                @endif
            </p>
            @if($user->userRelationship())
            <a role="button" data-placement="bottom" data-html="true" data-toggle="collapse" href="#collapseFriend" aria-expanded="false" aria-controls="collapseFriend">@if(\Auth::user()->language == 'VN') Tùy chọn @else Option @endif <span class="caret"></span></a>
            <div id="collapseFriend" class="collapse m-t-5">
                @if($user->userRelationship()->status == 3 AND $user->userRelationship()->action_user_id == \Auth::user()->id)
                <form method="POST" action="/relationship/blocked">
                    {{ csrf_field() }}
                    <input type="hidden" name="user_relationship_id" value="{{$user->userRelationship()->id}}">
                    <input type="hidden" name="status" value="1">
                    <button class="btn btn-xs" type="submit" style="float: left; margin-right: 5px">@if(\Auth::user()->language == 'VN') Bỏ chặn @else Unblock @endif</button>
                </form>
                @endif
                @if($user->userRelationship()->status == 1)
                <form method="POST" action="/relationship/blocked">
                    {{ csrf_field() }}
                    <input type="hidden" name="user_relationship_id" value="{{$user->userRelationship()->id}}">
                    <input type="hidden" name="status" value="3">
                    <button class="btn btn-xs" type="submit" style="float: left; margin-right: 5px">@if(\Auth::user()->language == 'VN') Chặn @else Block @endif</button>
                </form>
                @endif
                @if($user->userRelationship()->status == 1 || $user->userRelationship()->status == 3)
                <form method="POST" action="/relationship/delete">
                    {{ csrf_field() }}
                    <input type="hidden" name="user_relationship_id" value="{{$user->userRelationship()->id}}">
                    <button class="btn btn-xs" type="submit">@if(\Auth::user()->language == 'VN') Hủy kết bạn @else Unfriend @endif</button>
                </form>
                @endif
                @if($user->userRelationship()->status == 0 AND $user->userRelationship()->action_user_id != \Auth::user()->id)
                <form method="POST" action="/relationship/accepted">
                    {{ csrf_field() }}
                    <input type="hidden" name="user_relationship_id" value="{{$user->userRelationship()->id}}">
                    <input type="hidden" name="status" value="1">
                    <button class="btn btn-xs btn-primary" type="submit" style="float: left; margin-right: 5px">@if(\Auth::user()->language == 'VN') Chấp nhận @else Agree @endif</button>
                </form>
                <form method="POST" action="/relationship/declined">
                    {{ csrf_field() }}
                    <input type="hidden" name="user_relationship_id" value="{{$user->userRelationship()->id}}">
                    <input type="hidden" name="status" value="2">
                    <button class="btn btn-xs" type="submit">@if(\Auth::user()->language == 'VN') Từ chối @else Refuse @endif</button>
                </form>
                @endif
                @if($user->userRelationship()->status == 0 AND $user->userRelationship()->action_user_id == \Auth::user()->id)
                <p class="text-muted" style="float: left; margin-right: 20px">@if(\Auth::user()->language == 'VN') Bạn đã gửi yêu cầu kết bạn đến @else You have sent a friend request to @endif <a href="/user/{{$user->id}}">{{$user->name}}</a></p>
                <form method="POST" action="/relationship/delete">
                    {{ csrf_field() }}
                    <input type="hidden" name="user_relationship_id" value="{{$user->userRelationship()->id}}">
                    (<button class="btn btn-link" type="submit" style="padding:0">@if(\Auth::user()->language == 'VN') Hủy @else Cancel @endif</button>)
                </form>
                @endif
            </div>
            @elseif($user->id == \Auth::user()->id)

            @else
            <form method="POST" action="/relationship/create">
                {{ csrf_field() }}
                <input type="hidden" name="user_one_id" value="{{\Auth::user()->id}}">
                <input type="hidden" name="user_two_id" value="{{$user->id}}">
                <input type="hidden" name="status" value="0">
                <button class="btn btn-xs btn-theme" type="submit"><i class="fa fa-plus"></i>@if(\Auth::user()->language == 'VN') Kết bạn @else Make friend @endif</button>
                <button type="button" class="btn btn-xs btn-outline" data-toggle="modal" data-target="#myModal">Gợi ý</button>
            </form>
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Tình bạn của {{Auth::user()->getHoroscope()}} và {{$user->getHoroscope()}}</h4>
                        </div>
                        <div class="modal-body">
                            {{$user->getCouple()}}
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">@if(\Auth::user()->language == 'VN') Đóng @else Close @endif</button>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <hr>
            <p class="text-muted">@if(\Auth::user()->language == 'VN') Tôn giáo @else Religion @endif: @if($user->religion) {{$user->religion}} @else @if(\Auth::user()->language == 'VN') không @else Do not @endif @endif</p>
            <p class="text-muted">@if(\Auth::user()->language == 'VN') Giới thiệu bản thân @else Introduce yourself @endif: {{$user->description}}</p>
            <p class="text-muted">@if(\Auth::user()->language == 'VN') Sở thích @else Hobby @endif: {{$user->hobby}}</p>
            <p class="text-muted">@if(\Auth::user()->language == 'VN') Giới tính @else Sex @endif: @if($user->sex == 'male') Nam @else Nữ @endif</p>
            <p class="text-muted">@if(\Auth::user()->language == 'VN') Tuổi @else Age @endif: {{$user->getAge()}}</p>
            <p class="text-muted">@if(\Auth::user()->language == 'VN') Cung hoàng đạo @else Horoscope @endif: <span class="label label-primary">{{$user->getHoroscope()}}</span></p>
            <p class="text-muted">@if(\Auth::user()->language == 'VN') Ngày sinh @else Birthday @endif: {{date('d-M-Y',strtotime($user->birthday))}} - {{$canChi}} - Mệnh {{$cungMenh}}</p>
        </div>
    </div>
</div>
@if(count($images) > 0)
<label class="text-muted">@if(\Auth::user()->language == 'VN') Ảnh @else Images @endif</label>
<div class="col-md-12 box">
    @foreach($images as $image)
    <a class="example-image-link" data-lightbox="example-set" href="{{$image->image}}" data-title=""><img src="{{$image->image}}" style="max-height: 150px;float: left" class="img-responsive example-image m-r-5 m-b-5 img-hover-shadow" alt=""></a>
    @endforeach
</div>
@endif
<link href="/assets/plugin/lightbox2-master/css/lightbox.min.css" rel="stylesheet" type="text/css">
<script src="/assets/plugin/lightbox2-master/js/lightbox-plus-jquery.min.js"></script>
@endsection

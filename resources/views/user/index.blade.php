@extends('layouts.index')

@section('title') {{$user->name}} @endsection

@section('content')
<div class="col-md-12 box">
    <div class="row">
        @include('user.__titleUser')
        <div class="col-md-12">
            <form action="/user" method="POST" class="form-horizontal" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label class="col-md-3">@if(\Auth::user()->language == 'VN') Ảnh đại diện @else Avatar @endif</label>
                    <div class="col-md-9">
                        <img src="{{$user->getAvatar()}}" height="128" width="128">
                        <p class="text-muted" style="margin-top: 5px">@if(\Auth::user()->language == 'VN') tỉ lệ (1:1) @else ratio (1:1) @endif</p>
                        <input type="file" name="avatar" style="margin-top: 10px">
                        @if ($errors->has('avatar'))
                            <label class="text-danger">{{ $errors->first('avatar') }}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3">Username</label>
                    <div class="col-md-9">
                        <input type="text" name="user_name" value="{{$user->user_name}}" class="input-default">
                        @if ($errors->has('user_name'))
                            <label class="text-danger">{{ $errors->first('user_name') }}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3">@if(\Auth::user()->language == 'VN') Tên @else Name @endif</label>
                    <div class="col-md-9">
                        <input type="text" name="name" value="{{$user->name}}" class="input-default" required>
                        @if ($errors->has('name'))
                            <label class="text-danger">{{ $errors->first('name') }}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3">@if(\Auth::user()->language == 'VN') Họ và tên @else Full name @endif</label>
                    <div class="col-md-9">
                        <input type="text" name="full_name" value="{{$user->full_name}}" class="input-default">
                        @if ($errors->has('full_name'))
                            <label class="text-danger">{{ $errors->first('full_name') }}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3">Email</label>
                    <div class="col-md-9">
                        <input type="email" name="email" value="{{$user->email}}" readonly class="input-default">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3">@if(\Auth::user()->language == 'VN') Giới thiệu bản thân @else Introduce yourself @endif</label>
                    <div class="col-md-9">
                        <textarea name="description" class="textarea-default">{{$user->description}}</textarea>
                        @if ($errors->has('description'))
                            <label class="text-danger">{{ $errors->first('description') }}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3">@if(\Auth::user()->language == 'VN') Sở thích @else Hobby @endif</label>
                    <div class="col-md-9">
                        <textarea name="hobby" class="textarea-default">{{$user->hobby}}</textarea>
                        @if ($errors->has('hobby'))
                            <label class="text-danger">{{ $errors->first('hobby') }}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3">@if(\Auth::user()->language == 'VN') Giới tính @else Sex @endif</label>
                    <div class="col-md-9">
                        @if($user->sex == 'male')
                        <input type="radio" checked required name="sex" value="male"> @if(\Auth::user()->language == 'VN') Nam @else Male @endif
                        <input type="radio" required name="sex" value="female"> @if(\Auth::user()->language == 'VN') Nữ @else Female @endif
                        @else
                        <input type="radio" required name="sex" value="male"> @if(\Auth::user()->language == 'VN') Nam @else Male @endif
                        <input type="radio" checked required name="sex" value="female"> @if(\Auth::user()->language == 'VN') Nữ @else Female @endif
                        @endif
                        @if ($errors->has('sex'))
                            <label class="text-danger">{{ $errors->first('sex') }}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3">@if(\Auth::user()->language == 'VN') Ngày sinh @else Birthday @endif</label>
                    <div class="col-md-9">
                        <input type="text" name="birthday" id="datepicker" value="{{date('Y-m-d', strtotime($user->birthday))}}" class="input-default">
                        @if ($errors->has('birthday'))
                            <label class="text-danger">{{ $errors->first('birthday') }}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3">@if(\Auth::user()->language == 'VN') Địa chỉ @else Address @endif</label>
                    <div class="col-md-9">
                        <input type="text" name="address" value="{{$user->address}}" class="input-default">
                        @if ($errors->has('address'))
                            <label class="text-danger">{{ $errors->first('address') }}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3">@if(\Auth::user()->language == 'VN') Tôn giáo @else Religion @endif</label>
                    <div class="col-md-9">
                        <input type="text" name="religion" value="{{$user->religion}}" class="input-default">
                        @if ($errors->has('religion'))
                            <label class="text-danger">{{ $errors->first('religion') }}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3">@if(\Auth::user()->language == 'VN') Điện thoại @else Phone @endif</label>
                    <div class="col-md-9">
                        <input type="text" name="phone" value="{{$user->phone}}" class="input-default" style="max-width: 200px">
                        @if ($errors->has('phone'))
                            <label class="text-danger">{{ $errors->first('phone') }}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3">@if(\Auth::user()->language == 'VN') Ngôn ngữ @else Language @endif</label>
                    <div class="col-md-9">
                        <select name="language" class="select-default" required style="max-width: 200px">
                            @if($user->language == 'VN')
                            <option value="VN" selected>Tiếng việt</option>
                            <option value="EN">English</option>
                            @else
                            <option value="VN">Vietnamese</option>
                            <option value="EN" selected>English</option>
                            @endif
                        </select>
                        @if ($errors->has('language'))
                            <label class="text-danger">{{ $errors->first('language') }}</label>
                        @endif
                    </div>
                </div>
                <hr/>
                <div class="form-group">
                    <div class="col-md-3"></div>
                    <div class="col-md-9">
                        <button type="submit" class="btn btn-theme">@if(\Auth::user()->language == 'VN') Lưu thay đổi @else Save changes @endif</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $( function() {
        $( "#datepicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '1950:2016',
            dateFormat: "yy-mm-dd",
            @if(\Auth::user()->language == 'VN')
            appendText: "(năm-tháng-ngày)"
            @else
            appendText: "(year-month-day)"
            @endif
        });
    });
</script>
@endsection

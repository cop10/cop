<div class="col-md-2"></div>
<div class="col-md-6 post content-blog">
    @yield('content')
</div>
<div class="col-md-4 list-right-blog">
    <div class="col-md-12">
        <legend class="text-muted">@if(\Auth::user()->language == 'VN') Blog cung hoàng đạo @else Zodiac blog @endif</legend>
        @if(\Auth::user()->role_id != 3)
        <label><a href="/blog/post/create" class="text-success"><i class="fa fa-plus"></i> @if(\Auth::user()->language == 'VN') Viết bài @else Blogging @endif</a></label>
        @endif
        <ul class="list-unstyled">
            @foreach(\App\Models\Post::getPosts() as $post)
            <li><label><a href="/blog/post/{{$post->id}}"><i class="fa fa-caret-right"></i> {{$post->title}}</a></label></li>
            @endforeach
        </ul>
    </div>
</div>
<style type="text/css">
    .content-blog{
        background-color: #fff;
        margin: 20px 0;
        border-radius: 3px;
        padding: 10px 15px;
    }
    .list-right-blog{
        margin-top: 20px;
    }
</style>
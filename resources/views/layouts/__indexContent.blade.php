<div class="col-md-2"></div>
<div class="col-md-6 content-center">
    @yield('content')
</div>
<div class="col-md-2 content-info">
    <div class="col-md-12 box">
        <label class="text-muted"><a href="/blog/post">@if(\Auth::user()->language == 'VN') Cung hoàng đạo @else Zodiac Blog @endif</a></label>
        <ul class="list-unstyled">
            @foreach(\App\Models\Post::getLastFivePosts() as $post)
            <li>
                <a href="/blog/post/{{$post->id}}"><img src="{{$post->image}}" class="img-responsive"></a>
                <p><a href="/blog/post/{{$post->id}}">{{$post->title}}</a></p>
            </li>
            @endforeach
        </ul>
        <hr style="margin: 0 0 7px 0">
        <p class="text-center"><a href="/blog/post" class="text-muted">@if(\Auth::user()->language == 'VN') Xem tất cả @else View all @endif</a></p>
    </div>
</div>
<div class="col-md-2 sidebar-right">
    <div class="col-md-12 box">
        @if(\Request::is( '/'))
        <label class="text-muted"><a href="/message">@if(\Auth::user()->language == 'VN') Tin Nhắn @else Recent messages @endif</a></label>
        <ul class="list-unstyled list-last-messages">
            @foreach(\App\Models\User::lastMessages() as $message)
            @if($message->receiver_id)
            <a href="/message/{{$message->getUser->id}}">
                <li  style="@if($message->receiver_id == \Auth::user()->id AND $message->status == 1) background-color: #f2f2f2 @endif">
                    <img src="{{$message->getUser->getAvatar()}}" height="25px" width="25"> {{str_limit($message->getUser->name, $limit = 15, $end = '...')}}
                    <p class="text-muted" style="padding: 2px">{{str_limit($message->content, $limit = 20, $end = '...')}}
                    @if($message->receiver_id == \Auth::user()->id AND $message->status == 1)
                    <span class="fa fa-circle new-message"></span>
                    @endif
                    <br><small>{{\App\Models\Status::getTimeCreated($message->created_at)}}</small></p>
                </li>
            </a>
            @else
            <a href="/group/{{$message->group_id}}" data-toggle="tooltip" title="{{$message->group->title}}">
                <li style="@if($message->sender_id != \Auth::user()->id AND $message->status == 1) background-color: #f2f2f2 @endif">
                    <img src="/assets/images/group-avatar.png" class="" height="25px" width="25"> {{str_limit($message->group->title, $limit = 15, $end = '...')}} <i class="fa fa-users text-muted" style="font-size: 12px"></i>
                    <p class="text-muted" style="padding: 2px">{{str_limit($message->content, $limit = 20, $end = '...')}}
                    @if($message->sender_id != \Auth::user()->id AND $message->status == 1)
                    <span class="fa fa-circle new-message"></span>
                    @endif
                    <br><small>{{\App\Models\Status::getTimeCreated($message->created_at)}}</small></p>
                </li>
            </a>
            @endif
            @endforeach
        </ul>
        @if(count(\App\Models\User::lastMessages()) == 0)
        <p class="text-muted text-center">@if(\Auth::user()->language == 'VN') Không có tin nhắn @else Don't have messages @endif</p>
        @endif
        @else
        <label class="text-muted"><a href="/">@if(\Auth::user()->language == 'VN') Bảng tin @else News @endif</a></label>
        <ul class="list-unstyled">
            @foreach(\App\Models\Status::getFriendStatus() as $stt)
            <li>
                <img src="{{$stt->user->getAvatar()}}" class="img-circle" height="30px" width="30"><a href="/user/{{$stt->user->id}}"> {{$stt->user->name}}</a>
                <br><small class="text-muted">{{\App\Models\Status::getTimeCreated($stt->created_at)}}</small><br>
                @if($stt->content)
                <p class="text-muted">{{str_limit($stt->content, $limit = 100, $end = '...')}}</p>
                @endif
                @if(count($stt->statusAttachment) > 0)
                    <p class="label label-default">{{count($stt->statusAttachment)}} @if(\Auth::user()->language == 'VN') tệp @else attachment @endif</p>
                @endif
                @if(count($stt->statusImages) == 1)
                    <p class="label label-default">{{count($stt->statusImages)}} @if(\Auth::user()->language == 'VN') ảnh @else image @endif</p>
                @endif
                @if(count($stt->statusImages) > 1)
                    <p class="label label-default">{{count($stt->statusImages)}} @if(\Auth::user()->language == 'VN') ảnh @else images @endif</p>
                @endif
                <hr style="margin:5px 0">
            </li>
            @endforeach
            @if(count(\App\Models\Status::getFriendStatus()) == 0)
            <br>
            <p class="text-center text-muted">@if(\Auth::user()->language == 'VN') Không có bài đăng @else Empty @endif</p>
            @else
            <p class="text-center"><a href="/" class="text-muted">@if(\Auth::user()->language == 'VN') Xem tất cả @else View all @endif</a></p>
            @endif
        </ul>
        @endif
    </div>
</div>
<style type="text/css">
    .list-last-messages>a>li{
        background-color: #f9f9f9;
        border: 1px solid #f1f1f1;
        height: 70px;
        margin-bottom: 10px;
        position: relative;
    }
    .tooltip-inner {
        white-space:pre-wrap;
    }
    .new-message{
        color: orange;
        font-size: 10px;
    }
</style>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="assets/images/logo.png">
    <title>HoroChat</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/auth.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
</head>
<body>
    <nav class="navbar">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/login"><img src="assets/images/logo.png"> Horo<span>Chat</span></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    @if($_GET['language'] == 'VN')
                    <li><a href="/introduce">Giới thiệu</a></li>
                    @else
                    <li><a href="/introduce">Introduce</a></li>
                    @endif
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">@if($_GET['language'] == 'VN') Ngôn ngữ: Tiếng việt @else Language: English @endif<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="?language=VN">@if($_GET['language'] == 'VN') Tiếng Việt @else vietnamese @endif @if($_GET['language'] == 'VN') <i class="fa fa-check"></i> @endif</a></li>
                            <li><a href="?language=EN">English @if($_GET['language'] == 'EN') <i class="fa fa-check"></i> @endif</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <div class="container content">
        <div class="row">
            <div class="col-md-9 hidden-xs hidden-sm">
                <div class="row">
                    <div class="col-md-3">
                        <img src="assets/images/star-1.jpg" class="img-responsive img-rounded">
                        @if($_GET['language'] == 'VN')
                        <label>Bảo Bình</label>
                        @else
                        <label>Aquarius</label>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <img src="assets/images/star-2.jpg" class="img-responsive img-rounded">
                        @if($_GET['language'] == 'VN')
                        <label>Ma Kết</label>
                        @else
                        <label>Capricorn</label>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <img src="assets/images/star-3.jpg" class="img-responsive img-rounded">
                        @if($_GET['language'] == 'VN')
                        <label>Nhân Mã</label>
                        @else
                        <label>Sagittarius</label>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <img src="assets/images/star-4.jpg" class="img-responsive img-rounded">
                        @if($_GET['language'] == 'VN')
                        <label>Hổ Cáp</label>
                        @else
                        <label>Scorpius</label>
                        @endif
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-3">
                        <img src="assets/images/star-5.jpg" class="img-responsive img-rounded">
                        @if($_GET['language'] == 'VN')
                        <label>Thiên Bình</label>
                        @else
                        <label>Libra</label>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <img src="assets/images/star-6.jpg" class="img-responsive img-rounded">
                        @if($_GET['language'] == 'VN')
                        <label>Xử Nữ</label>
                        @else
                        <label>Virgo</label>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <img src="assets/images/star-7.jpg" class="img-responsive img-rounded">
                        @if($_GET['language'] == 'VN')
                        <label>Sư Tử</label>
                        @else
                        <label>Leo</label>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <img src="assets/images/star-8.jpg" class="img-responsive img-rounded">
                        @if($_GET['language'] == 'VN')
                        <label>Cự Giải</label>
                        @else
                        <label>Cancer</label>
                        @endif
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-3">
                        <img src="assets/images/star-9.jpg" class="img-responsive img-rounded">
                        @if($_GET['language'] == 'VN')
                        <label>Song Tử</label>
                        @else
                        <label>Gemini</label>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <img src="assets/images/star-10.jpg" class="img-responsive img-rounded">
                        @if($_GET['language'] == 'VN')
                        <label>Kim Ngưu</label>
                        @else
                        <label>Taurus</label>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <img src="assets/images/star-11.jpg" class="img-responsive img-rounded">
                        @if($_GET['language'] == 'VN')
                        <label>Bạch Dương</label>
                        @else
                        <label>Aries</label>
                        @endif
                    </div>
                    <div class="col-md-3">
                        <img src="assets/images/star-12.jpg" class="img-responsive img-rounded">
                        @if($_GET['language'] == 'VN')
                        <label>Song Ngư</label>
                        @else
                        <label>Pisces</label>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="box-form">
                    @yield('content')
                </div>
            </div>
        </div>
        <div class="col-md-6 col-md-offset-3">
            <ul class="list-inline footer">
                @if($_GET['language'] == 'VN')
                <li><a href="/introduce">Giới thiệu</a></li>
                <li><a href="#">Trợ giúp</a></li>
                <li><a href="#">Liên hệ</a></li>
                <li><a href="#">Điều khoản</a></li>
                <li><a href="/relationship/index">Tìm bạn bè</a></li>
                <li><a href="/blog">Blog</a></li>
                <li><a href="#">© 2016 HoroChat</a></li>
                @else
                <li><a href="/introduce">Introduce</a></li>
                <li><a href="#">Help</a></li>
                <li><a href="#">Contact</a></li>
                <li><a href="#">Terms</a></li>
                <li><a href="/relationship/index">Find friend</a></li>
                <li><a href="blog">Blog</a></li>
                <li><a href="#">© 2016 HoroChat</a></li>
                @endif
            </ul>
        </div>
    </div>
</body>
</html>
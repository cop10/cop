<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/assets/images/logo.png">
    <title>@yield('title')</title>
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/assets/css/style-index.css" rel="stylesheet">
    <link href="/assets/plugin/chosen/chosen.css" rel="stylesheet" type="text/css">
    <link href="/assets/plugin/chosen/chosenImage.css" rel="stylesheet" type="text/css">
    <link href="/assets/plugin/toastr/toastr.min.css" rel="stylesheet" type="text/css">
    <!-- Script -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="/assets/js/jquery-2.1.1.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/plugin/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="/assets/plugin/chosen/chosenImage.jquery.js" type="text/javascript"></script>
    <script src="/assets/plugin/chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
    <script src="/assets/plugin/toastr/toastr.min.js"></script>
    <!-- <script src="//code.jquery.com/jquery-1.11.2.min.js"></script> -->
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="https://cdn.socket.io/socket.io-1.3.4.js"></script>
    <script type="text/javascript">
        var app_url = '{{ env('APP_URL') }}:8080';
        var socket = io.connect(app_url);
    </script>
</head>
<body>
    <nav class="navbar navbar-theme navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="fa fa-lg fa-bars"></span>
                </button>
                <a class="navbar-brand" href="/"><img src="/assets/images/logo-o.png" height="20" width="20"> Horo<span>Chat</span></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <form action="javascript:void(0);" class="hidden-xs hidden-sm navbar-form navbar-left">
                    <div class="form-group group-search-top">
                        <input type="text" name="keyword" autocomplete="off" class="form-control input-search" placeholder="@if(\Auth::user()->language == 'VN') Tìm kiếm @else Search @endif" style="width: 400px">
                        <a href="javascript:;" class="remove-keyword hidden"><i class="fa fa-times text-muted"></i></a>
                        <script type="text/javascript">
                            $('.input-search').keyup(function(){
                                if( !$(this).val() ) {
                                    $('.list-users-keyup').addClass('hidden');
                                    $('.remove-keyword').addClass('hidden');
                                }else{
                                    $('.list-users-keyup').removeClass('hidden');
                                    $('.remove-keyword').removeClass('hidden');
                                }
                                var keyword = $(this).val();
                                var upperKeyword = keyword.toUpperCase();
                                var users = $(".list-users-keyup").find("a");
                                for (var i = 0; i < users.length; i++) {
                                    var message = $(users[i]).text();
                                    var upperMessage = message.toUpperCase();
                                    if (keyword && keyword != ' ') {
                                        if (upperMessage.indexOf(upperKeyword) == -1) {
                                            $(users[i]).addClass('hidden');
                                        }else{
                                            $(users[i]).removeClass('hidden');
                                        }
                                    }else{
                                        $(users[i]).removeClass('hidden');
                                    }
                                }
                            });
                            $('.remove-keyword').click(function (){
                                $('.input-search').val('');
                                $('.list-users-keyup').addClass('hidden');
                                $(this).addClass('hidden');
                            })
                        </script>
                    </div>
                </form>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="glyphicon glyphicon-user" style="margin-left: 8px"></i><i class="glyphicon glyphicon-user" style="color: #45275d;margin-left: -18px"></i><i class="glyphicon glyphicon-user" style="margin-left: -17px;font-size: 13px"></i> <span class="m-l-10"> @if(\Auth::user()->language == 'VN') Bạn bè @else Friend @endif </span> <span class="badge badge-warning"></span></a>
                        <ul class="dropdown-menu list-dropdown">
                            @if(count(\App\Models\UserRelationship::pendingRelationship()) != 0)
                            <li>
                                <a href="/relationship/index">@if(\Auth::user()->language == 'VN') Lời mời kết bạn @else Friend request @endif
                                <span class="badge badge-warning">{{count(\App\Models\UserRelationship::pendingRelationship())}}</span>
                                </a>
                            </li>
                            @endif
                            <li><a href="/relationship/index">@if(\Auth::user()->language == 'VN') Gợi ý kết bạn @else Friend suggestions @endif</a></li>
                            <li><a href="/relationship/declined/view">@if(\Auth::user()->language == 'VN') Lời mời đã từ chối @else Invitation refused @endif</a></li>
                            <li><a href="/relationship/blocked/view">@if(\Auth::user()->language == 'VN') Chặn bạn bè @else Block Friend @endif</a></li>
                            <li><a href="/user/friend">@if(\Auth::user()->language == 'VN') Bạn bè @else Friends @endif</a></li>
                            <li><a href="/blog/post">Horo Blog</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-envelope" style="margin-left: 6px;font-size: 17px"></i><i class="fa fa-square" style="color: #45275d;margin-left: -18px;font-size: 14px"></i><i class="fa fa-envelope" style="margin-left: -16px;font-size: 13px"></i> <span class="m-l-10"> @if(\Auth::user()->language == 'VN') Tin nhắn @else Message @endif </span> <span class="badge badge-warning" id="icon-new-message"></span></a>
                        <ul class="dropdown-menu list-dropdown" id="list-new-message">
                            <li role="separator" class="divider hidden" id="divider-message"></li>
                            <li><a href="/message/new">@if(\Auth::user()->language == 'VN') Tin nhắn mới @else A new message @endif</a></li>
                            <li><a href="/message">@if(\Auth::user()->language == 'VN') Tất cả tin nhắn @else All messages @endif</a></li>
                            <li><a href="/group">@if(\Auth::user()->language == 'VN') Nhóm @else Groups @endif</a></li>
                            <li><a href="/group/create">@if(\Auth::user()->language == 'VN') Tạo nhóm @else Create a group @endif</a></li>
                        </ul>
                        <script type="text/javascript">
                            var title = document.title;
                            var countNewMsg = 0;
                            var auth_id = {{ \Auth::user()->id }}
                            var groups_id = {{ \App\Models\Group::getGroupsId() }}
                            socket.on('message', function (data) {
                                data = jQuery.parseJSON(data);
                                console.log(data);
                                if (data.receiver_id == auth_id) {
                                    $('#divider-message').removeClass('hidden');
                                    $('#list-new-message').prepend('<li><a href="/message/'+data.sender_id+'"><img src="'+data.avatar+'" class="img-circle" height="23" width="23"> '+data.sender_name+'<br><small>'+data.content.substr(0,20)+'...</small></a></li>');
                                    countNewMsg = countNewMsg + 1;
                                    document.title = '('+countNewMsg+') '+title;
                                    $('#icon-new-message').text(countNewMsg);
                                }
                                for (var i = 0; i < groups_id.length; i++) {
                                    if (data.group_id == groups_id[i] && data.user_id != auth_id) {
                                        $('#divider-message').removeClass('hidden');
                                        $('#list-new-message').prepend('<li><a href="/group/'+data.group_id+'"><img src="/assets/images/group-avatar.png" class="img-circle" height="23" width="23"> '+data.group_title+' <br><small>'+data.content.substr(0,20)+'...</small></a></li>');
                                        countNewMsg = countNewMsg + 1;
                                        document.title = '('+countNewMsg+') '+title;
                                        $('#icon-new-message').text(countNewMsg);
                                    }
                                }
                            });
                        </script>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" title="@if(\Auth::user()->language == 'VN') Tài khoản @else Account @endif" style="padding: 10px"><img src="{{Auth::user()->getAvatar()}}" height="30" width="30" class="img-rounded img-acc"></a>
                        <ul class="dropdown-menu list-dropdown">
                            <li><a href="/user/{{\Auth::user()->id}}">{{str_limit(\Auth::user()->name, $limit = 30, $end = '...')}} @if(\Auth::user()->role_id == 1) <label class="label label-success">Admin</label> @elseif(\Auth::user()->role_id == 2) <label class="label label-info">Moderator</label> @endif </a></li>
                            <li><a href="/user/{{\Auth::user()->id}}/edit">@if(\Auth::user()->language == 'VN') Bảo mật @else Security @endif</a></li>
                            @if(\Auth::user()->role_id != 3)
                            <li><a href="/blog/post/create">@if(\Auth::user()->language == 'VN') Viết blog @else Blogging @endif</a></li>
                            @endif
                            <li><a href="/diary">@if(\Auth::user()->language == 'VN') Nhật ký @else Diary @endif</a></li>
                            <li><a href="/logout">@if(\Auth::user()->language == 'VN') Đăng xuất @else Logout @endif <i class="fa fa-sign-out"></i></a></li>
                            <li role="separator" class="divider"></li>
                            @if(\Auth::user()->role_id == 1)
                            <li><a href="/user/list">@if(\Auth::user()->language == 'VN') Người dùng @else Users @endif</a></li>
                            @endif
                            <li><a href="/">@if(\Auth::user()->language == 'VN') Trợ giúp @else Help @endif <i class="fa fa-info-circle "></i></a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <br/>
    <div class="page-content">
        <div class="sidebar-left hidden-xs hidden-sm">
            <div class="sidebar-info">
                <a href="/relationship/index" title="@if(\Auth::user()->language == 'VN') Kết bạn qua cung hoàng đạo @else Making friends through the zodiac @endif"><i class="fa fa-user-plus"></i>@if(\Auth::user()->language == 'VN') Gợi ý kết bạn @else Friend suggestions @endif</a>
                <hr style="margin: 0px">
            </div>
            <div class="sidebar-box scroll-style">
                <br/>
                <label><a href="/user/friend">@if(\Auth::user()->language == 'VN') Bạn bè @else Friends @endif</a></label>
                <br/>
                <ul class="list-friend list-unstyled">
                    @foreach(\App\Models\UserRelationship::getFriend() as $friend)
                    <li><a href="/message/{{$friend->user()->id}}"><img src="{{$friend->user()->getAvatar()}}" class="img-circle" height="35" width="35"> {{str_limit($friend->user()->name, $limit = 20, $end = '...')}}
                    @if($friend->user()->countNewMesage())
                        <span class="badge badge-warning">{{$friend->user()->countNewMesage()}}</span>
                    @endif
                    @if($friend->user()->isOnline())
                        <i class="fa fa-check"></i>
                    @endif
                    </a></li>
                    @endforeach
                </ul>
                <label><a href="/group">@if(\Auth::user()->language == 'VN') Nhóm @else Groups @endif</a></label>
                <br/>
                <ul class="list-friend list-unstyled">
                    @foreach(\App\Models\Group::getGroups() as $group)
                    <li><a href="/group/{{$group->id}}"><img src="/assets/images/group-avatar.png" class="img-circle" height="35" width="35"> {{str_limit($group->title, $limit = 20, $end = '...')}}
                    @if($group->countNewMesage())
                        <span class="badge badge-warning">{{$group->countNewMesage()}}</span>
                    @endif
                    </a></li>
                    @endforeach
                </ul>
                <p style="padding-right: 7px"><a href="/group/create" class="btn btn-block btn-sm btn-success">@if(\Auth::user()->language == 'VN') Tạo nhóm @else Create group @endif</a></p>
            </div>
            <hr style="margin:0px 10px 0px 0px">
            <div class="sidebar-search">
                <input type="text" id="searchUserMessage" placeholder="@if(\Auth::user()->language == 'VN') Tìm kiếm @else Search @endif" class="form-control">
                <script type="text/javascript">
                    $('#searchUserMessage').keyup(function(){
                        var keyword = $(this).val();
                        var upperKeyword = keyword.toUpperCase();
                        var users = $(".list-friend").find("a");
                        for (var i = 0; i < users.length; i++) {
                            var message = $(users[i]).text();
                            var upperMessage = message.toUpperCase();
                            if (keyword && keyword != ' ') {
                                if (upperMessage.indexOf(upperKeyword) == -1) {
                                    $(users[i]).addClass('hidden');
                                }else{
                                    $(users[i]).removeClass('hidden');
                                }
                            }else{
                                $(users[i]).removeClass('hidden');
                            }
                        }
                    });
                </script>
            </div>
        </div>
        @if(\Request::is( 'blog/*'))
        @include('layouts.__indexBlog')
        @else
        @include('layouts.__indexContent')
        @endif
    </div>
    <div class="list-users-keyup hidden scroll-style">
        <ul class="list-unstyled">
        @foreach(\App\Models\User::getAllUserNotMe() as $user)
            <li><a href="/user/{{$user->id}}"><img src="{{$user->getAvatar()}}" class="img-circle" height="40" width="40"> {{$user->name}}
            @if($user->userRelationship() AND $user->role_id != 1)
                @if($user->userRelationship()->status == 1 || $user->userRelationship()->status == 3)
                <small class="pull-right"><i class="fa fa-check"></i> @if(\Auth::user()->language == 'VN') Bạn bè @else Friend @endif</small>
                @endif
            @endif
            </a></li>
        @endforeach
        </ul>
    </div>
    @if(Session::has('success'))
    <script type="text/javascript">
        $(function () {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-bottom-right",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 7000,
                "extendedTimeOut": 1000,
                "showDuration": 400,
                "hideDuration": 1000
            }
            toastr.success("{{Session::get('success')}}");
        });
    </script>
    @endif
    @if(Session::has('warning'))
    <script type="text/javascript">
        $(function () {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-bottom-right",
                "onclick": null,
                "fadeIn": 300,
                "fadeOut": 1000,
                "timeOut": 7000,
                "extendedTimeOut": 1000,
                "showDuration": 400,
                "hideDuration": 1000
            }
            toastr.warning("{{Session::get('warning')}}");
        });
    </script>
    @endif
</body>
</html>
@extends('layouts.index')

@section('title') {{$post->title}} @endsection

@section('content')
    <h2 style="color: #640fa9">{{$post->title}}<br></h2>
    <small class="text-muted">{{$post->user->name}} | {{date('d-m-Y h:i A', strtotime($post->created_at))}} @if(\Auth::user()->role_id != 3) | <a href="/blog/post/{{$post->id}}/edit">@if(\Auth::user()->language == 'VN') Sửa @else Edit @endif</a> @endif</small>
    <img src="{{$post->image}}" class="img-responsive img-post-info">
    <p>{!! $post->content !!}</p>
    <form action="/blog/post/like" id="postLike" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="post_id" value="{{$post->id}}">
        <div class="button-group">
        <a href="javascript:;" class="btn focus-reply btn-link" style="padding-left: 0"><i class="fa fa-reply"></i> @if(\Auth::user()->language == 'VN') Trả lời @else Reply @endif</a>
        @if($post->checkLike())
            <button class="btn btn-link" style="padding: 0"><i class="fa fa-heart"></i> @if(\Auth::user()->language == 'VN') Thích @else Like @endif @if(count($post->likes) != 0) {{count($post->likes)}} @endif</button>
        @else
            <button class="btn btn-link" style="padding: 0"><i class="fa fa-heart-o"></i> @if(\Auth::user()->language == 'VN') Thích @else Like @endif @if(count($post->likes) != 0) {{count($post->likes)}} @endif</button>
        @endif
        </div>
    </form>
    <script type="text/javascript">
        $(function () {
            $('#postLike').on('submit', function (e) {
                $.ajax({
                    type: 'post',
                    url: $(this).attr('action'),
                    data: $(this).serialize(),
                    success: function (data) {
                        $('#postLike button').remove();
                        $('#postLike .button-group').append(data.button);
                    },
                    error: function(jqXHR, exception) {
                        if (jqXHR.status === 0) {
                            alert('Not connect.\n Verify Network.');
                        } else if (jqXHR.status == 404) {
                            alert('Requested page not found. [404]');
                        } else if (jqXHR.status == 500) {
                            alert('Internal Server Error [500].');
                        } else if (exception === 'parsererror') {
                            alert('Requested JSON parse failed.');
                        } else if (exception === 'timeout') {
                            alert('Time out error.');
                        } else if (exception === 'abort') {
                            alert('Ajax request aborted.');
                        } else {
                            alert('Uncaught Error.\n' + jqXHR.responseText);
                        }
                    }
                });
                e.preventDefault();
            });
        });
    </script>
    <hr style="margin-top: 0">
    <div id="commentsArea">
        @foreach($post->comments as $comment)
        <div class="row">
            <div class="col-md-1 col-sm-1 col-xs-2">
                <img src="{{$comment->user->getAvatar()}}" class="img-circle img-avatar-comment">
            </div>
            <div class="col-md-11 col-sm-11 col-xs-10">
                <a href="/user/{{$comment->user->id}}">{{$comment->user->name}}</a>
                <p><small class="text-muted">{{date('d-m-Y h:i A', strtotime($comment->created_at))}}</small><br>{{$comment->content}}</p>
                <form action="/blog/comment/{{$comment->id}}" id="commentEdit{{$comment->id}}" method="POST" class="collapse">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <textarea name="content" class="textarea-default" spellcheck="false" required>{{$comment->content}}</textarea>
                    <button type="submit" class="btn btn-sm btn-success pull-right">@if(\Auth::user()->language == 'VN') Lưu @else Save @endif</button>
                    <a role="button" href="#commentEdit{{$comment->id}}" data-toggle="collapse" aria-expanded="false" aria-controls="commentEdit{{$comment->id}}" class="btn btn-sm m-r-5 m-b-10 btn-link pull-right">@if(\Auth::user()->language == 'VN') Hủy @else Cancel @endif</a>
                    <br>
                </form>
                <div class="btn-group" style="position: absolute;top:-5px;right:10px">
                    <button type="button" class="btn btn-sm btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        @if($comment->created_by == \Auth::user()->id)
                        <li><a role="button" href="#commentEdit{{$comment->id}}" data-toggle="collapse" aria-expanded="false" aria-controls="commentEdit{{$comment->id}}">@if(\Auth::user()->language == 'VN') Chỉnh sửa @else Edit @endif</a></li>
                        <li>
                            <form action="/blog/comment/{{$comment->id}}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                @if(\Auth::user()->language == 'VN')
                                <button class="btn-li-dropdown" onclick="return confirm('Bạn chắc chắn muốn xóa bình luận này?')">Xóa bình luận</button>
                                @else
                                <button class="btn-li-dropdown" onclick="return confirm('You sure you want to delete this comment?')">Delete comment</button>
                                @endif
                            </form>
                        </li>
                        @else
                        <li><a href="#">@if(\Auth::user()->language == 'VN') Báo cáo spam hoặc lạm dụng @else Report spam or abuse @endif</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <br>
    <div class="row">
        <div class="col-md-1 col-sm-1 col-xs-2">
            <img src="{{\Auth::user()->getAvatar()}}" class="img-circle img-avatar-comment">
        </div>
        <div class="col-md-11 col-sm-11 col-xs-10">
            <form action="/blog/comment" id="createComment" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="post_id" value="{{$post->id}}">
                <input type="text" autocomplete="off" placeholder="@if(\Auth::user()->language == 'VN') Bình luận ... @else Comment ... @endif" name="content" required>
            </form>
        </div>
    </div>
    <br>
    <style type="text/css">
        p table{
            margin: 0 auto;
        }
        .img-post-info{
            margin: 10px auto;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $('#createComment').on('submit', function (e) {
                $.ajax({
                    type: 'post',
                    url: $(this).attr('action'),
                    data: $(this).serialize(),
                    success: function (data) {
                        $("#createComment input[name='content']").val('');
                        $("#commentsArea").load("/blog/post/{{$post->id}} #commentsArea>*", "");
                    },
                    error: function(jqXHR, exception) {
                        if (jqXHR.status === 0) {
                            alert('Not connect.\n Verify Network.');
                        } else if (jqXHR.status == 404) {
                            alert('Requested page not found. [404]');
                        } else if (jqXHR.status == 500) {
                            alert('Internal Server Error [500].');
                        } else if (exception === 'parsererror') {
                            alert('Requested JSON parse failed.');
                        } else if (exception === 'timeout') {
                            alert('Time out error.');
                        } else if (exception === 'abort') {
                            alert('Ajax request aborted.');
                        } else {
                            alert('Uncaught Error.\n' + jqXHR.responseText);
                        }
                    }
                });
                e.preventDefault();
            });
        });
        $('.focus-reply').click(function () {
            $("#createComment input[name='content']").focus();
        });
    </script>
@endsection
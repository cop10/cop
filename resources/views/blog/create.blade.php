@extends('layouts.index')

@section('title') Create post @endsection

@section('content')
    <label><a href="javascript:;">@if(\Auth::user()->language == 'VN') Viết bài @else Create post @endif</a></label>
    <a href="/blog/post" class="pull-right">Blog</a>
    <hr style="margin: 0">
    <br>
    <form action="/blog/post" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
            @if(\Auth::user()->language == 'VN')
            <label>Tên bài viết *<span class="text-muted"> (dưới 255 ký tự)</span></label>
            @else
            <label>Title of article *<span class="text-muted"> (dưới 255 ký tự)</span></label>
            @endif
            <input type="text" name="title" class="input-title-post" value="{{old('title')}}" required>
            @if ($errors->has('title'))
                <label class="text-danger">{{ $errors->first('title') }}</label>
            @endif
        </div>
        <div class="form-group">
            @if(\Auth::user()->language == 'VN')
            <label>Ảnh mô tả *<span class="text-muted"> (W:500px | H:380px) </span></label>
            @else
            <label>Photo description *<span class="text-muted"> (W:500px | H:380px) </span></label>
            @endif
            <input type="file" name="image" style="border: none;padding: 0" required>
            @if ($errors->has('image'))
                <label class="text-danger">{{ $errors->first('image') }}</label>
            @endif
        </div>
        <div class="form-group">
            <label>@if(\Auth::user()->language == 'VN') Nội dung * @else Content * @endif</label>
            <textarea name="content" class="ckeditor" required>{!! old('content') !!}</textarea>
            @if ($errors->has('content'))
                <label class="text-danger">{{ $errors->first('content') }}</label>
            @endif
        </div>
        <div class="form-group">
            <p class="text-muted">(*) @if(\Auth::user()->language == 'VN') bắt buộc @else required @endif</p>
            <button class="btn btn-theme">@if(\Auth::user()->language == 'VN') Đăng bài @else Publish @endif</button>
        </div>
    </form>
    <style type="text/css">
        .input-title-post{
            width: 100%;
            border: 1px solid #2b579a !important;
            box-shadow: 0 0 6px 1px rgba(172,181,194,0.56);
        }
        .cke_chrome {
            border: 1px solid #45275d;
        }
        .cke_bottom {
            background: #45275d;
        }
    </style>
    <script src="/assets/plugin/ckeditor/ckeditor.js"></script>
@endsection
@extends('layouts.index')

@section('title') Declined @endsection

@section('content')
<div class="col-md-12 box box-min">
    <div class="row">
        <div class="col-md-12">
            @include('userRelationship.__titleRelationship')
        </div>
        <div class="col-md-12">
            <hr style="margin:0">
            <br>
        </div>
        <div class="col-md-12">
            <label class="text-muted">@if(\Auth::user()->language == 'VN') Lời mời đã từ chối @else Invitation refused @endif</label>
            @if(count($userRelationship) == 0)
            <br>
            <br>
            <br>
            <p class="text-center text-muted">@if(\Auth::user()->language == 'VN') Trống @else Empty @endif</p>
            <br>
            @endif
        </div>
        @foreach($userRelationship as $userRe)
        <div class="col-md-12 item-full-row">
            <div class="item">
                <a href="/user/{{$userRe->user()->id}}"><img src="/assets/images/user.jpg" class="img-thumbnail"></a>
                <label><a href="/user/{{$userRe->user()->id}}">{{$userRe->user()->name}}</a></label><br/>
                <span class="text-muted">{{$userRe->user()->getAge()}} @if(\Auth::user()->language == 'VN') tuổi @else age @endif</span><br/>
                <span class="text-muted">@if($userRe->user()->sex == 'male')<i class="fa fa-mars"></i>@else<i class="fa fa-venus"></i>@endif {{$userRe->user()->getHoroscope()}}</span><br/>
                <form method="POST" action="/relationship/accepted">
                    {{ csrf_field() }}
                    <input type="hidden" name="user_relationship_id" value="{{$userRe->id}}">
                    <input type="hidden" name="status" value="1">
                    <button class="btn btn-xs btn-info" type="submit" style="float:left; margin-right: 5px">@if(\Auth::user()->language == 'VN') Đồng ý kết bạn @else Accept friendship @endif</button>
                </form>
                <form method="POST" action="/relationship/delete">
                    {{ csrf_field() }}
                    <input type="hidden" name="user_relationship_id" value="{{$userRe->id}}">
                    <button class="btn btn-xs" type="submit">@if(\Auth::user()->language == 'VN') Xóa yêu cầu @else Remove request @endif</button>
                </form>
            </div>
        </div>
        @endforeach
    </div>
</div>
<style type="text/css">
    .item{
        background-color: #fdfdfd;
        padding: 10px;
        border: 1px solid #f1f1f1;
    }
    .item img{
        border: 1px solid lightgray;
        height: 100px;
        width: 100px;
    }
    .item-full-row{
        margin-bottom: 10px;
    }
</style>
@endsection

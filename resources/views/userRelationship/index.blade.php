@extends('layouts.index')

@section('title') Search friends @endsection

@section('content')
<div class="col-md-12 box">
    <div class="row">
        <div class="col-md-12">
            @include('userRelationship.__titleRelationship')
        </div>
        <div class="col-md-12">
            <hr style="margin:0">
            <br>
        </div>
        @if(count($pendingRelationship) != 0)
        <div class="col-md-12">
            <label class="text-muted">@if(\Auth::user()->language == 'VN') Lời mời kết bạn @else Friend request @endif</label>
        </div>
        @endif
        @foreach($pendingRelationship as $userRelationship)
        <div class="col-md-12 item-full-row">
            <div class="item">
                <a href="/user/{{$userRelationship->user()->id}}"><img src="{{$userRelationship->user()->getAvatar()}}" class="img-thumbnail"></a>
                <label><a href="/user/{{$userRelationship->user()->id}}">{{$userRelationship->user()->name}}</a></label><br/>
                <span class="text-muted">{{$userRelationship->user()->getAge()}} @if(\Auth::user()->language == 'VN') tuổi @else age @endif</span><br/>
                <span class="text-muted">@if($userRelationship->user()->sex == 'male')<i class="fa fa-mars"></i>@else<i class="fa fa-venus"></i>@endif {{$userRelationship->user()->getHoroscope()}}</span><br/>
                <form method="POST" action="/relationship/accepted">
                    {{ csrf_field() }}
                    <input type="hidden" name="user_relationship_id" value="{{$userRelationship->id}}">
                    <input type="hidden" name="status" value="1">
                    <button class="btn btn-xs btn-primary" type="submit" style="float:left; margin-right: 5px"> @if(\Auth::user()->language == 'VN') Chấp nhận @else Agree @endif</button>
                </form>
                <form method="POST" action="/relationship/declined">
                    {{ csrf_field() }}
                    <input type="hidden" name="user_relationship_id" value="{{$userRelationship->id}}">
                    <input type="hidden" name="status" value="2">
                    <button class="btn btn-xs" type="submit">@if(\Auth::user()->language == 'VN') Từ chối @else Refuse @endif</button>
                </form>
            </div>
        </div>
        @endforeach
        @if(count($sendFriendsRequest) != 0)
        <div class="col-md-12">
            <label class="text-muted">@if(\Auth::user()->language == 'VN') Bạn đã gửi yêu cầu kết bạn tới @else You have sent a friend request to @endif
                <a class="btn btn-xs btn-default" role="button" data-toggle="collapse" href="#send-friends-request" aria-expanded="false" aria-controls="send-friends-request"><span class="caret"></span></a>
            </label>
        </div>
        @endif
        <div class="collapse" id="send-friends-request">
            @foreach($sendFriendsRequest as $userRelationship)
            <div class="col-md-12 item-full-row">
                <div class="item">
                    <a href="/user/{{$userRelationship->user()->id}}"><img src="{{$userRelationship->user()->getAvatar()}}" class="img-thumbnail"></a>
                    <label><a href="/user/{{$userRelationship->user()->id}}">{{$userRelationship->user()->name}}</a></label><br/>
                    <span class="text-muted">{{$userRelationship->user()->getAge()}} @if(\Auth::user()->language == 'VN') tuổi @else age @endif</span><br/>
                    <span class="text-muted">@if($userRelationship->user()->sex == 'male')<i class="fa fa-mars"></i>@else<i class="fa fa-venus"></i>@endif {{$userRelationship->user()->getHoroscope()}}</span><br/>
                    <form method="POST" action="/relationship/delete">
                        {{ csrf_field() }}
                        <input type="hidden" name="user_relationship_id" value="{{$userRelationship->id}}">
                        <button class="btn btn-xs" type="submit">@if(\Auth::user()->language == 'VN') Hủy @else Cancel @endif</button>
                    </form>
                </div>
            </div>
            @endforeach
        </div>
        <div class="col-md-12">
            <label class="text-muted">@if(\Auth::user()->language == 'VN') Gợi ý kết bạn theo cung hoàng đạo @else Suggestions to you under the zodiac @endif</label>
        </div>
    </div>

    <div class="row items">
        @foreach($users as $user)
        @if($user->hasRelationship() == false AND $user->role_id != 1)
        <div class="col-md-6">
            <div class="item">
                <a href="/user/{{$user->id}}"><img src="{{$user->getAvatar()}}" class="img-thumbnail"></a>
                <label><a href="/user/{{$user->id}}">{{$user->name}}</a></label><br/>
                <span class="text-muted">{{$user->getAge()}} @if(\Auth::user()->language == 'VN') tuổi @else age @endif</span><br/>
                <span class="text-muted">@if($user->sex == 'male')<i class="fa fa-mars"></i>@else<i class="fa fa-venus"></i>@endif {{$user->getHoroscope()}}</span><br/>
                <form method="POST" action="/relationship/create" style="float: left">
                    {{ csrf_field() }}
                    <input type="hidden" name="user_one_id" value="{{\Auth::user()->id}}">
                    <input type="hidden" name="user_two_id" value="{{$user->id}}">
                    <input type="hidden" name="status" value="0">
                    <button class="btn btn-xs btn-theme m-r-5" type="submit"><i class="fa fa-plus"></i> @if(\Auth::user()->language == 'VN') Kết bạn @else Make friend @endif</button>
                </form>
                <a href="/message/{{$user->id}}" class="btn btn-xs btn-outline"><i class="fa fa-comments"></i> @if(\Auth::user()->language == 'VN') Tin nhắn @else Message @endif</a>
                <button type="button" class="btn btn-xs btn-outline" data-toggle="modal" data-target="#myModal{{$user->id}}">Gợi ý</button>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Tình bạn của {{Auth::user()->getHoroscope()}} và {{$user->getHoroscope()}}</h4>
                    </div>
                    <div class="modal-body">
                        {{$user->getCouple()}}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">@if(\Auth::user()->language == 'VN') Đóng @else Close @endif</button>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @endforeach
    </div>
</div>
<style type="text/css">
    .items{
        padding: 0px 10px;
    }
    .item{
        background-color: #fdfdfd;
        padding: 10px;
        border: 1px solid #f1f1f1;
    }
    .items .col-md-6{
        height: 200px;
        padding: 5px;
    }
    .items .col-md-6 img,
    .item img{
        border: 1px solid lightgray;
        height: 100px;
        width: 100px;
    }
    .item-full-row{
        margin-bottom: 10px;
    }
</style>
@endsection

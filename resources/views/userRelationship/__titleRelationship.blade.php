<label><a href="/user/friend" title="@if(\Auth::user()->language == 'VN') Danh sách bạn bè @else List friends @endif">@if(\Auth::user()->language == 'VN') Bạn bè @else Friends @endif</a></label>
<ul class="list-inline pull-right">
    <li><a href="/relationship/declined/view" class="{{ \Request::is( 'relationship/declined/view') ? 'nav-active' : '' }}" title="@if(\Auth::user()->language == 'VN') Lời mời bạn đã từ chối @else Invitation refused @endif">@if(\Auth::user()->language == 'VN') Từ chối @else Refuse @endif</a></li>
    <li><a href="/relationship/blocked/view" class="{{ \Request::is( 'relationship/blocked/view') ? 'nav-active' : '' }}" title="@if(\Auth::user()->language == 'VN') Những người mà bạn đã chặn @else The people blocked @endif">@if(\Auth::user()->language == 'VN') Chặn @else Block @endif</a></li>
</ul>
<style type="text/css">
    .nav-active{
        color: #000;
    }
</style>
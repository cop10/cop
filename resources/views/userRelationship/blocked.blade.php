@extends('layouts.index')

@section('title') Blocked @endsection

@section('content')
<div class="col-md-12 box box-min">
    <div class="row">
        <div class="col-md-12">
            @include('userRelationship.__titleRelationship')
        </div>
        <div class="col-md-12">
            <hr style="margin:0">
            <br>
        </div>
        <div class="col-md-12">
            <form action="/relationship/block" method="POST">
                {{ csrf_field() }}
                <select data-placeholder="@if(\Auth::user()->language == 'VN') Nhập tên bạn bè @else Enter the name of friends @endif" class="chosen-select-width my-select" name="user_id" tabindex="16" required>
                <option>@if(\Auth::user()->language == 'VN') -- chọn -- @else -- select -- @endif</option>
                @foreach(\App\Models\UserRelationship::getFriend() as $friend)
                <option data-img-src="{{$friend->user()->getAvatar()}}" value="{{$friend->user()->id}}"><img src="{{$friend->user()->getAvatar()}}" height="30" width="30"> <label>{{$friend->user()->name}}</label></option>
                @endforeach
                </select>
                <button class="btn btn-sm btn-primary" style="padding: 2px 5px">@if(\Auth::user()->language == 'VN') Chặn @else Block @endif</button>
            </form>
            <br>
            <br>
        </div>
        @foreach($userRelationship as $userRe)
        <div class="col-md-12 item-full-row">
            <div class="item">
                <a href="/user/{{$userRe->user()->id}}"><img src="/assets/images/user.jpg" class="img-thumbnail"></a>
                <label><a href="/user/{{$userRe->user()->id}}">{{$userRe->user()->name}}</a></label><br/>
                <span class="text-muted">{{$userRe->user()->getAge()}} @if(\Auth::user()->language == 'VN') tuổi @else age @endif</span><br/>
                <span class="text-muted">@if($userRe->user()->sex == 'male')<i class="fa fa-mars"></i>@else<i class="fa fa-venus"></i>@endif {{$userRe->user()->getHoroscope()}}</span><br/>
                <form method="POST" action="/relationship/blocked">
                    {{ csrf_field() }}
                    <input type="hidden" name="user_relationship_id" value="{{$userRe->id}}">
                    <input type="hidden" name="status" value="1">
                    <button class="btn btn-xs" type="submit" style="float:left; margin-right: 5px">@if(\Auth::user()->language == 'VN') Bỏ chặn @else Unblock @endif</button>
                </form>
                <form method="POST" action="/relationship/delete">
                    {{ csrf_field() }}
                    <input type="hidden" name="user_relationship_id" value="{{$userRe->id}}">
                    <button class="btn btn-xs" title="Hủy kết bạn" type="submit">@if(\Auth::user()->language == 'VN') Hủy kết bạn @else Unfriend @endif</button>
                </form>
            </div>
        </div>
        @endforeach
    </div>
</div>
<style type="text/css">
    .item{
        background-color: #fdfdfd;
        padding: 10px;
        border: 1px solid #f1f1f1;
    }
    .item img{
        border: 1px solid lightgray;
        height: 100px;
        width: 100px;
    }
    .item-full-row{
        margin-bottom: 10px;
    }
</style>
<script type="text/javascript">
    var config = {
        '.chosen-select'           : {},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không tìm thấy!'},
        '.chosen-select-width'     : {width:"50%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
    $(".my-select").chosenImage({
      disable_search_threshold: 10
    });
</script>
@endsection

@extends('layouts.index')

@section('title') Create diary @endsection

@section('content')
<div class="col-md-12 box box-min">
    <div class="row">
        @include('user.__titleUser')
        <div class="col-md-12">
            <form action="/diary" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label>@if(\Auth::user()->language == 'VN') Tiêu đề @else Title @endif</label>
                    <input type="text" name="title" class="input-default" required>
                    @if ($errors->has('title'))
                        <label class="text-danger">{{ $errors->first('title') }}</label>
                    @endif
                </div>
                <div class="form-group">
                    <label>@if(\Auth::user()->language == 'VN') Nội dung @else Content @endif</label>
                    <textarea name="content" required class="ckeditor"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success">@if(\Auth::user()->language == 'VN') Tạo nhật ký @else Create @endif</button>
                    <a href="/diary" class="btn btn-link">@if(\Auth::user()->language == 'VN') Hủy @else Cancel @endif</a>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="/assets/plugin/ckeditor_diary/ckeditor.js"></script>
@endsection

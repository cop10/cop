@extends('layouts.index')

@section('title') My diary @endsection

@section('content')
<div class="col-md-12 box box-min">
    <div class="row">
        @include('user.__titleUser')
        <div class="col-md-12">
            <a href="/diary/create" class="m-b-5 pull-right"><i class="fa fa-plus"></i> @if(\Auth::user()->language == 'VN') Tạo mới @else New @endif</a>
            <table class="table">
                <br>
                <tr class="active">
                    <th><span class="text-muted">#</span></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                @foreach($diarys as $key => $diary)
                <tr>
                    <td style="width: 20px"><span class="text-muted">{{$key+1}}</span></td>
                    <td>{{$diary->title}}</td>
                    <td><a href="/diary/{{$diary->id}}/edit">@if(\Auth::user()->language == 'VN') Sửa @else Edit @endif</a></td>
                    <td><a href="/diary/{{$diary->id}}">@if(\Auth::user()->language == 'VN') Xem @else View @endif</a></td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
@endsection

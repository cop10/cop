@extends('layouts.index')

@section('title') {{$diary->title}} @endsection

@section('content')
<div class="col-md-12 box box-min">
    <div class="row">
        @include('user.__titleUser')
        <div class="col-md-12">
            <h3 style="color: #74d885">{{$diary->title}}</h3>
            <a href="/diary/{{$diary->id}}/edit" class="btn btn-link" style="float: left"><span class="fa fa-pencil"></span>@if(\Auth::user()->language == 'VN') sửa @else edit @endif</a>
            <form action="/diary/{{$diary->id}}" method="POST">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <button class="btn btn-link" style="color: #f32929" onclick="return confirm('@if(\Auth::user()->language == 'VN') Bạn chắc chắn muốn xóa nhật ký này? @else You sure you want to delete this diary? @endif')"><i class="fa fa-trash"></i> @if(\Auth::user()->language == 'VN') xóa @else delete @endif</button>
            </form>
            <p class="text-muted">{!! $diary->content !!}</p>
        </div>
    </div>
</div>
@endsection

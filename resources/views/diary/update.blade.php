@extends('layouts.index')

@section('title') {{$diary->title}} @endsection

@section('content')
<div class="col-md-12 box box-min">
    <div class="row">
        @include('user.__titleUser')
        <div class="col-md-12">
            <form action="/diary/{{$diary->id}}" method="POST">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="form-group">
                    <label>@if(\Auth::user()->language == 'VN') Tiêu đề @else Title @endif</label>
                    <input type="text" name="title" class="input-default" value="{{$diary->title}}" required>
                    @if ($errors->has('title'))
                        <label class="text-danger">{{ $errors->first('title') }}</label>
                    @endif
                </div>
                <div class="form-group">
                    <label>@if(\Auth::user()->language == 'VN') Nội dung @else Content @endif</label>
                    <textarea name="content" required class="ckeditor">{!! $diary->content !!}</textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success">@if(\Auth::user()->language == 'VN') Lưu thay đổi @else Save changes @endif</button>
                    <a href="/diary/{{$diary->id}}" class="btn btn-link">@if(\Auth::user()->language == 'VN') Hủy @else Cancel @endif</a>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="/assets/plugin/ckeditor_diary/ckeditor.js"></script>
@endsection

@extends('layouts.auth')

@section('content')
<form role="form" method="POST" action="{{ url('/login') }}" onsubmit="return changeButton()">
    {{ csrf_field() }}
    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
        @if ($errors->has('email'))
            <label class="text-danger">{{ $errors->first('email') }}</label>
        @endif
    </div>
    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <input id="password" type="password" class="form-control" name="password" placeholder="@if($_GET['language'] == 'VN') Mật khẩu @else Password @endif" required>
        @if ($errors->has('password'))
            <label class="text-danger">{{ $errors->first('password') }}</label>
        @endif
    </div>
    <div class="form-group">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="remember"> @if($_GET['language'] == 'VN') Ghi nhớ @else Remember @endif
            </label>
        </div>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-block btn-login"><span class="text-loading hidden"><i class="fa fa-spinner fa-spin"></i> Loading...</span> <span class="text-submit"><i class="fa fa-sign-in"></i> @if($_GET['language'] == 'VN') Đăng nhập @else Login @endif </span></button>
        <p>@if($_GET['language'] == 'VN') Bạn chưa có tài khoản? @else Don't have an account? @endif<a href="{{ url('/register') }}">@if($_GET['language'] == 'VN') Đăng ký @else Register @endif</a><br/><a href="{{ url('/password/reset') }}">@if($_GET['language'] == 'VN') Quên mật khẩu? @else Lost password? @endif</a></p>
    </div>
</form>
<script type="text/javascript">
    function changeButton() {
        $('.text-loading').removeClass('hidden');
        $('.text-submit').addClass('hidden');
    }
</script>
@endsection

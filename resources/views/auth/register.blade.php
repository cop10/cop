@extends('layouts.auth')

@section('content')
<form role="form" method="POST" action="{{ url('/register') }}">
    {{ csrf_field() }}
    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        <input id="name" type="text" class="form-control" name="name" placeholder="@if($_GET['language'] == 'VN') Tên @else Name @endif" value="{{ old('name') }}" required autofocus>
        @if ($errors->has('name'))
            <label class="text-danger">{{ $errors->first('name') }}</label>
        @endif
    </div>
    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder=" Email" required>
        @if ($errors->has('email'))
            <label class="text-danger">{{ $errors->first('email') }}</label>
        @endif
    </div>
    <div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
        <input id="datepicker" type="text" class="form-control" name="birthday" placeholder="@if($_GET['language'] == 'VN') Ngày sinh (năm-tháng-ngày) @else Birthday (Y-m-d) @endif" required autocomplete="off">
        @if ($errors->has('birthday'))
            <label class="text-danger">{{ $errors->first('birthday') }}</label>
        @endif
    </div>
    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <input id="password" type="password" class="form-control" name="password" placeholder="@if($_GET['language'] == 'VN') Mật khẩu @else Password @endif" required>
        @if ($errors->has('password'))
            <label class="text-danger">{{ $errors->first('password') }}</label>
        @endif
    </div>
    <div class="form-group">
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="@if($_GET['language'] == 'VN') Nhập lại mật khẩu @else Password confirmation @endif" required>
    </div>
    <div class="form-group">
        <input type="radio" name="sex" value="male"> @if($_GET['language'] == 'VN') Nam @else Male @endif
        <input type="radio" name="sex" value="female" required> @if($_GET['language'] == 'VN') Nữ @else Female @endif
        <br/>
        @if($_GET['language'] == 'VN')
        <input type="checkbox" name="rule" required value="true"> Bạn chấp nhận các <a href="#">điều khoản</a> của chúng tôi.
        @else
        <input type="checkbox" name="rule" required value="true"> You accept our <a href="#">terms</a>.
        @endif
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-register">@if($_GET['language'] == 'VN') Đăng ký @else Register @endif</button>
        <p>@if($_GET['language'] == 'VN') Bạn có tài khoản? @else You have an account? @endif<a href="{{ url('/login') }}">@if($_GET['language'] == 'VN') Đăng nhập @else Login @endif</a></p>
    </div>
</form>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $( function() {
        $( "#datepicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '1950:2016',
            dateFormat: "yy-mm-dd"
        });
    } );
</script>
@endsection

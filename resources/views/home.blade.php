@extends('layouts.index')

@section('title') HoroChat @endsection

@section('content')
<div class="col-md-12 box status">
    <div class="row">
        <div class="col-md-1 col-sm-1 col-xs-2">
            <img src="{{\Auth::user()->getAvatar()}}" class="img-circle img-avatar">
        </div>
        <div class="col-md-11 col-sm-11 col-xs-10">
            <form action="/status" id="createStatus" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <textarea name="content" placeholder="@if(\Auth::user()->language == 'VN') Đăng trạng thái ... @else what are you thinking? @endif" class="area-status"></textarea>
                <button type="button" class="btn btn-sm btn-theme" style="position: absolute;bottom: 0px;right: 15px;" onclick="return createStatus()">@if(\Auth::user()->language == 'VN') Đăng @else Publish @endif</button>
                <ul class="list-inline">
                    <li><p class="text-muted">@if(\Auth::user()->language == 'VN') Tải lên @else Upload @endif: </p></li>
                    <li><a href="javascript:;" id="selectImage" title="max size 10mb" class="text-muted"><i class="fa fa-picture-o"></i> @if(\Auth::user()->language == 'VN') Ảnh @else Image @endif</a></li>
                    <li><a href="javascript:;" id="selectAttachment" title="max size 10mb" class="text-muted"><i class="fa fa-paperclip"></i> @if(\Auth::user()->language == 'VN') Tệp @else Attachment @endif</a></li>
                </ul>
                <input type="file" name="images[]" multiple id="profile-img" style="display: none">
                <input type="file" name="attachment" id="attachment" style="display: none">
                <div class="preview-area"></div>
                <a href="javascript:;" class="hidden text-muted" id="rmImg"><i class="fa fa-times"></i> @if(\Auth::user()->language == 'VN') hủy @else cancel @endif</a>
                @if($errors->has('image'))
                <label class="text-danger">{{$errors->first('image')}}</label>
                @endif
                @if($errors->has('images'))
                <label class="text-danger">{{$errors->first('images')}}</label>
                @endif
                @if($errors->has('attachment'))
                <label class="text-danger">{{$errors->first('attachment')}}</label>
                @endif
            </form>
            <script type="text/javascript">
                var inputLocalFont = document.getElementById("profile-img");
                inputLocalFont.addEventListener("change",previewImages,false); //bind the function to the input

                function previewImages(){
                    var fileList = this.files;
                    var anyWindow = window.URL || window.webkitURL;
                    for(var i = 0; i < fileList.length; i++){
                        var objectUrl = anyWindow.createObjectURL(fileList[i]);
                        $('.preview-area').append('<img src="' + objectUrl + '" class="img-preview m-r-5 m-b-5" width="auto" height="50px" />');
                        window.URL.revokeObjectURL(fileList[i]);
                    }
                    if (fileList.length != 0) {
                        $('#rmImg').removeClass('hidden');
                    }
                }
                $("#selectImage").click(function(){
                    $('#profile-img').trigger('click');
                });
                $("#selectAttachment").click(function(){
                    $('#attachment').trigger('click');
                });
                $("#attachment").change(function() {
                    var filename = $('#attachment').val().split('\\').pop();
                    $('.preview-area').append('<i class="fa fa-file-o"></i> <span class="text-muted">'+filename+'</span>');
                    $('#rmImg').removeClass('hidden');
                });
                $('#rmImg').click(function() {
                    $('#profile-img').val(null);
                    $('#attachment').val(null);
                    $('.preview-area').empty();
                    $(this).addClass('hidden');
                });
                function createStatus() {
                    var img = $("#profile-img");
                    var text = $("#createStatus textarea");
                    if (img.val() && parseInt(img.get(0).files.length)>20){
                        @if(\Auth::user()->language == 'VN')
                        alert("Bạn chỉ có thể tải lên tối đa là 20 hình ảnh");
                        @else
                        alert("You can only upload a maximum of 20 files");
                        @endif
                        return false;
                    }else if (img.val() || text.val()) {
                        $('#createStatus').submit();
                    }
                    if (!img.val() && !text.val()) {
                        @if(\Auth::user()->language == 'VN')
                        alert('Bạn chưa nhập nội dung');
                        @else
                        alert('You have not entered content');
                        @endif
                    }
                }
            </script>
        </div>
    </div>
</div>
@foreach(\App\Models\Status::getFriendStatus() as $stt)
<div class="col-md-12 box post" id="stt{{$stt->id}}">
    <img src="{{$stt->user->getAvatar()}}" class="img-circle img-avatar">
    <a href="/user/{{$stt->user->id}}">{{$stt->user->name}}</a>
    <div class="btn-group" style="position: absolute;top: 5px;right: 5px">
        <button type="button" class="btn btn-sm btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i>
        </button>
        <ul class="dropdown-menu pull-right">
            @if($stt->created_by == \Auth::user()->id)
            <li><a role="button" href="#statusEdit{{$stt->id}}" data-toggle="collapse" aria-expanded="false" aria-controls="statusEdit{{$stt->id}}">@if(\Auth::user()->language == 'VN') Chỉnh sửa @else Edit @endif</a></li>
            <li>
                <form action="/status/{{$stt->id}}" method="POST" id="deletestatusEdit{{$stt->id}}" class="hidden">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                </form>
                <a href="javascript:;" onclick="return confirmDeleteStatus{{$stt->id}}()">@if(\Auth::user()->language == 'VN') Xóa bài viết @else Delete Post @endif</a>
            </li>
            @else
            <li><a href="#">@if(\Auth::user()->language == 'VN') Báo cáo spam hoặc lạm dụng @else Report spam or abuse @endif</a></li>
            @endif
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12">
            <small class="text-muted m-t-10">{{\App\Models\Status::getTimeCreated($stt->created_at)}}</small>
            <p class="contentStt">{{$stt->content}}</p>
            @if(count($stt->statusImages) == 1)
            <ul class="list-inline">
                @foreach($stt->statusImages as $images)
                <li><a href="{{$images->image}}" class="example-image-link" data-lightbox="example-1"><img src="{{$images->image}}" class="img-responsive img-hover-shadow example-image" alt=""></a></li>
                @endforeach
            </ul>
            @elseif(count($stt->statusImages) == 2)
            <div class="row">
                <div class="col-md-6" style="padding-right: 3px"><a class="example-image-link" data-lightbox="example-set" href="{{$stt->statusImages[0]->image}}" data-title=""><img src="{{$stt->statusImages[0]->image}}" class="img-responsive example-image img-hover-shadow" alt=""></a></div>
                <div class="col-md-6" style="padding-left: 3px"><a class="example-image-link" data-lightbox="example-set" href="{{$stt->statusImages[1]->image}}" data-title=""><img src="{{$stt->statusImages[1]->image}}" class="img-responsive example-image img-hover-shadow" alt=""></a></div>
            </div>
            @elseif(count($stt->statusImages) >= 3)
                @foreach($stt->statusImages as $images)
                <a class="example-image-link" data-lightbox="example-set" href="{{$images->image}}" data-title=""><img src="{{$images->image}}" style="max-height: 145px;float: left" class="img-responsive example-image m-r-5 m-b-5 img-hover-shadow" alt=""></a>
                @endforeach
            @endif
            <div class="clearfix"></div>
            @if($stt->statusAttachment)
                @foreach($stt->statusAttachment as $attachment)
                <p class="text-muted"><i class="fa fa-download"></i> <a download href="{{$attachment->attachment}}">{{substr($attachment->attachment,21)}}</a></p>
                @endforeach
            @endif
            <form action="/status/{{$stt->id}}" id="statusEdit{{$stt->id}}" class="edit-status-form m-t-5 collapse" method="POST">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <textarea name="content" class="textarea-default" spellcheck="false" required>{{$stt->content}}</textarea>
                <button type="submit" class="btn btn-success btn-sm pull-right">@if(\Auth::user()->language == 'VN') Lưu @else Save @endif</button>
                <a role="button" href="#statusEdit{{$stt->id}}" data-toggle="collapse" aria-expanded="false" aria-controls="statusEdit{{$stt->id}}" class="btn btn-link m-r-5 btn-sm pull-right">@if(\Auth::user()->language == 'VN') Hủy @else Cancel @endif</a>
            </form>
            <script type="text/javascript">
                $(function () {
                    $('#statusEdit{{$stt->id}}').on('submit', function (e) {
                        $.ajax({
                            type: 'post',
                            url: $(this).attr('action'),
                            data: $(this).serialize(),
                            success: function (data) {
                                $('#stt{{$stt->id}} .contentStt').text(data.content);
                                $('#statusEdit{{$stt->id}} a').trigger('click');
                            },
                            error: function(jqXHR, exception) {
                                if (jqXHR.status === 0) {
                                    alert('Not connect.\n Verify Network.');
                                } else if (jqXHR.status == 404) {
                                    alert('Requested page not found. [404]');
                                } else if (jqXHR.status == 500) {
                                    alert('Internal Server Error [500].');
                                } else if (exception === 'parsererror') {
                                    alert('Requested JSON parse failed.');
                                } else if (exception === 'timeout') {
                                    alert('Time out error.');
                                } else if (exception === 'abort') {
                                    alert('Ajax request aborted.');
                                } else {
                                    alert('Uncaught Error.\n' + jqXHR.responseText);
                                }
                            }
                        });
                        e.preventDefault();
                    });
                });
            </script>
            <form action="/status/like" id="like{{$stt->id}}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="status_id" value="{{$stt->id}}">

                <div class="button-group">
                <a href="javascript:;" class="btn focus-reply{{$stt->id}} btn-link" style="padding-left: 0"><i class="fa fa-reply"></i> @if(\Auth::user()->language == 'VN') Trả lời @else Reply @endif</a>
                @if($stt->checkLike())
                    <button class="btn btn-link" style="padding: 0" title="Đã thích"><i class="fa fa-heart"></i> @if(\Auth::user()->language == 'VN') Thích @else Like @endif @if(count($stt->likes) != 0) {{count($stt->likes)}} @endif</button>
                    @else
                    <button class="btn btn-link" style="padding: 0" title="Thích"><i class="fa fa-heart-o"></i> @if(\Auth::user()->language == 'VN') Thích @else Like @endif @if(count($stt->likes) != 0) {{count($stt->likes)}} @endif</button>
                @endif
                </div>
            </form>
            <script type="text/javascript">
                $(function () {
                    $('#like{{$stt->id}}').on('submit', function (e) {
                        $.ajax({
                            type: 'post',
                            url: $(this).attr('action'),
                            data: $(this).serialize(),
                            success: function (data) {
                                $('#like{{$stt->id}} button').remove();
                                $('#like{{$stt->id}} .button-group').append(data.button);
                            },
                            error: function(jqXHR, exception) {
                                if (jqXHR.status === 0) {
                                    alert('Not connect.\n Verify Network.');
                                } else if (jqXHR.status == 404) {
                                    alert('Requested page not found. [404]');
                                } else if (jqXHR.status == 500) {
                                    alert('Internal Server Error [500].');
                                } else if (exception === 'parsererror') {
                                    alert('Requested JSON parse failed.');
                                } else if (exception === 'timeout') {
                                    alert('Time out error.');
                                } else if (exception === 'abort') {
                                    alert('Ajax request aborted.');
                                } else {
                                    alert('Uncaught Error.\n' + jqXHR.responseText);
                                }
                            }
                        });
                        e.preventDefault();
                    });
                });
            </script>
        </div>
    </div>
    <hr style="margin-top: 5px">
    <div class="row" id="commentsArea{{$stt->id}}">
        @foreach($stt->comments as $comment)
        <div id="comment{{$comment->id}}">
            <div class="col-md-1 col-sm-1 col-xs-2">
                <img src="{{$comment->user->getAvatar()}}" class="img-circle img-avatar-comment">
            </div>
            <div class="col-md-11 col-sm-11 col-xs-10">
                <a href="/user/{{$comment->user->id}}">{{$comment->user->name}}</a><br/>
                <small class="text-muted">{{\App\Models\Status::getTimeCreated($comment->created_at)}}</small><br/>
                <p class="contentComment">{{$comment->content}}</p>
                <form action="/status/comment/{{$comment->id}}" id="commentEdit{{$comment->id}}" method="POST" class="collapse">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <textarea name="content" class="textarea-default" spellcheck="false" required>{{$comment->content}}</textarea>
                    <button type="submit" class="btn btn-sm btn-success pull-right">@if(\Auth::user()->language == 'VN') Lưu @else Save @endif</button>
                    <a role="button" href="#commentEdit{{$comment->id}}" data-toggle="collapse" aria-expanded="false" aria-controls="commentEdit{{$comment->id}}" class="btn btn-sm m-r-5 m-b-10 btn-link pull-right">@if(\Auth::user()->language == 'VN') Hủy @else Cancel @endif</a>
                    <br>
                </form>
                <div class="btn-group" style="position: absolute;top:-5px;right:10px">
                    <button type="button" class="btn btn-sm btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i>
                    </button>
                    <ul class="dropdown-menu pull-right">
                        @if($comment->created_by == \Auth::user()->id)
                        <li><a role="button" href="#commentEdit{{$comment->id}}" data-toggle="collapse" aria-expanded="false" aria-controls="commentEdit{{$comment->id}}">@if(\Auth::user()->language == 'VN') Chỉnh sửa @else Edit @endif</a></li>
                        <li>
                            <form action="/status/comment/{{$comment->id}}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                @if(\Auth::user()->language == 'VN')
                                <button class="btn-li-dropdown" onclick="return confirm('Bạn chắc chắn muốn xóa bình luận này?')">Xóa bình luận</button>
                                @else
                                <button class="btn-li-dropdown" onclick="return confirm('You sure you want to delete this comment?')">Delete comment</button>
                                @endif
                            </form>
                        </li>
                        @else
                        <li><a href="#">@if(\Auth::user()->language == 'VN') Báo cáo spam hoặc lạm dụng @else Report spam or abuse @endif</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col-md-1 col-sm-1 col-xs-2">
            <img src="{{\Auth::user()->getAvatar()}}" class="img-circle img-avatar-comment">
        </div>
        <div class="col-md-11 col-sm-11 col-xs-10">
            <form action="/status/comment" id="createComment{{$stt->id}}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="status_id" value="{{$stt->id}}">
                <input type="text" autocomplete="off" placeholder="@if(\Auth::user()->language == 'VN') Bình luận ... @else Comment ... @endif" name="content" required>
            </form>
        </div>
    </div>
</div>
<link href="/assets/plugin/lightbox2-master/css/lightbox.min.css" rel="stylesheet" type="text/css">
<script src="/assets/plugin/lightbox2-master/js/lightbox-plus-jquery.min.js"></script>
<script type="text/javascript">
    function confirmDeleteStatus{{$stt->id}}(){
        var agree=confirm("@if(\Auth::user()->language == 'VN') Bạn chắc chắn muốn xóa bài viết này? @else You sure you want to delete this post? @endif");
        if (agree)
            $('#deletestatusEdit{{$stt->id}}').submit();
        else
            return false ;
    }
    $(function () {
        $('#createComment{{$stt->id}}').on('submit', function (e) {
            $.ajax({
                type: 'post',
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function (data) {
                    $("#createComment{{$stt->id}} input[name='content']").val('');
                    $("#commentsArea{{$stt->id}}").load("/ #commentsArea{{$stt->id}}>*", "");
                },
                error: function(jqXHR, exception) {
                    if (jqXHR.status === 0) {
                        alert('Not connect.\n Verify Network.');
                    } else if (jqXHR.status == 404) {
                        alert('Requested page not found. [404]');
                    } else if (jqXHR.status == 500) {
                        alert('Internal Server Error [500].');
                    } else if (exception === 'parsererror') {
                        alert('Requested JSON parse failed.');
                    } else if (exception === 'timeout') {
                        alert('Time out error.');
                    } else if (exception === 'abort') {
                        alert('Ajax request aborted.');
                    } else {
                        alert('Uncaught Error.\n' + jqXHR.responseText);
                    }
                }
            });
            e.preventDefault();
        });
        $('.focus-reply{{$stt->id}}').click(function () {
            $("#createComment{{$stt->id}} input[name='content']").focus();
        });
    });
</script>
@endforeach
@endsection

@extends('layouts.index')

@section('title') {{$group->title}} @endsection

@section('content')
<div class="col-md-12 box box-min">
    <div class="row">
        <div class="col-md-12">
            <label><a href="/group/{{$group->id}}">{{$group->title}}</a></label>
            <hr style="margin-top: 0">
        </div>
        <div class="col-md-10 col-md-offset-1">
            <form action="/group/{{$group->id}}" method="POST" class="form-horizontal">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="form-group">
                    <p class="col-md-3 text-muted">@if(\Auth::user()->language == 'VN') Tên nhóm @else Name of group @endif</p>
                    <div class="col-md-9">
                        <input type="text" name="title" style="width: 100%;height: 30px;padding: 3px" value="{{$group->title}}" required>
                        @if ($errors->has('title'))
                            <label class="text-danger">{{ $errors->first('title') }}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <p class="col-md-3 text-muted">@if(\Auth::user()->language == 'VN') Thành viên @else Members @endif</p>
                    <div class="col-md-9">
                        @if ($errors->has('users_id'))
                            <label class="text-danger">{{ $errors->first('users_id') }}</label>
                        @endif
                        <select data-placeholder="@if(\Auth::user()->language == 'VN') Nhập tên bạn bè @else Enter the name of friends @endif" multiple class="chosen-select-width my-select" name="users_id[]" tabindex="16" required>
                        @foreach(\App\Models\UserRelationship::getFriend() as $friend)
                            <option data-img-src="{{$friend->user()->getAvatar()}}" value="{{$friend->user()->id}}"
                            @foreach($usersGroups as $usersGroup)
                            @if($usersGroup->user->id == $friend->user()->id)
                            selected
                            @endif
                            @endforeach
                            ><img src="{{$friend->user()->getAvatar()}}" height="30" width="30"> <label>{{$friend->user()->name}}</label></option>
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3"></div>
                    <div class="col-md-9">
                        <button class="btn btn-success" onclick="return confirm('@if(\Auth::user()->language == 'VN') Bạn có muốn lưu những thay đổi này không? @else Do you want to save these changes do not? @endif');"><i class="fa fa-edit"></i> @if(\Auth::user()->language == 'VN') Lưu thay đổi @else Save changes @endif</button>
                        <a href="/group/{{$group->id}}" class="btn btn-default">@if(\Auth::user()->language == 'VN') Hủy @else Cancel @endif</a>
                    </div>
                </div>
            </form>
            <a role="button" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">@if(\Auth::user()->language == 'VN') Thêm tùy chọn @else More options @endif</a>
            <div class="collapse" id="collapseExample">
                <hr style="margin-top: 7px">
                <form action="/group/{{$group->id}}" method="POST" class="form-horizontal">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <div class="form-group">
                        <div class="col-md-3"></div>
                        <div class="col-md-9">
                            <button class="btn btn-danger" onclick="return confirm('@if(\Auth::user()->language == 'VN') Bạn chắc chắn muốn xóa nhóm này? @else You sure you want to delete this group? @endif');"><i class="fa fa-trash"></i> @if(\Auth::user()->language == 'VN') Xóa nhóm @else Delete group @endif</button>
                            <span class="text-muted">@if(\Auth::user()->language == 'VN') Lưu ý: tất cả tin nhắn sẽ bị xóa @else Note: All messages will be deleted @endif</span>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <small class="text-muted" style="position: absolute;bottom:10px;"><i class="fa fa-info-circle "></i> @if(\Auth::user()->language == 'VN') Chỉ các thành viên trong nhóm mới có thể trò chuyện với nhau, bạn là người quản lý nhóm, có thể thêm hoặc xóa thành viên, đổi tên nhóm. @else Only group members can chat with each other, only you are group manager, you can add or remove users, change the group name. @endif</small>
</div>
<script type="text/javascript">
    var config = {
        '.chosen-select'           : {},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không tìm thấy!'},
        '.chosen-select-width'     : {width:"100%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
    $(".my-select").chosenImage({
      disable_search_threshold: 10
    });
</script>
@endsection

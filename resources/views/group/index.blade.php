@extends('layouts.index')

@section('title') My groups @endsection

@section('content')
<div class="col-md-12 box box-min">
    <div class="row">
        <div class="col-md-12">
            <label><a href="javascript:;">@if(\Auth::user()->language == 'VN') Nhóm @else Groups @endif</a></label>
            <hr style="margin-top: 0">
        </div>
        <div class="col-md-12">
            @foreach(\App\Models\Group::getGroups() as $group)
            <a href="/group/{{$group->id}}">
                <div class="col-md-12 box-my-group">
                    <div class="bg-group">
                        <img src="/assets/images/group-avatar.png" height="98" width="98"> <label>{{$group->title}}</label>
                        <p class="text-muted">@if($group->getCurrentMessase()) <span class="text-info">{{$group->getCurrentMessase()->user->name}}</span>: {{str_limit($group->getCurrentMessase()->content, $limit = 150, $end = '...')}} @endif</p>
                        @if($group->getCurrentMessase())
                        <small class="text-muted">{{\App\Models\Status::getTimeCreated($group->getCurrentMessase()->created_at)}}</small>
                        @endif
                    </div>
                </div>
            </a>
            @endforeach
        </div>
    </div>
</div>
<style type="text/css">
    .box-my-group{
        padding: 5px 0;
    }
    .box-my-group>.bg-group{
        height: 100px;
        background-color: #f9f9f9;
        border: 1px solid #f5f5f5;
    }
    .box-my-group>.bg-group>label{
        position: absolute;
        top: 12px;
        left: 105px;
    }
    .box-my-group>.bg-group>p{
        position: absolute;
        top: 30px;
        left: 105px;
    }
    .box-my-group>.bg-group>small{
        position: absolute;
        bottom: 10px;
        right: 10px;
    }
</style>
@endsection

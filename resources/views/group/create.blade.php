@extends('layouts.index')

@section('title') Create group @endsection

@section('content')
<div class="col-md-12 box box-min">
    <div class="row">
        <div class="col-md-12">
            <label><a href="/group">@if(\Auth::user()->language == 'VN') Nhóm @else Groups @endif</a></label>
            <hr style="margin-top: 0">
        </div>
        <div class="col-md-10 col-md-offset-1">
            @if(count(\App\Models\UserRelationship::getFriend()) == 0)
            <p><label class="text-danger">@if(\Auth::user()->language == 'VN') Bạn chưa có bạn bè, hãy kết bạn trước khi tạo nhóm. @else You have no friends, make friends before creating groups. @endif</label></p>
            @endif
            <form action="/group" method="POST" class="form-horizontal">
                {{ csrf_field() }}
                <div class="form-group">
                    <p class="col-md-3">@if(\Auth::user()->language == 'VN') Tên nhóm @else Name of group @endif</p>
                    <div class="col-md-9">
                        <input type="text" name="title" style="width: 100%;height: 30px;padding: 3px" required>
                        @if ($errors->has('title'))
                            <label class="text-danger">{{ $errors->first('title') }}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <p class="col-md-3">@if(\Auth::user()->language == 'VN') Thành viên @else Members @endif</p>
                    <div class="col-md-9">
                        @if ($errors->has('users_id'))
                            <label class="text-danger">{{ $errors->first('users_id') }}</label>
                        @endif
                        <select data-placeholder="@if(\Auth::user()->language == 'VN') Nhập tên bạn bè @else Enter the name of friends @endif" multiple class="chosen-select-width my-select" name="users_id[]" tabindex="16" required>
                        @foreach(\App\Models\UserRelationship::getFriend() as $friend)
                        <option data-img-src="{{$friend->user()->getAvatar()}}" value="{{$friend->user()->id}}"><img src="{{$friend->user()->getAvatar()}}" height="30" width="30"> <label>{{$friend->user()->name}}</label></option>
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3"></div>
                    <div class="col-md-9">
                        <button class="btn btn-success">@if(\Auth::user()->language == 'VN') Tạo nhóm @else Create group @endif</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <small class="text-muted" style="position: absolute;bottom:10px;"><i class="fa fa-info-circle "></i> @if(\Auth::user()->language == 'VN') Chỉ các thành viên trong nhóm mới có thể trò chuyện với nhau, bạn là người quản lý nhóm, có thể thêm hoặc xóa thành viên, đổi tên nhóm. @else Only group members can chat with each other, only you are group manager, you can add or remove users, change the group name. @endif</small>
</div>
<script type="text/javascript">
    var config = {
        '.chosen-select'           : {},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không tìm thấy!'},
        '.chosen-select-width'     : {width:"100%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
    $(".my-select").chosenImage({
      disable_search_threshold: 10
    });
</script>
@endsection

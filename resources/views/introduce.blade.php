<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="assets/images/logo.png">
    <title>HoroChat</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/auth.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
</head>
<body>
    <nav class="navbar">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/login"><img src="assets/images/logo.png"> Horo<span>Chat</span></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    @if($_GET['language'] == 'VN')
                    <li><a href="/introduce">Giới thiệu</a></li>
                    @else
                    <li><a href="/introduce">Introduce</a></li>
                    @endif
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">@if($_GET['language'] == 'VN') Ngôn ngữ: Tiếng việt @else Language: English @endif<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="?language=VN">@if($_GET['language'] == 'VN') Tiếng Việt @else vietnamese @endif @if($_GET['language'] == 'VN') <i class="fa fa-check"></i> @endif</a></li>
                            <li><a href="?language=EN">English @if($_GET['language'] == 'EN') <i class="fa fa-check"></i> @endif</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <div class="container content">
        <div class="col-md-6 col-md-offset-3 content-page">
            <h4>TRANG WEB GIAO LƯU KẾT BẠN TRỰC TUYẾN</h4>
            <p>Với việc kết bạn qua việc gặp gỡ, nhắn tin SMS, gọi điện thoại, được người nào đó giới thiệu rất phức tạp, tốn thời gian, thì bây giờ việc kết bạn sẽ trở nên rất dễ dàng đối với mọi người. Bằng việc click chuột vào trang web Giao lưu kết bạn trực tuyến thì bạn có thể kết bạn với mọi người một cách nhanh chóng.</p>
            <p>Vậy phần mềm này có gì hay?</p>
            <ul>
                <li>Kết bạn thông qua việc tìm kiếm theo số điện thoại, địa chỉ, email, tên, tuổi, giới tính và đặc biệt có thể tìm kiếm qua cung hoàng đạo một cách dễ dàng.</li>
                <li>Không cần đi đâu chỉ việc ở nhà cũng có thể kết bạn với nhau.</li>
                <li>Biết được thông tin về người muốn kết bạn.</li>
                <li>Tích hợp chức năng “Chat” để mọi người trò chuyện thoải mãi với nhau.</li>
                <li>Người dùng có thể đăng status lên đây , comment, like … </li>
            </ul>
            <p>Vậy nên, việc tạo một phần mềm như vậy sẽ rất là cần thiết cho mọi người trong thời đại phát triển hiện nay.</p>
            <p>Tất cả mọi người đều có thể sử dụng trang web một cách dễ dàng, chỉ cần tạo tài khoản trên HoroChat là có thể kết bạn với người mình thích và chát trực tuyến mọi lúc mọi nơi.</p>
            <p>Đặc biệt HoroChat có một Blog về cung hoàng đạo cho tất cả mọi người có thể xem, tìm hiểu về nó, hiểu tính cách của mình, của bạn, và rất nhiều mối quan hệ khác nữa.</p>
        </div>
        <div class="col-md-6 col-md-offset-3">
            <ul class="list-inline footer">
                @if($_GET['language'] == 'VN')
                <li><a href="/introduce">Giới thiệu</a></li>
                <li><a href="#">Trợ giúp</a></li>
                <li><a href="#">Liên hệ</a></li>
                <li><a href="#">Điều khoản</a></li>
                <li><a href="/relationship/index">Tìm bạn bè</a></li>
                <li><a href="/blog">Blog</a></li>
                <li><a href="javascript:;">© 2016 HoroChat</a></li>
                @else
                <li><a href="/introduce">Introduce</a></li>
                <li><a href="#">Help</a></li>
                <li><a href="#">Contact</a></li>
                <li><a href="#">Terms</a></li>
                <li><a href="/relationship/index">Find friend</a></li>
                <li><a href="/blog">Blog</a></li>
                <li><a href="javascript:;">© 2016 HoroChat</a></li>
                @endif
            </ul>
        </div>
    </div>
    <style type="text/css">
        .content-page{
            background-color: #fff;
            border-radius: 3px;
            -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=80)";
            filter: alpha(opacity=80);
            -moz-opacity:0.8;
            -khtml-opacity: 0.8;
            opacity: 0.8;
        }
    </style>
</body>
</html>
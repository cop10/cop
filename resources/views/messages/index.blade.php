@extends('layouts.index')

@section('title') Messages @endsection

@section('content')
<div class="col-md-12 box">
    <div class="row">
        <div class="col-md-12">
            <label style="color: #337ab7">@if(\Auth::user()->language == 'VN') Tin nhắn @else Messages @endif</label>
            <a href="/message/new" class="pull-right">@if(\Auth::user()->language == 'VN') Tin nhắn mới @else New message @endif</a>
            <hr style="margin: 5px 0 10px 0">
        </div>
        <div class="col-md-12">
            @foreach(\App\Models\User::lastMessages() as $message)
            @if($message->receiver_id)
            <a href="/message/{{$message->getUser->id}}">
                <div class="items-all-message">
                    <div class="item-content" style="@if($message->receiver_id == \Auth::user()->id AND $message->status == 1) background-color: #f8f8f8 @endif">
                        <img src="{{$message->getUser->getAvatar()}}" height="48" width="48">
                        <label>{{$message->getUser->name}} @if($message->getUser->isOnline()) <i class="fa fa-circle circle-online"></i> @endif </label>
                        <p class="text-muted">{{str_limit($message->content, $limit = 50, $end = '...')}}</p>
                        <small class="text-muted">{{\App\Models\Status::getTimeCreated($message->created_at)}}</small>
                        @if($message->receiver_id == \Auth::user()->id AND $message->status == 1)
                        <span class="fa fa-circle text-muted"></span>
                        @endif
                    </div>
                </div>
            </a>
            @else
            <a href="/group/{{$message->group_id}}">
                <div class="items-all-message">
                    <div class="item-content" style="@if($message->sender_id != \Auth::user()->id AND $message->status == 1) background-color: #f8f8f8 @endif">
                        <img src="/assets/images/group-avatar.png" height="48" width="48">
                        <label>{{$message->group->title}} <i class="fa fa-users text-muted"></i></label>
                        <p class="text-muted">{{str_limit($message->content, $limit = 50, $end = '...')}}</p>
                        <small class="text-muted">{{\App\Models\Status::getTimeCreated($message->created_at)}}</small>
                        @if($message->sender_id != \Auth::user()->id AND $message->status == 1)
                        <span class="fa fa-circle text-muted"></span>
                        @endif
                    </div>
                </div>
            </a>
            @endif
            @endforeach
        </div>
    </div>
</div>
<style type="text/css">
    .items-all-message{
        margin-bottom: 10px;
    }
    .items-all-message>.item-content{
        height: 50px;
        background-color: #fff;
        border: 1px solid #f2f2f2;
        position: relative;
    }
    .items-all-message>.item-content>label{
        position: absolute;
        top: 5px;
        left: 60px;
    }
    .items-all-message>.item-content>p{
        position: absolute;
        top: 23px;
        left: 60px;
    }
    .items-all-message>.item-content>small{
        position: absolute;
        bottom: 5px;
        right: 5px;
    }
    .items-all-message>.item-content>span{
        position: absolute;
        top: 10px;
        right: 5px;
        color: orange;
        font-size: 10px;
    }
</style>
@endsection

@extends('layouts.index')

@section('title') {{$user->name}} @endsection

@section('content')
<div class="col-md-12 box">
    <label><a href="/user/{{$user->id}}">{{$user->name}}</a></label>@if($user->isOnline()) <small class="text-online">online</small> @endif
    <a href="javascript:;" id="icon-show-search" class="pull-right m-l-5"><i class="fa fa-search"></i></a>
    <a href="javascript:;" id="icon-hide-search" class="pull-right hidden m-l-5"><i class="fa fa-times"></i></a>
    <input type="text" id="search-message" class="pull-right" placeholder="@if(\Auth::user()->language == 'VN') Tìm kiếm @else Search @endif">
    <a href="/message" class="pull-right hidden-xs m-r-10">@if(\Auth::user()->language == 'VN') Tất cả tin nhắn @else All messages @endif</a>
    <!-- <hr style="margin: 5px 0px 0px 0px"> -->
    <div class="box-chat scroll-style m-t-5">
        @foreach($messages as $message)
        @if($message->isFirstDailyMessage())
        <h6 class="text-center first-message">
            @if(date('d-m-Y', strtotime($message->created_at)) == date('d-m-Y'))
            @if(\Auth::user()->language == 'VN') hôm nay @else today @endif
            @else
            {{date('d-m-Y', strtotime($message->created_at))}}
            @endif
        </h6>
        @endif
        @if($message->sender_id == $user->id)
        <div class="row">
            <div class="col-md-1">
                <img src="{{$message->user->getAvatar()}}" class="img-circle" height="35" width="35">
            </div>
            <div class="col-md-9">
                <p class="message-left">{{$message->content}}</p>
            </div>
            <div class="col-md-2">
                <small class="pull-right time-box-message">{{date('g:i A', strtotime($message->created_at))}}</small>
            </div>
        </div>
        @else
        <div class="row">
            <div class="col-md-10">
                <p class="message-right">{{$message->content}}</p>
            </div>
            <div class="col-md-2">
                <small class="pull-right time-box-message">{{date('g:i A', strtotime($message->created_at))}}</small>
            </div>
        </div>
        @endif
        @endforeach
    </div>
    <div class="form-chats">
        <form action="/message/create/{{$user->id}}" id="sendmessage" method="POST">
            {{ csrf_field() }}
            <textarea name="content" placeholder="@if(\Auth::user()->language == 'VN') Viết tin nhắn @else Write Message @endif" required class="form-control message"></textarea>
            <!-- <ul class="list-inline">
                <li><a href="javascript:;"><i class="fa fa-lg fa-paperclip"></i></a></li>
                <li><a href="javascript:;"><i class="fa fa-lg fa-smile-o"></i></a></li>
                <li><a href="javascript:;"><i class="fa fa-lg fa-picture-o"></i></a></li>
            </ul> -->
            <button type="button" class="btn btn-sm btn-outline pull-right send-msg"><i class="fa fa-send-o"></i> @if(\Auth::user()->language == 'VN') Gửi @else Send @endif</button>
        </form>
    </div>
</div>
<script type="text/javascript">
    // send message
    var auth_id = {{ \Auth::user()->id }}
    var user_id = {{ $user->id }}
    socket.on('message', function (data) {
        data = jQuery.parseJSON(data);
        console.log(data);
        if (data.sender_id == auth_id && data.receiver_id == user_id) {
            $( ".box-chat" ).append( '<div class="row"><div class="col-md-10"><p class="message-right">'+data.content+'</p></div><div class="col-md-2"><small class="pull-right time-box-message">'+data.created_at+'</small></div></div>' );
            var wtf    = $('.box-chat');
            var height = wtf[0].scrollHeight;
            wtf.scrollTop(height);
        }else if(data.sender_id == user_id && data.receiver_id == auth_id){
            $( ".box-chat" ).append( '<div class="row"><div class="col-md-1"><img src="'+data.avatar+'" class="img-circle" height="35" width="35"></div><div class="col-md-9"><p class="message-left">'+data.content+'</p></div><div class="col-md-2"><small class="pull-right time-box-message">'+data.created_at+'</small></div></div>' );
            var wtf    = $('.box-chat');
            var height = wtf[0].scrollHeight;
            wtf.scrollTop(height);
        }
    });
    $(".send-msg").click(function(e){
        e.preventDefault();
        var msg = $(".message").val();
        if(msg != ''){
            $.ajax({
                type: "POST",
                url: $('#sendmessage').attr('action'),
                dataType: "json",
                data: $('#sendmessage').serialize(),
                success:function(data){
                    console.log(data);
                    $(".message").val('');
                    var wtf    = $('.box-chat');
                    var height = wtf[0].scrollHeight;
                    wtf.scrollTop(height);
                },
                error: function(jqXHR, exception) {
                    if (jqXHR.status === 0) {
                        alert('Not connect.\n Verify Network.');
                    } else if (jqXHR.status == 404) {
                        alert('Requested page not found. [404]');
                    } else if (jqXHR.status == 500) {
                        alert('Internal Server Error [500].');
                    } else if (exception === 'parsererror') {
                        alert('Requested JSON parse failed.');
                    } else if (exception === 'timeout') {
                        alert('Time out error.');
                    } else if (exception === 'abort') {
                        alert('Ajax request aborted.');
                    } else {
                        alert('Uncaught Error.\n' + jqXHR.responseText);
                    }
                }
            });
        }
    });
    //search message
    $("#search-message").keyup(function(){
        var keyword = $(this).val();
        var upperKeyword = keyword.toUpperCase();
        var messages = $(".box-chat").find("p");
        for (var i = 0; i < messages.length; i++) {
            var message = $(messages[i]).text();
            var upperMessage = message.toUpperCase();
            if (keyword && keyword != ' ') {
                if (upperMessage.indexOf(upperKeyword) == -1) {
                    $(messages[i]).css("color", "#000");
                }else{
                    $(messages[i]).css("color", "red");
                }
            }else{
                $('.message-right').css("color", "#fff");
                $('.message-left').css("color", "#5d5d5d");
            }
        }
    });
    // scroll box to buttom
    $(function() {
        var wtf    = $('.box-chat');
        var height = wtf[0].scrollHeight;
        wtf.scrollTop(height);
    });
    $('#icon-show-search').click(function(){
        $('#icon-hide-search').removeClass('hidden');
        $(this).addClass('hidden');
        $('#search-message').show().animate({ width: "30%" }, 300 );
    });
    $('#icon-hide-search').click(function(){
        $(this).addClass('hidden');
        $('#icon-show-search').removeClass('hidden');
        $('#search-message').animate({ width: "0%" }, 300 ).hide();
    });
</script>
<style type="text/css">
    #search-message{
        padding: 0 5px;
        display: none;
        width:0px;
    }
</style>
@endsection

@extends('layouts.index')

@section('title') New message @endsection

@section('content')
<div class="col-md-12 box box-min">
    <div class="row">
        <div class="col-md-12">
            <label><a href="/message">@if(\Auth::user()->language == 'VN') Tin nhắn @else Messages @endif</a></label>
            <hr style="margin: 5px 0 10px 0">
        </div>
        <div class="col-md-12">
            <form action="/message/new" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <p class="text-muted">@if(\Auth::user()->language == 'VN') Người nhận @else Name friends @endif</p>
                    <select data-placeholder="@if(\Auth::user()->language == 'VN') Nhập tên bạn bè @else Enter the name of friends @endif" multiple class="chosen-select-width my-select" name="users_id[]" tabindex="16" required>
                    @foreach(\App\Models\UserRelationship::getFriend() as $friend)
                    <option data-img-src="{{$friend->user()->getAvatar()}}" value="{{$friend->user()->id}}"><img src="{{$friend->user()->getAvatar()}}" height="30" width="30"> <label>{{$friend->user()->name}}</label></option>
                    @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <p class="text-muted">@if(\Auth::user()->language == 'VN') Nội dung @else Content @endif</p>
                    <textarea name="content" style="width: 100%;height: 200px" required></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-sm btn-theme"><i class="fa fa-paper-plane"></i> @if(\Auth::user()->language == 'VN') Gửi @else Send @endif</button>
                </div>
            </form>
        </div>
    </div>
    <small class="text-muted" style="position: absolute;bottom:10px;"><i class="fa fa-info-circle"></i> @if(\Auth::user()->language == 'VN') bạn có thể gửi tin nhắn cho nhiều người cùng lúc @else you can send messages to multiple people at once @endif</small>
</div>
<script type="text/javascript">
    var config = {
        '.chosen-select'           : {},
        '.chosen-select-deselect'  : {allow_single_deselect:true},
        '.chosen-select-no-single' : {disable_search_threshold:10},
        '.chosen-select-no-results': {no_results_text:'Không tìm thấy!'},
        '.chosen-select-width'     : {width:"50%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
    $(".my-select").chosenImage({
      disable_search_threshold: 10
    });
</script>
@endsection

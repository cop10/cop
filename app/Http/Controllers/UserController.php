<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserRelationship;
use App\Models\StatusImage;

class UserController extends Controller
{
    function __construct(){
        $this->middleware('admin', ['only' => ['postChangeRole', 'getList']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::find(\Auth::user()->id);
        return view('user.index',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $user = User::find(\Auth::user()->id);
        if ($user->user_name == $request->user_name) {
            $userNameValidator = 'max:255';
        }else{
            $userNameValidator = 'unique:users|alpha_dash|min:5|max:15';
        }
        $rules = array('name'  => 'required|max:255',
                       'address'  => 'max:255',
                       'full_name'  => 'max:255',
                       'religion'  => 'max:255',
                       'user_name'  => $userNameValidator,
                       'birthday'  => 'required|date_format:Y-m-d',
                       'avatar'  => 'mimes:jpeg,jpg,png,gif|dimensions:ratio=1/1|max:10240',
                       'phone'  => 'max:255');
        $validator = \Validator::make(\Input::all(),$rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $user->name = $request->name;
        $user->sex = $request->sex;
        $user->user_name = $request->user_name;
        $user->address = $request->address;
        $user->full_name = $request->full_name;
        $user->phone = $request->phone;
        $user->description = $request->description;
        $user->hobby = $request->hobby;
        $user->religion = $request->religion;
        $user->language = $request->language;
        $user->birthday = $request->birthday;
        if ($request->avatar) {
            $destinationPath = 'uploads/avatars';
            $natureFile = $request->avatar->getClientOriginalName();
            $extension = \App\Helper\UploadHelper::convertNameToEnglish($natureFile);
            $fileName = time().'_'.$extension;
            $request->avatar->move($destinationPath, $fileName);
            if ($user->avatar) {
                \File::delete(substr($user->avatar,1));
            }
            $user->avatar = '/'.$destinationPath.'/'.$fileName;
        }
        $user->save();
        if (\Auth::user()->language == 'VN') {
            \Session::flash('success','Tài khoản đã được cập nhật.');
        }else{
            \Session::flash('success','Account has been updated.');
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $auth = \Auth::user();
        $user = User::findOrFail($id);
        if ($auth->role_id != 1 && $user->role_id == 1) abort(402);

        $images = StatusImage::where('created_by',$id)->orderBy('created_at','desc')->get();
        $canChi = \App\Helper\UtilitiesHelper::getCanChi(date('Y',strtotime($user->birthday)));
        $cungMenh = \App\Helper\UtilitiesHelper::cungMenh(date('Y',strtotime($user->birthday)),$user->sex);
        return view('user.view',compact('user','images','canChi','cungMenh'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $user = User::find($id);
        return view('user.security',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $rules = array('old_password'  => 'required|max:255',
                    'new_password' => 'required|min:6|max:255|different:old_password',
                    'confirm_new_password' => 'required|max:255|same:new_password');
        $validator = \Validator::make(\Input::all(),$rules);
        if ($validator->fails()) {
            return back()->withErrors($validator);
        }
        if (\Auth::attempt(['password' => $request->old_password])) {
            $user = \Auth::user();
            $user->password = bcrypt($request->new_password);
            if (\Auth::user()->language == 'VN') {
                \Session::flash('success','Mật khẩu đã được thay đổi.');
            }else{
                \Session::flash('success','Password has been changed.');
            }
            $user->save();
        }else{
            if (\Auth::user()->language == 'VN') {
                \Session::flash('error','Mật khẩu không đúng, vui lòng thử lại.');
            }else{
                \Session::flash('error','Incorrect password, please try again.');
            }
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getFriend(){
        return view('user.friend');
    }
    public function postSearch(Request $request){
        // $users = User::where('name','like',$name)->get();
    }
    public function getList(){
        $users = User::all();
        return view('user.list',compact('users'));
    }
    public function postChangeRole(Request $request, $id){
        $user = User::find($id);
        $user->role_id = $request->role_id;
        $user->save();
        return back();
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StatusComment;

class StatusCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make(\Input::all(),['content' => 'required']);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $comment = new StatusComment();
        $comment->content = $request->content;
        $comment->status_id = $request->status_id;
        $comment->created_by = \Auth::user()->id;
        $comment->save();
        return response()->json(['success' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make(\Input::all(),['content' => 'required']);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $statusComment = StatusComment::find($id);
        $statusComment->content = $request->content;
        $statusComment->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $statusComment = StatusComment::find($id);
        $statusComment->delete();
        return back();
    }
}

<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Models\Message;
use App\Models\User;
use App\Models\Group;
use Request;
use LRedis;

class MessageController extends Controller
{
    public function getIndex(){
        return view('messages.index');
    }
    public function getShow($id){
        $user = User::find($id);
        $messages = Message::where([['sender_id',$id],['receiver_id',\Auth::user()->id]])
                        ->orWhere([['sender_id',\Auth::user()->id],['receiver_id',$id]])
                        ->orderBy('created_at','asc')->get();
        foreach ($messages as $message) {
            if ($message->receiver_id == \Auth::user()->id) {
                $message->status = null;
                $message->save();
            }
        }
        return view('messages.show',compact('messages','user'));
    }
    public function postCreate($id){
        $content = Request::input('content');
        self::sendMessage($content, $id);
    }
    public function sendMessage($content, $id){
        $user = \Auth::user();
        $message = new Message();
        $message->receiver_id = $id;
        $message->sender_id = \Auth::user()->id;
        $message->content = $content;
        $message->status = 1;
        $message->save();
        $data = ['content' => $message->content, 'created_at' => date('g:i A', strtotime($message->created_at)),'sender_id' => $message->sender_id, 'receiver_id' => $message->receiver_id, 'sender_name' => $user->name,'avatar' => $message->user->getAvatar()];
        $redis = LRedis::connection();
        $redis->publish('message', json_encode($data));
    }
    public function postCreateGroup($id){
        $message = new Message();
        $message->content = Request::input('content');
        $message->sender_id = \Auth::user()->id;
        $message->group_id = $id;
        $message->status = 1;
        $message->save();

        $group = Group::find($id);
        $data = ['content' => $message->content, 'created_at' => date('g:i A', strtotime($message->created_at)),'sender_id' => $message->sender_id, 'group_id' => $message->group_id,'user_avatar' => $message->user->getAvatar(), 'user_name' => $message->user->name, 'user_id' => $message->user->id, 'group_title' => $group->title];
        $redis = LRedis::connection();
        $redis->publish('message', json_encode($data));
        return response()->json($data);
    }
    public function getNew(){
        return view('messages.new');
    }
    public function postNew(){
        $inputs = \Input::all();
        $validator = \Validator::make(\Input::all(),['users_id' => 'required','content' => 'required']);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        foreach ($inputs['users_id'] as $userId) {
            self::sendMessage($inputs['content'], $userId);
        }
        if (count($inputs['users_id']) == 1) {
            $user = User::find($inputs['users_id'][0]);
            if (\Auth::user()->language == 'VN') {
                \Session::flash('success','Bạn đã gửi tin nhắn đến '.$user->name.'.');
            }else{
                \Session::flash('success','You sent a message to '.$user->name.'.');
            }
        }else{
            if (\Auth::user()->language == 'VN') {
                \Session::flash('success','Bạn đã gửi tin nhắn đến '.count($inputs['users_id']).' người.');
            }else{
                \Session::flash('success','You sent a message to '.count($inputs['users_id']).' people.');
            }
        }
        return back();
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Diary;

class DiaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $diarys = Diary::where('created_by',\Auth::user()->id)->get();
        return view('diary.index',compact('diarys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('diary.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make(\Input::all(),['title' => 'required|max:251','content' => 'required']);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $diary = new Diary();
        $diary->title = $request->title;
        $diary->content = $request->content;
        $diary->created_by = \Auth::user()->id;
        $diary->save();
        return redirect('/diary/'.$diary->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $diary = Diary::find($id);
        if ($diary->created_by == \Auth::user()->id) {
            return view('diary.show',compact('diary'));
        }else{
            return view('errors.503');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $diary = Diary::find($id);
        if ($diary->created_by == \Auth::user()->id) {
            return view('diary.update',compact('diary'));
        }else{
            return view('errors.503');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make(\Input::all(),['title' => 'required|max:251','content' => 'required']);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $diary = Diary::find($id);
        $diary->title = $request->title;
        $diary->content = $request->content;
        $diary->save();
        return redirect('/diary/'.$diary->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $diary = Diary::find($id);
        if ($diary->created_by == \Auth::user()->id) {
            $diary->delete();
        }
        return redirect('/diary');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\User;
use App\Models\PostLike;

class PostController extends Controller
{
    function __construct(){
        $this->middleware('moderator', ['only' => ['create', 'store', 'edit', 'update', 'destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::first();
        return view('blog.show',compact('post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = array('title' => 'required|max:255',
                      'content' => 'required',
                      'image' => 'required|mimes:jpeg,jpg,png,gif|max:10240|dimensions:width=500,height=380');
        $validator = \Validator::make(\Input::all(),$rule);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $post = new Post();
        $post->title = $request->title;
        $post->content = $request->content;
        $post->created_by = \Auth::user()->id;

        $destinationPath = 'uploads/img';
        $natureFile = $request->image->getClientOriginalName();
        $extension = \App\Helper\UploadHelper::convertNameToEnglish($natureFile);
        $fileName = time().'_'.$extension;
        $request->image->move($destinationPath, $fileName);
        $post->image = '/'.$destinationPath.'/'.$fileName;

        $post->save();
        return redirect('/blog/post/'.$post->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        if (!$post) {
            if (\Auth::user()->language == 'VN') {
                \Session::flash('warning','Bài viết đã bị xóa hoặc không tồn tại!');
            }else{
                \Session::flash('warning','The post has been deleted or does not exist!');
            }
            return back();
        }
        return view('blog.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        return view('blog.update',compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rule = array('title' => 'required|max:255',
                      'content' => 'required',
                      'image' => 'mimes:jpeg,jpg,png,gif|max:10240|dimensions:width=500,height=380');
        $validator = \Validator::make(\Input::all(),$rule);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $post = Post::find($id);
        $post->title = $request->title;
        $post->content = $request->content;
        if ($request->image) {
            $destinationPath = 'uploads/img';
            $natureFile = $request->image->getClientOriginalName();
            $extension = \App\Helper\UploadHelper::convertNameToEnglish($natureFile);
            $fileName = time().'_'.$extension;
            $request->image->move($destinationPath, $fileName);
            if ($post->image) {
                \File::delete(substr($post->image,1));
            }
            $post->image = '/'.$destinationPath.'/'.$fileName;
        }

        $post->save();
        return redirect('/blog/post/'.$post->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function postLike(Request $request){
        $like = PostLike::where([['post_id',$request->post_id],['created_by',\Auth::user()->id]])->first();
        $data = [];
        if (\Auth::user()->language == 'VN') {
            $textButton = 'Thích';
        }else{
            $textButton = 'Like';
        }
        if (!$like) {
            $newLike = new PostLike();
            $newLike->created_by = \Auth::user()->id;
            $newLike->post_id = $request->post_id;
            $newLike->save();
            $count = PostLike::where('post_id',$request->post_id)->count();
            $data = ['like'=>true, 'button' => '<button class="btn btn-link" style="padding: 0" title="'.$textButton.'"><i class="fa fa-heart"></i> '.$textButton.' '.$count.'</button>'];
        }else{
            $like->delete();
            $count = PostLike::where('post_id',$request->post_id)->count();
            if (!$count) {
                $data = ['like'=>false, 'button' => '<button class="btn btn-link" style="padding: 0" title="'.$textButton.'"><i class="fa fa-heart-o"></i> '.$textButton.'</button>'];
            }else{
                $data = ['like'=>false, 'button' => '<button class="btn btn-link" style="padding: 0" title="'.$textButton.'"><i class="fa fa-heart-o"></i> '.$textButton.' '.$count.'</button>'];
            }
        }
        return response()->json($data);
    }
}

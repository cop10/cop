<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Group;
use App\Models\UsersGroup;
use App\Models\Message;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('group.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::where('id','!=',\Auth::user()->id)->get();
        return view('group.create',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = array('title' => 'required|max:255',
                      'users_id' => 'required');
        $validator = \Validator::make(\Input::all(),$rule);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $group = new Group();
        $group->title = $request->title;
        $group->created_by = \Auth::user()->id;
        $group->save();
        self::addUserToGroup(\Auth::user()->id, $group->id);
        foreach ($request->users_id as $user_id) {
            self::addUserToGroup($user_id, $group->id);
        }
        return redirect('/group/'.$group->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $member = UsersGroup::where([['group_id',$id],['user_id',\Auth::user()->id]])->first();
        if (!$member) {
            return view('errors.503');
        }
        $group = Group::find($id);
        $usersGroup = UsersGroup::where('group_id',$id)->get();
        $messages = Message::where('group_id',$id)->orderBy('created_at','asc')->get();
        foreach ($messages as $message) {
            if ($message->sender_id != \Auth::user()->id) {
                $message->status = null;
                $message->save();
            }
        }
        return view('group.show',compact('group','usersGroup','messages'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = Group::find($id);
        if ($group->created_by != \Auth::user()->id) {
            return view('errors.503');
        }
        $usersGroups = UsersGroup::where('group_id',$id)->get();
        return view('group.update',compact('group','usersGroups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rule = array('title' => 'required|max:255',
                      'users_id' => 'required');
        $validator = \Validator::make(\Input::all(),$rule);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $group = Group::find($id);
        $group->title = $request->title;
        $group->save();
        $usersGroups = UsersGroup::where([['group_id',$id],['user_id','<>',\Auth::user()->id]])->delete();
        foreach ($request->users_id as $user_id) {
            self::addUserToGroup($user_id, $group->id);
        }
        if (\Auth::user()->language == 'VN') {
            \Session::flash('success','Bạn vừa cập nhật thông tin nhóm '.$group->title.'.');
        }else{
            \Session::flash('success','You just updated information '.$group->title.' group.');
        }
        return redirect('/group/'.$group->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usersGroups = Message::where('group_id',$id)->delete();
        $usersGroups = UsersGroup::where('group_id',$id)->delete();
        $group = Group::find($id);
        if (\Auth::user()->language == 'VN') {
            \Session::flash('success','Đã xóa nhóm '.$group->title.'.');
        }else{
            \Session::flash('success', $group->title.'group deleted.');
        }
        $group->delete();
        return redirect('/group');
    }
    public function addUserToGroup($user_id,$group_id){
        $query = UsersGroup::where([['user_id',$user_id],['group_id',$group_id]])->first();
        if (!$query) {
            $usersGroup = new UsersGroup();
            $usersGroup->user_id = $user_id;
            $usersGroup->group_id = $group_id;
            $usersGroup->save();
        }
    }
    public function postLeaveGroup(Request $request, $id){
        $query = UsersGroup::where([['user_id',$request->user_id],['group_id',$id]])->first();
        if ($query) {
            $query->delete();
        }
        return redirect('/group');
    }
}

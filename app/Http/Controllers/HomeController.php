<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Status;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['index']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function getIntroduce(){
        if (isset($_GET['language'])) {
            \Session::put('language', $_GET['language']);
        }else{
            if (\Session::get('language')) {
                $_GET['language'] = \Session::get('language');
            }else{
                $_GET['language'] = 'VN';
            }
        }
        return view('introduce');
    }
}

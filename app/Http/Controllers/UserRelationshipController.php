<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserRelationship;

class UserRelationshipController extends Controller
{
    public function getIndex(){
        $user_id = \Auth::user()->id;
        $pendingRelationship = UserRelationship::where([['user_one_id',$user_id],['status','0'],['action_user_id','!=',$user_id]])->orWhere([['user_two_id',$user_id],['status','0'],['action_user_id','!=',$user_id]])->get();
        $sendFriendsRequest = UserRelationship::where([['user_one_id',$user_id],['status','0'],['action_user_id',$user_id]])->orWhere([['user_two_id',$user_id],['status','0'],['action_user_id',$user_id]])->get();
        $users = User::where('id','!=',$user_id)->get();
        return view('userRelationship.index', compact('users','pendingRelationship','sendFriendsRequest'));
    }
    public function postCreate(Request $request){
        $user_one_id = $request->user_one_id;
        $user_two_id = $request->user_two_id;
        if ($user_one_id == $user_two_id) {
            return back();
        }
        $userRelationshipOne = UserRelationship::where('user_one_id',$user_one_id)->where('user_two_id',$user_two_id)->first();
        $userRelationshipTwo = UserRelationship::where('user_one_id',$user_two_id)->where('user_two_id',$user_one_id)->first();
        if (!$userRelationshipOne && !$userRelationshipTwo) {
            $newRelationship = new UserRelationship();
            $newRelationship->user_one_id = $user_one_id;
            $newRelationship->user_two_id = $user_two_id;
            $newRelationship->status = $request->status;
            $newRelationship->action_user_id = \Auth::user()->id;
            $newRelationship->save();
        }
        return back();
    }
    public function postAccepted(Request $request){
        self::updateUserRelationship($request);
        return back();
    }
    public function postDeclined(Request $request){
        self::updateUserRelationship($request);
        return back();
    }
    public function updateUserRelationship($request){
        $userRelationship = UserRelationship::find($request->user_relationship_id);
        $userRelationship->status = $request->status;
        $userRelationship->action_user_id = \Auth::user()->id;
        $userRelationship->save();
    }
    public function getDeclined(){
        $user_id = \Auth::user()->id;
        $userRelationship = UserRelationship::where([['status','2'],['user_one_id',$user_id],['action_user_id',$user_id]])->orWhere([['user_two_id',$user_id],['action_user_id',$user_id],['status','2']])->orderBy('updated_at','desc')->get();
        return view('userRelationship.declined',compact('userRelationship'));
    }
    public function getBlocked(){
        $user_id = \Auth::user()->id;
        $userRelationship = UserRelationship::where([['status','3'],['user_one_id',$user_id],['action_user_id',$user_id]])->orWhere([['status','3'],['user_two_id',$user_id],['action_user_id',$user_id]])->orderBy('updated_at','desc')->get();
        return view('userRelationship.blocked',compact('userRelationship'));
    }
    public function postDelete(Request $request){
        $userRelationship = UserRelationship::find($request->user_relationship_id);
        $userRelationship->delete();
        return back();
    }
    public function postBlocked(Request $request){
        $userRelationship = UserRelationship::find($request->user_relationship_id);
        $userRelationship->status = $request->status;
        $userRelationship->action_user_id = \Auth::user()->id;
        $userRelationship->save();
        return back();
    }
    public function postBlock(Request $request){
        $user_one_id = $request->user_id;
        $user_two_id = \Auth::user()->id;
        $query = UserRelationship::where('user_one_id',$user_one_id)->where('user_two_id',$user_two_id)->first();
        if (!$query) {
            $query = UserRelationship::where('user_one_id',$user_two_id)->where('user_two_id',$user_one_id)->first();
        }
        $query->status = 3;
        $query->action_user_id = \Auth::user()->id;
        $query->save();
        return back();
    }
}

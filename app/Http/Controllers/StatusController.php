<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Status;
use App\Models\StatusLike;
use App\Models\StatusImage;
use App\Models\StatusAttachment;

class StatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->images) {
            $rules = array('images' => 'required|array',
                           'attachment' => 'max:10240');
        }else{
            $rules = array('content' => 'required',
                           'attachment' => 'max:10240');
        }
        $validator = \Validator::make(\Input::all(), $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $status = new Status();
        $status->content = $request->content;
        $status->created_by = \Auth::user()->id;
        $status->save();
        if ($request->images) {
            $files = \Input::file('images');
            foreach ($files as $file){
                $imageRules = array('image' => 'required|mimes:jpeg,jpg,png,gif|max:10240');
                $imageValidator = \Validator::make(array('image' => $file), $imageRules);
                if ($imageValidator->fails()) {
                    $status->delete();
                    return back()->withErrors($imageValidator)->withInput();
                }
                $statusImage = new StatusImage();
                $destinationPath = 'uploads/img';
                $natureFile = $file->getClientOriginalName();
                $extension = \App\Helper\UploadHelper::convertNameToEnglish($natureFile);
                $fileName = time().'_'.$extension;
                $file->move($destinationPath, $fileName);
                $statusImage->image = '/'.$destinationPath.'/'.$fileName;
                $statusImage->status_id = $status->id;
                $statusImage->created_by = \Auth::user()->id;
                $statusImage->save();
            }
        }
        if ($request->attachment) {
            $destinationPath = 'uploads/attachments';
            $natureFile = $request->attachment->getClientOriginalName();
            $extension = \App\Helper\UploadHelper::convertNameToEnglish($natureFile);
            $fileName = time().'_'.$extension;
            $request->attachment->move($destinationPath, $fileName);
            $statusAttachment = new StatusAttachment();
            $statusAttachment->status_id = $status->id;
            $statusAttachment->created_by = \Auth::user()->id;
            $statusAttachment->attachment = '/'.$destinationPath.'/'.$fileName;
            $statusAttachment->save();
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = \Validator::make(\Input::all(),['content' => 'required']);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $status = Status::find($id);
        $status->content = $request->content;
        $status->save();
        return response()->json(['content' => $status->content]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = Status::find($id);
        foreach ($status->statusAttachment as $attachment) {
            if ($attachment->attachment) {
                \File::delete(substr($attachment->attachment,1));
            }
            $attachment->delete();
        }
        foreach ($status->statusImages as $image) {
            if ($image->image) {
                \File::delete(substr($image->image,1));
            }
            $image->delete();
        }
        foreach ($status->comments as $comment) {
            if ($comment) {
                $comment->delete();
            }
        }
        foreach ($status->likes as $like) {
            if ($like) {
                $like->delete();
            }
        }
        $status->delete();
        return back();
    }
    public function postLike(Request $request){
        $like = StatusLike::where([['status_id',$request->status_id],['created_by',\Auth::user()->id]])->first();
        $data = [];
        if (\Auth::user()->language == 'VN') {
            $textButton = 'Thích';
        }else{
            $textButton = 'Like';
        }
        if (!$like) {
            $newLike = new StatusLike();
            $newLike->created_by = \Auth::user()->id;
            $newLike->status_id = $request->status_id;
            $newLike->save();
            $count = StatusLike::where('status_id',$request->status_id)->count();
            $data = ['like'=>true, 'button' => '<button class="btn btn-link" style="padding: 0" title="'.$textButton.'"><i class="fa fa-heart"></i> '.$textButton.' '.$count.'</button>'];
        }else{
            $like->delete();
            $count = StatusLike::where('status_id',$request->status_id)->count();
            if (!$count) {
                $data = ['like'=>false, 'button' => '<button class="btn btn-link" style="padding: 0" title="'.$textButton.'"><i class="fa fa-heart-o"></i> '.$textButton.'</button>'];
            }else{
                $data = ['like'=>false, 'button' => '<button class="btn btn-link" style="padding: 0" title="'.$textButton.'"><i class="fa fa-heart-o"></i> '.$textButton.' '.$count.'</button>'];
            }
        }
        return response()->json($data);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusLike extends Model
{
    protected $table = 'status_likes';
}

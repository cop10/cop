<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusAttachment extends Model
{
    protected $table = 'status_attachment';
}

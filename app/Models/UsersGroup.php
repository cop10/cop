<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersGroup extends Model
{
    protected $table = 'users_group';
    public function user(){
        return $this->belongsTo('App\Models\User','user_id','id');
    }
}

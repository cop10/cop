<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\UsersGroup;
use App\Models\Group;

class Group extends Model
{
    protected $table = 'groups';
    public static function getGroups(){
        $usersGroup = UsersGroup::where('user_id',\Auth::user()->id)->get();
        $groups_id = array();
        foreach ($usersGroup as $userGroup) {
            $groups_id[] = $userGroup->group_id;
        }
        $groups = Group::find($groups_id);
        return $groups;
    }
    public function getCurrentMessase(){
        return Message::where('group_id',$this->id)->orderBy('created_at','desc')->first();
    }
    public function members(){
        return $this->hasMany('App\Models\UsersGroup','group_id','id');
    }
    public static function getGroupsId(){
        $usersGroup = UsersGroup::where('user_id',\Auth::user()->id)->get();
        $groups_id = array();
        foreach ($usersGroup as $userGroup) {
            $groups_id[] = $userGroup->group_id;
        }
        $groups = Group::find($groups_id);
        $result = [];
        foreach ($groups as $group) {
            $result[] = $group->id;
        }
        return json_encode($result);
    }
    public function countNewMesage(){
        $query = Message::where([['sender_id','!=', \Auth::user()->id], ['group_id',$this->id], ['status',1]])->get();
        if ($query) {
            return count($query);
        }
        return false;
    }
}

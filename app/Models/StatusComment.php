<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StatusComment extends Model
{
    protected $table = 'status_comment';
    public function user(){
        return $this->belongsTo('App\Models\User','created_by','id');
    }
}

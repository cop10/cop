<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\UserRelationship;
use App\Models\User;

class UserRelationship extends Model
{
    protected $table = 'user_relationship';

    // Status  Meaning
    // 0       Pending  (chưa giải quyết)
    // 1       Accepted (được chấp nhận)
    // 2       Declined (bị từ chối)
    // 3       Blocked  (bị chặn)

    public function user(){
        $user_one = User::find($this->user_one_id);
        $user_two = User::find($this->user_two_id);
        if ($user_one->id == \Auth::user()->id)
            return $user_two;
        return $user_one;
    }
    public static function getFriend(){
        $userRelationship = UserRelationship::where([['status','1'],['user_one_id',\Auth::user()->id]])->orWhere([['status','1'],['user_two_id',\Auth::user()->id]])->get();
        return $userRelationship;
    }
    public static function pendingRelationship(){
        $user_id = \Auth::user()->id;
        return UserRelationship::where([['user_one_id',$user_id],['status','0'],['action_user_id','!=',$user_id]])->orWhere([['user_two_id',$user_id],['status','0'],['action_user_id','!=',$user_id]])->get();
    }
}

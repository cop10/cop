<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Post;
use App\Models\PostLike;

class Post extends Model
{
    protected $table = 'posts';
    public function user(){
        return $this->belongsTo('App\Models\User','created_by','id');
    }
    public static function getPosts(){
        return Post::orderBy('created_at','desc')->get();
    }
    public static function getLastFivePosts(){
        return Post::orderBy('created_at','desc')->take(5)->get();
    }
    public function comments(){
        return $this->hasMany('App\Models\PostComment','post_id','id');
    }
    public function checkLike(){
        $like = PostLike::where([['post_id',$this->id],['created_by',\Auth::user()->id]])->first();
        if ($like) {
            return true;
        }
        return false;
    }
    public function likes(){
        return $this->hasMany('App\Models\PostLike','post_id','id');
    }
}

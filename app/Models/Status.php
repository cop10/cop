<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\StatusLike;
use App\Models\Status;

class Status extends Model
{
    protected $table = 'status';
    public function user(){
        return $this->belongsTo('App\Models\User','created_by','id');
    }
    public function comments(){
        return $this->hasMany('App\Models\StatusComment','status_id','id');
    }
    public function checkLike(){
        $like = StatusLike::where([['status_id',$this->id],['created_by',\Auth::user()->id]])->first();
        if ($like) {
            return true;
        }
        return false;
    }
    public function likes(){
        return $this->hasMany('App\Models\StatusLike','status_id','id');
    }
    public static function getFriendStatus(){
        $statusId = array();
        $myStatus = Status::where('created_by',\Auth::user()->id)->get();
        foreach ($myStatus as $value) {
            if ($value) {
                $statusId[] = $value->id;
            }
        }
        $userRelationship = \Auth::user()->friend();
        foreach ($userRelationship as $value) {
            $sttUser = Status::where('created_by',$value->user()->id)->get();
            foreach ($sttUser as $stt) {
                if ($stt) {
                    $statusId[] = $stt->id;
                }
            }
        }
        return Status::whereIn('id',$statusId)->orderBy('created_at','desc')->get();
    }
    public function statusImages(){
        return $this->hasMany('App\Models\StatusImage','status_id','id');
    }
    public function statusAttachment(){
        return $this->hasMany('App\Models\StatusAttachment','status_id','id');
    }
    public static function getTimeCreated($created_at){
        $seconds  = strtotime(date('Y-m-d H:i:s')) - strtotime($created_at);

        $years = floor($seconds / (3600*24*30*12));
        $months = floor($seconds / (3600*24*30));
        $day = floor($seconds / (3600*24));
        $hours = floor($seconds / 3600);
        $mins = floor(($seconds - ($hours*3600)) / 60);
        $secs = floor($seconds % 60);

        if($seconds < 60){
            if (\Auth::user()->language == 'VN') {
                $time = "vừa xong";
            }else{
                $time = $secs." seconds ago";
            }
        }else if( 60 <= $seconds && $seconds < 60*2 ){
            if (\Auth::user()->language == 'VN') {
                $time = $mins." phút";
            }else{
                $time = $mins." min ago";
            }
        }else if(60*2 <= $seconds && $seconds < 60*60 ){
            if (\Auth::user()->language == 'VN') {
                $time = $mins." phút";
            }else{
                $time = $mins." mins ago";
            }
        }else if( 60*60*1 <= $seconds && $seconds < 60*60*2){
            if (\Auth::user()->language == 'VN') {
                $time = $hours." giờ";
            }else{
                $time = $hours." hour ago";
            }
        }else if( 60*60*2 <= $seconds && $seconds < 60*60*24){
            if (\Auth::user()->language == 'VN') {
                $time = $hours." giờ";
            }else{
                $time = $hours." hours ago";
            }
        }else if(60*60*24 <= $seconds && $seconds < 60*60*24*2){
            if (\Auth::user()->language == 'VN') {
                $time = $day." ngày";
            }else{
                $time = $day." day ago";
            }
        }else if(60*60*24*2 <= $seconds && $seconds < 60*60*24*30){
            if (\Auth::user()->language == 'VN') {
                $time = $day." ngày";
            }else{
                $time = $day." days ago";
            }
        }else if(60*60*24*30*1 <= $seconds && $seconds < 60*60*24*30*2){
            if (\Auth::user()->language == 'VN') {
                $time = $months." tháng";
            }else{
                $time = $months." month ago";
            }
        }else if(60*60*24*30*2 <= $seconds && $seconds < 60*60*24*30*12){
            if (\Auth::user()->language == 'VN') {
                $time = $months." tháng";
            }else{
                $time = $months." months ago";
            }
        }else if(60*60*24*30*12 <= $seconds && $seconds < 60*60*24*30*12*2){
            if (\Auth::user()->language == 'VN') {
                $time = $years." năm";
            }else{
                $time = $years." year ago";
            }
        }else{
            if (\Auth::user()->language == 'VN') {
                $time = $years." năm";
            }else{
                $time = $years." years ago";
            }
        }
        if ( str_contains($time,'ngày') || str_contains($time,'tháng') || str_contains($time,'năm') || str_contains($time,'day') || str_contains($time,'days') || str_contains($time,'month') || str_contains($time,'months') || str_contains($time,'year') || str_contains($time,'years')) {
            return date('d-m-Y h:i A',strtotime($created_at));
        }
        return $time;
    }
}

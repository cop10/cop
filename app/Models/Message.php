<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Message;
use App\Models\User;

class Message extends Model
{
    protected $table = 'messages';
    public function user(){
        return $this->belongsTo('App\Models\User','sender_id','id');
    }
    public function getUser(){
        if ($this->sender_id == \Auth::user()->id) {
            return $this->belongsTo('App\Models\User','receiver_id','id');
        }
        return $this->belongsTo('App\Models\User','sender_id','id');
    }
    public function group(){
        return $this->belongsTo('App\Models\Group','group_id','id');
    }
    public function isFirstDailyMessage(){
        $date = date('Y-m-d',strtotime($this->created_at));
        $query = Message::whereDate('created_at',$date)->where(function ($query) {
                    $query->where([['sender_id',$this->receiver_id],['receiver_id',$this->sender_id]])->orWhere([['receiver_id',$this->receiver_id],['sender_id',$this->sender_id]]);
                })->orderBy('created_at','asc')->first();
        if ($query->id == $this->id) {
            return true;
        }
        return false;
    }
    public function isFirstDailyMessageGroup(){
        $date = date('Y-m-d',strtotime($this->created_at));
        $query = Message::whereDate('created_at',$date)->where('group_id',$this->group_id)->orderBy('created_at','asc')->first();
        if ($query->id == $this->id) {
            return true;
        }else{
            return false;
        }
    }
}

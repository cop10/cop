<?php

namespace App\Helper;

class UtilitiesHelper{
    public static function getCan($year) {
        $can = array('Canh','Tân','Nhâm','Quý','Giáp','Ất','Bính','Đinh','Mậu','Kỷ');
        $result = '';
        foreach ($can as $key => $value) {
            if ($key == substr($year, -1)){
                $result = $value;
            }
        }
        return $result;
    }
    public static function getChi($year) {
        $chi = array('Tý','Sửu','Dần','Mão','Thìn','Tỵ','Ngọ','Mùi','Thân','Dậu','Tuất','Hợi');
        $result = '';
        if ($year < 2000) {
            $x = substr($year,-2)%12;
        }else{
            $x = (substr($year,-2)+100)%12;
        }
        foreach ($chi as $key => $value) {
            if ($key == $x){
                $result = $value;
            }
        }
        return $result;
    }
    public static function getCanChi($year) {
        return self::getCan($year).' '.self::getChi($year);
    }
    public static function cungMenh($namSinh,$gioiTinh){
        $x = self::tinhQuaiSo($namSinh,$gioiTinh);
        $queMenh = array('1' => 'Thủy',
                         '2' => 'Thổ',
                         '3' => 'Mộc',
                         '4' => 'Mộc',
                         '5' => 'Thổ',
                         '6' => 'Kim',
                         '7' => 'Kim',
                         '8' => 'Thổ',
                         '9' => 'Hỏa');
        $cungMenh = '';
        foreach ($queMenh as $key => $value) {
            if ($key == $x) {
                $cungMenh = $value;
            }
        }
        return $cungMenh;
    }
    public static function tinhQuaiSo($namSinh,$gioiTinh){
        $x = substr($namSinh, -1) +  substr($namSinh, -2,1);
        if ($x >= 10) {
            $x = substr($x, 0,1) + substr($x, 1,1);
        }
        if ($gioiTinh == 'male') {
            $x = 10 - $x;
        }else{
            $x = 5 + $x;
        }
        if ($x >= 10) {
            $x = substr($x, 0,1) + substr($x, 1,1);
        }
        return $x;
    }
    public static function nguHanh($namSinh){
        //nam sinh tu 1920 => 2049
        //một số tuổi năm canh thân : 1800,1860,1920,1980,2040
        //tuoi - canh than = x sau đó x-1=y , y:2=z , z-1=a
        $nguHanh = array(1=> array('ngu hanh' => 'Hải trung kim','giai nghia' => 'Vàng trong biển'),
                        2 => array('ngu hanh' => 'Lư trung hỏa','giai nghia' => 'Lửa trong lò'),
                        3 => array('ngu hanh' => 'Đại lâm mộc','giai nghia' => 'Gỗ rừng già'),
                        4 => array('ngu hanh' => 'Lộ bàng thổ','giai nghia' => 'Đất đường đi'),
                        5 => array('ngu hanh' => 'Kiếm phong kim','giai nghia' => 'Vàng mũi kiếm'),
                        6 => array('ngu hanh' => 'Sơn đầu hỏa','giai nghia' => 'Lửa trên núi'),
                        7 => array('ngu hanh' => 'Gian hạ thủy','giai nghia' => 'Nước cuối khe'),
                        8 => array('ngu hanh' => 'Thành Đầu Thổ','giai nghia' => 'Đất trên thành'),
                        9 => array('ngu hanh' => 'Bạch lạp kim','giai nghia' => 'Vàng chân đèn'),
                        10 => array('ngu hanh' => 'Dương liễu mộc','giai nghia' => 'Gỗ cây dương'),
                        11 => array('ngu hanh' => 'Tuyền trung thủy','giai nghia' => 'Nước trong suối'),
                        12 => array('ngu hanh' => 'Ôc thượng thổ','giai nghia' => 'Đất nóc nhà'),
                        13 => array('ngu hanh' => 'Tích lịch hỏa','giai nghia' => 'Lửa sấm sét'),
                        14 => array('ngu hanh' => 'Tùng Bách Mộc','giai nghia' => 'Gỗ tùng bách'),
                        15 => array('ngu hanh' => 'Trường lưu thủy','giai nghia' => 'Nước chảy mạnh'),
                        16 => array('ngu hanh' => 'Sa trung kim','giai nghia' => 'Vàng trong cát'),
                        17 => array('ngu hanh' => 'Sơn hạ hỏa','giai nghia' => 'Lửa trên núi'),
                        18 => array('ngu hanh' => 'Bình địa mộc','giai nghia' => 'Gỗ đồng bằng'),
                        19 => array('ngu hanh' => 'Bích thượng thổ','giai nghia' => 'Đất tò vò'),
                        20 => array('ngu hanh' => 'Kim bạc kim','giai nghia' => 'Vàng pha bạc'),
                        21 => array('ngu hanh' => 'Phúc đăng hỏa','giai nghia' => 'Lửa đèn to'),
                        22 => array('ngu hanh' => 'Thiên hà thủy','giai nghia' => 'Nước trên trời'),
                        23 => array('ngu hanh' => 'Đại dịch thổ','giai nghia' => 'Đất nền nhà'),
                        24 => array('ngu hanh' => 'Thoa xuyến kim','giai nghia' => 'Vàng trang sức'),
                        25 => array('ngu hanh' => 'Tang chá mộc','giai nghia' => 'Gỗ cây dâu'),
                        26 => array('ngu hanh' => 'Đại khe thủy','giai nghia' => 'Nước khe lớn'),
                        27 => array('ngu hanh' => 'Sa trung thổ','giai nghia' => 'Đất pha cát'),
                        28 => array('ngu hanh' => 'Thiên thượng hỏa','giai nghia' => 'Lửa trên trời'),
                        29 => array('ngu hanh' => 'Thạch lựu mộc','giai nghia' => 'Gỗ cây lựu'),
                        30 => array('ngu hanh' => 'Đại hải thủy','giai nghia' => 'Nước biển lớn'));
        if ($namSinh == 1920) {
            $canhThan = 1860;
        }elseif(1920 < $namSinh && $namSinh <= 1980){
            $canhThan = 1920;
        }elseif(1980 < $namSinh && $namSinh <= 2040){
            $canhThan = 1980;
        }else{
            $canhThan = 2040;
        }
        $x = $namSinh - $canhThan;
        if ($x%2 == 0) {
            $x = ($x/2)-1;
        }else{
            $x = (($x-1)/2)-1;
        }
        if ($x == -1) {
            $x = 29;
        }
        if ($x == 0) {
            $x = 30;
        }
        $result = array('ngu_hanh' => '','giai_nghia' => '','menh' => '');
        foreach ($nguHanh as $key => $value) {
            if ($key == $x) {
                $result['ngu_hanh'] = $value['ngu hanh'];
                $result['giai_nghia'] = $value['giai nghia'];
            }
        }
        $menh = array(1 => 'Kim',2 => 'Thủy',3 => 'Hỏa',4 => 'Thổ',5 => 'Mộc');
        $canValue = array('Giáp' => '1','Ất' => '1',
                          'Bính' => '2','Đinh' => '2',
                          'Mậu' => '3','Kỷ' => '3',
                          'Canh' => '4','Tân' => '4',
                          'Nhâm' => '5','Quý' => '5');
        $chiValue = array('Tý' => '0','Sửu' => '0','Ngọ' => '0','Mùi' => '0',
                          'Dần' => '1','Mão' => '1','Thân' => '1','Dậu' => '1',
                          'Thìn' => '2','Tỵ' => '2','Tuất' => '2','Hợi' => '2');
        $canNamSinh = self::getCan($namSinh);
        $chiNamSinh = self::getChi($namSinh);
        foreach ($canValue as $key => $value) {
            if ($key == $canNamSinh) {
                $a = $value;
            }
        }
        foreach ($chiValue as $key => $value) {
            if ($key == $chiNamSinh) {
                $b = $value;
            }
        }
        $c = $a + $b;
        if ($c > 5) {
            $c -= 5;
        }
        foreach ($menh as $key => $value) {
            if ($c == $key) {
                $result['menh'] = $value;
            }
        }
        return $result;
    }
}